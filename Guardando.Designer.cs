﻿namespace PruebaScreen
{
    partial class Guardando
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.imgGuardando = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgGuardando)).BeginInit();
            this.SuspendLayout();
            // 
            // imgGuardando
            // 
            this.imgGuardando.Location = new System.Drawing.Point(186, 27);
            this.imgGuardando.Name = "imgGuardando";
            this.imgGuardando.Size = new System.Drawing.Size(748, 611);
            this.imgGuardando.TabIndex = 0;
            this.imgGuardando.TabStop = false;
            // 
            // Guardando
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 661);
            this.Controls.Add(this.imgGuardando);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Guardando";
            this.Opacity = 0.7D;
            this.Text = "Guardando";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Guardando_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgGuardando)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PictureBox imgGuardando;
    }
}