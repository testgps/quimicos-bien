﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaScreen
{
    public partial class Detalle : Form
    {
        Clases.CSustancia cSustancia = new Clases.CSustancia();
        Clases.CPictograma cPictograma = new Clases.CPictograma();
        Clases.CEquipopp cEquipopp = new Clases.CEquipopp();
        Clases.CGaveta cGaveta = new Clases.CGaveta();
        Clases.CArea cArea = new Clases.CArea();
        private int indice;
        private int rol;
        Font SmallFont = new Font("Siemens Sans", 10);
        Font SmallFontBold = new Font("Siemens Sans", 10, FontStyle.Bold);
        Font MediumFont = new Font("Siemens Sans", 12, FontStyle.Bold);
        Font LargeFont = new Font("Siemens Sans", 14, FontStyle.Bold);

        public Detalle(int indice, int rol)
        {
            InitializeComponent();
            this.indice = indice;
            this.rol = rol;
            //MessageBox.Show("ESTAMOS EN EL DETALLE DE: "+ indice);
            mostrarSustancia();
            
        }

        public void mostrarSustancia()
        {
            List<Clases.CSustancia> lista = new List<Clases.CSustancia>();
            lista = cSustancia.sustanciaId(indice);
            int n = cSustancia.sustanciaId(indice).Count();
            //CHECKS SELECCIONADOS DEL PICTOGRAMA
            List<Clases.CPictograma> listaPictograma = cPictograma.GetPictogramaById(indice);
            int npictogramas = listaPictograma.Count;

            List<Clases.CEquipopp> listaEpp = cEquipopp.GetEquipoById(indice);
            int nequipo = listaEpp.Count;

            List<Clases.CGaveta> listaGaveta = cGaveta.GetGavetaById(indice);
            int ngaveta = listaGaveta.Count;

            List<Clases.CArea> listaArea = cArea.GetAreaById(indice);
            int narea = listaArea.Count;

            //MessageBox.Show("LAS GAVETAS DE LA SUSTANCIA "+ indice + " SON: " + ngaveta);

            for (int i = 0; i < n; i++)
            {
                PictureBox pictureBox = new PictureBox();

                // Crear un contenedor para el elemento de la lista
                TableLayoutPanel itemPanel = new TableLayoutPanel();
                itemPanel.AutoSize = true;
                itemPanel.ColumnCount = 2;
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));

                using (MemoryStream ms = new MemoryStream(lista[i].ImagenSustancia))
                {
                    // Crear una instancia de Image a partir del MemoryStream
                    Image imagen = Image.FromStream(ms);

                    pictureBox.Image = imagen;
                }

                pictureBox.Margin = new Padding(0, 0, 0, 50);
                pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                pictureBox.Size = new Size(200, 200);

                Label label = new Label();
                label.Text = "N. de Sustancia: " + lista[i].IdSustancia.ToString();
                label.AutoSize = false; // Establece AutoSize en false
                label.Dock = DockStyle.Top; // Establece Dock en Top
                label.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                label.Margin = new Padding(20, 0, 0, 25);
                label.Font = LargeFont;
                label.AutoSize = false;
                label.Size = new Size(label.PreferredWidth, label.PreferredHeight);

                Label label2 = new Label();
                label2.Text = lista[i].NombreSustancia.ToString();
                label2.Margin = new Padding(20, 0, 0, 25);
                label2.Font = LargeFont;
                label2.AutoSize = false;
                label2.Size = new Size(label2.PreferredWidth, label2.PreferredHeight);

                Label label3 = new Label();
                label3.Text = lista[i].UsoRecomendado.ToString();
                label3.Margin = new Padding(20, 0, 0, 25);
                label3.Font = SmallFont;
                label3.AutoSize = false;
                label3.Size = new Size(label3.PreferredWidth, label3.PreferredHeight);

                Label label4 = new Label();
                label4.Text = "Palabra de advertencia: " + lista[i].PalabraAdvertencia.ToString();
                label4.Margin = new Padding(20, 0, 0, 25);
                label4.Font = SmallFontBold;
                label4.AutoSize = false;
                label4.Size = new Size(label4.PreferredWidth, label4.PreferredHeight);

                Label label6 = new Label();
                label6.Text = "Teléfono de emergencia" + lista[i].TelefonoEmergencia.ToString();
                label6.Margin = new Padding(20, 0, 0, 25);
                label6.Font = SmallFont;
                label6.AutoSize = false;
                label6.Size = new Size(label6.PreferredWidth, label6.PreferredHeight);

                Label label7 = new Label();
                label7.Text = "Consumo promedio: " + lista[i].ConsumoPromedio.ToString();
                label7.Margin = new Padding(20, 0, 0, 25);
                label7.Font = SmallFont;
                label7.AutoSize = false;
                label7.Size = new Size(label7.PreferredWidth, label7.PreferredHeight);

                Label label8 = new Label();
                label8.Text = "Cantidad estimada: " + lista[i].CantidadEstimada.ToString();
                label8.Margin = new Padding(20, 0, 0, 25);
                label8.Font = SmallFont;
                label8.AutoSize = false;
                label8.Size = new Size(label8.PreferredWidth, label8.PreferredHeight);

                Label label9 = new Label();
                label9.Text = "Tipo de contenedor: " + lista[i].TipoContenedor.ToString();
                label9.Margin = new Padding(20, 0, 0, 25);
                label9.Font = SmallFontBold;
                label9.AutoSize = false;
                label9.Size = new Size(label9.PreferredWidth, label9.PreferredHeight);

                Label label10 = new Label();
                label10.Text = "Riesgos: " + lista[i].RiesgosSustancia.ToString();
                label10.Margin = new Padding(20, 0, 0, 25);
                label10.Font = SmallFont;
                label10.AutoSize = false;
                label10.Size = new Size(label10.PreferredWidth, label10.PreferredHeight);

                Label label11 = new Label();
                label11.Text = "Número CAS: " + lista[i].NumeroCas.ToString();
                label11.Margin = new Padding(20, 0, 0, 25);
                label11.Font = SmallFont;
                label11.AutoSize = false;
                label11.Size = new Size(label11.PreferredWidth, label11.PreferredHeight);

                Label label12 = new Label();
                label12.Text = "Número ONU: " + lista[i].NumeroOnu.ToString();
                label12.Margin = new Padding(20, 0, 0, 25);
                label12.Font = SmallFont;
                label12.AutoSize = false;
                label12.Size = new Size(label12.PreferredWidth, label12.PreferredHeight);

                Label label13 = new Label();
                label13.Text = "Datos del Fabricante";
                label13.Margin = new Padding(20, 0, 0, 25);
                label13.Font = MediumFont;
                label13.AutoSize = false;
                label13.Size = new Size(label13.PreferredWidth, label13.PreferredHeight);

                Label label14 = new Label();
                label14.Text = lista[i].NombreFabricante.ToString();
                label14.Margin = new Padding(20, 0, 0, 25);
                label14.Font = SmallFont;
                label14.AutoSize = false;
                label14.Size = new Size(label14.PreferredWidth, label14.PreferredHeight);

                Label label15 = new Label();
                label15.Text = "Teléfono: " + lista[i].TelefonoFabricante.ToString();
                label15.Margin = new Padding(20, 0, 0, 25);
                label15.Font = SmallFont;
                label15.AutoSize = false;
                label15.Size = new Size(label15.PreferredWidth, label15.PreferredHeight);

                Label label16 = new Label();
                label16.Text = lista[i].DireccionFabricante.ToString();
                label16.Margin = new Padding(20, 0, 0, 25);
                label16.Font = SmallFont;
                label16.AutoSize = false;
                label16.Size = new Size(label16.PreferredWidth, label16.PreferredHeight);

                Label label17 = new Label();
                label17.Text = "Pictogramas";
                label17.Margin = new Padding(20, 0, 0, 25);
                label17.Font = MediumFont;
                label17.AutoSize = false;
                label17.Size = new Size(label17.PreferredWidth, label17.PreferredHeight);

                // Ajustar el ancho de las columnas del TableLayoutPanel
                itemPanel.ColumnStyles[1] = new ColumnStyle(SizeType.Percent, 100);
                //itemPanel.ColumnStyles[2] = new ColumnStyle(SizeType.AutoSize);
                //itemPanel.ColumnStyles[3] = new ColumnStyle(SizeType.AutoSize);
                //itemPanel.ColumnStyles[4] = new ColumnStyle(SizeType.AutoSize);
                //itemPanel.ColumnStyles.Insert(0, new ColumnStyle(SizeType.Percent, 100));

                itemPanel.SetColumnSpan(pictureBox, 1);
                itemPanel.SetRowSpan(pictureBox, 5);
                itemPanel.Controls.Add(pictureBox, 0, 0);

                itemPanel.SetColumnSpan(label, 1);
                itemPanel.SetRowSpan(label, 1);
                itemPanel.Controls.Add(label, 1, 0);

                itemPanel.SetColumnSpan(label2, 1);
                itemPanel.SetRowSpan(label2, 1);
                itemPanel.Controls.Add(label2, 1, 1);

                itemPanel.SetColumnSpan(label3, 1);
                itemPanel.SetRowSpan(label3, 1);
                itemPanel.Controls.Add(label3, 1, 2);

                itemPanel.SetColumnSpan(label4, 1);
                itemPanel.SetRowSpan(label4, 1);
                itemPanel.Controls.Add(label4, 1, 3);

                Label label5 = new Label();
                label5.Text = "Áreas a las que pertenece: ";
                label5.Font = SmallFontBold;
                label5.Margin = new Padding(20, 0, 0, 25);
                label5.AutoSize = false;
                label5.Size = new Size(label5.PreferredWidth, label5.PreferredHeight);

                itemPanel.SetColumnSpan(label5, 1);
                itemPanel.SetRowSpan(label5, 1);
                itemPanel.Controls.Add(label5, 1, 4);

                /////////////////////////////

                FlowLayoutPanel flowLayoutPanelArea = new FlowLayoutPanel();
                flowLayoutPanelArea.FlowDirection = FlowDirection.LeftToRight;
                flowLayoutPanelArea.AutoSize = true; // Ajustar el tamaño automáticamente
                itemPanel.Controls.Add(flowLayoutPanelArea, 1, 5);

                for (int j = 0; j < narea; j++)
                {
                    Label area = new Label();
                    area.Text = listaArea[j].NombreArea.ToString();
                    area.Margin = new Padding(20, 0, 0, 25);
                    area.Font = SmallFontBold;
                    area.AutoSize = false;
                    area.Size = new Size(area.PreferredWidth, area.PreferredHeight);

                    flowLayoutPanelArea.Controls.Add(area);
                }

                itemPanel.SetColumnSpan(label7, 1);
                itemPanel.SetRowSpan(label7, 1);
                itemPanel.Controls.Add(label7, 1, 7);

                itemPanel.SetColumnSpan(label8, 1);
                itemPanel.SetRowSpan(label8, 1);
                itemPanel.Controls.Add(label8, 1, 8);

                itemPanel.SetColumnSpan(label9, 1);
                itemPanel.SetRowSpan(label9, 1);
                itemPanel.Controls.Add(label9, 1, 9);

                itemPanel.SetColumnSpan(label10, 1);
                itemPanel.SetRowSpan(label10, 1);
                itemPanel.Controls.Add(label10, 1, 10);

                itemPanel.SetColumnSpan(label11, 1);
                itemPanel.SetRowSpan(label11, 1);
                itemPanel.Controls.Add(label11, 1, 11);

                itemPanel.SetColumnSpan(label12, 1);
                itemPanel.SetRowSpan(label12, 1);
                itemPanel.Controls.Add(label12, 1, 12);

                itemPanel.SetColumnSpan(label13, 1);
                itemPanel.SetRowSpan(label13, 1);
                itemPanel.Controls.Add(label13, 1, 13);

                itemPanel.SetColumnSpan(label14, 1);
                itemPanel.SetRowSpan(label14, 1);
                itemPanel.Controls.Add(label14, 1, 14);

                itemPanel.SetColumnSpan(label15, 1);
                itemPanel.SetRowSpan(label15, 1);
                itemPanel.Controls.Add(label15, 1, 15);

                itemPanel.SetColumnSpan(label16, 1);
                itemPanel.SetRowSpan(label16, 1);
                itemPanel.Controls.Add(label16, 1, 16);

                itemPanel.SetColumnSpan(label17, 1);
                itemPanel.SetRowSpan(label17, 1);
                itemPanel.Controls.Add(label17, 1, 17);

                FlowLayoutPanel flowLayoutPanelPictogramas = new FlowLayoutPanel();
                flowLayoutPanelPictogramas.FlowDirection = FlowDirection.LeftToRight;
                flowLayoutPanelPictogramas.AutoSize = true; // Ajustar el tamaño automáticamente
                itemPanel.Controls.Add(flowLayoutPanelPictogramas, 1, 18);

                for (int j = 0; j < npictogramas; j++)
                {
                    PictureBox pictograma = new PictureBox();
                    pictograma.Margin = new Padding(0, 0, 0, 20);
                    pictograma.SizeMode = PictureBoxSizeMode.StretchImage;
                    pictograma.Size = new Size(100, 100);

                    using (MemoryStream ms = new MemoryStream(listaPictograma[j].ImagenPictograma))
                    {
                        // Crear una instancia de Image a partir del MemoryStream
                        Image imagen = Image.FromStream(ms);
                        pictograma.Image = imagen;
                    }

                    flowLayoutPanelPictogramas.Controls.Add(pictograma);
                }

                Label label18 = new Label();
                label18.Text = "Equipo de Protección Personal";
                label18.Margin = new Padding(20, 0, 0, 25);
                label18.Font = MediumFont;
                label18.AutoSize = false;
                label18.Size = new Size(label18.PreferredWidth, label18.PreferredHeight);

                itemPanel.SetColumnSpan(label18, 1);
                itemPanel.SetRowSpan(label18, 1);
                itemPanel.Controls.Add(label18, 1, 19);

                ////////////////////////////////////77

                FlowLayoutPanel flowLayoutPanelEpp = new FlowLayoutPanel();
                flowLayoutPanelEpp.FlowDirection = FlowDirection.LeftToRight;
                flowLayoutPanelEpp.AutoSize = true; // Ajustar el tamaño automáticamente
                itemPanel.Controls.Add(flowLayoutPanelEpp, 1, 20);

                for (int j = 0; j < nequipo; j++)
                {
                    PictureBox epp = new PictureBox();
                    epp.Margin = new Padding(0, 0, 0, 20);
                    epp.SizeMode = PictureBoxSizeMode.StretchImage;
                    epp.Size = new Size(100, 100);

                    using (MemoryStream ms = new MemoryStream(listaEpp[j].ImagenEquipo))
                    {
                        // Crear una instancia de Image a partir del MemoryStream
                        Image imagen = Image.FromStream(ms);
                        epp.Image = imagen;
                    }
                    flowLayoutPanelEpp.Controls.Add(epp);
                }

                Label label19 = new Label();
                label19.Text = "Gavetas en donde se encuentra la sustancia: ";
                label19.Margin = new Padding(20, 0, 0, 25);
                label19.Font = MediumFont;
                label19.AutoSize = false;
                label19.Size = new Size(label19.PreferredWidth, label19.PreferredHeight);

                itemPanel.SetColumnSpan(label19, 1);
                itemPanel.SetRowSpan(label19, 1);
                itemPanel.Controls.Add(label19, 1, 21);

                /////////////////////////////

                FlowLayoutPanel flowLayoutPanelGaveta = new FlowLayoutPanel();
                flowLayoutPanelGaveta.FlowDirection = FlowDirection.LeftToRight;
                flowLayoutPanelGaveta.AutoSize = true; // Ajustar el tamaño automáticamente
                itemPanel.Controls.Add(flowLayoutPanelGaveta, 1, 22);

                for (int j = 0; j < ngaveta; j++)
                {
                    Label gaveta = new Label();
                    gaveta.Text = listaGaveta[j].NombreGaveta.ToString();
                    gaveta.Margin = new Padding(20, 0, 0, 25);
                    gaveta.Font = SmallFont;
                    gaveta.AutoSize = false;
                    gaveta.Size = new Size(gaveta.PreferredWidth, gaveta.PreferredHeight);

                    flowLayoutPanelGaveta.Controls.Add(gaveta);
                }

                ////////////////////////////

                flowLayoutPanel1.Controls.Add(itemPanel);
            }
        }
    }
}
