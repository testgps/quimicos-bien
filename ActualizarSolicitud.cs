﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaScreen
{
    public partial class ActualizarSolicitud : Form
    {
        Clases.CSolicitud cSolicitud = new Clases.CSolicitud();
        private int rol;
        private int idUsuario;
        private int indice;

        public ActualizarSolicitud(int rol, int idUsuario, int indice)
        {
            InitializeComponent();
            this.rol = rol;
            this.idUsuario = idUsuario;
            this.indice = indice;

            List<Clases.CSolicitud> lista = new List<Clases.CSolicitud>();
            lista = cSolicitud.solicitudId(indice);
            int n = cSolicitud.solicitudId(indice).Count();
            for (int i = 0; i < n; i++)
            {
                txtNombreSolicitud.Text = lista[i].NombreSolicitud.ToString();
                txtFabricanteSolicitud.Text = lista[i].FabricanteSolicitud.ToString();
                txtRutaHds.Text = lista[i].ExtensionHdsSolicitud.ToString();
                txtRutaFicha.Text = lista[i].ExtensionFichaSolicitud.ToString();
            }
        }

        private void btnHds_Click(object sender, EventArgs e)
        {
            //Abrir el File Dialog
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                txtRutaHds.Text = openFileDialog1.FileName;
        }

        private void btnFichaTecnica_Click(object sender, EventArgs e)
        {
            if (openFileDialog2.ShowDialog() == DialogResult.OK)
                txtRutaFicha.Text = openFileDialog2.FileName;
        }

        private void btnActualizarSolicitud_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombreSolicitud.Text) || string.IsNullOrWhiteSpace(txtFabricanteSolicitud.Text)
                || string.IsNullOrWhiteSpace(txtRutaHds.Text) || string.IsNullOrWhiteSpace(txtRutaFicha.Text))
            {
                MessageBox.Show("Debes de llenar todos los campos");
            }
            else
            {
                updateSolicitud();
                Solicitudes solicitudes = new Solicitudes(rol, idUsuario); //Llama al form principal de iniciar sesión
                this.Hide(); //Esconde la pantalla actual
                solicitudes.ShowDialog(); //Nos lleva a la pantalla que queremos
                this.Close(); //Cierra la pantalla actual 
            }
        }

        private void updateSolicitud()
        {
            string status = "EN REVISIÓN";
            List<Clases.CSolicitud> lista = new List<Clases.CSolicitud>();
            lista = cSolicitud.solicitudId(indice);
            int id = indice;
            lista[0].IdSolicitud = id;

            //CONSULTAMOS EL DOC E IMAGEN
            byte[] hdsDoc = lista[0].HdsSolicitud;
            string extensionHds = lista[0].ExtensionHdsSolicitud.ToString();
            cSolicitud.ExtensionHdsSolicitud = extensionHds;

            byte[] fichaDoc = lista[0].FichaSolicitud;
            string extensionFicha = lista[0].ExtensionFichaSolicitud.ToString();
            cSolicitud.ExtensionFichaSolicitud = extensionFicha;

            //MessageBox.Show("EL DOCUMENTO ES: "+ cSustancia.HdsExtension);
            //MessageBox.Show("LA IMAGEN ES: " + cSustancia.ImagenExtension);

            /////////// VALIDACIONES DE DOCUMENTOS 
            if (!txtRutaHds.Text.Equals(extensionHds) && !txtRutaFicha.Text.Equals(extensionFicha))
            {
                //MessageBox.Show("CAMBIARON LOS 2");

                Stream myStream = openFileDialog1.OpenFile();
                MemoryStream memoryStream = new MemoryStream();
                myStream.CopyTo(memoryStream);
                hdsDoc = memoryStream.ToArray();

                Stream stream = openFileDialog2.OpenFile();
                MemoryStream mmStream = new MemoryStream();
                stream.CopyTo(mmStream);
                fichaDoc = mmStream.ToArray();

                cSolicitud.HdsSolicitud = hdsDoc;
                cSolicitud.ExtensionHdsSolicitud = openFileDialog1.SafeFileName;
                cSolicitud.FichaSolicitud = fichaDoc;
                cSolicitud.ExtensionFichaSolicitud = openFileDialog2.SafeFileName;
            }
            else if (!txtRutaHds.Text.Equals(extensionHds) || !txtRutaFicha.Text.Equals(extensionFicha))
            {
                //MessageBox.Show("CAMBIÓ 1 DE LOS 2");
                if (!txtRutaHds.Text.Equals(extensionHds))
                {
                    //MessageBox.Show("CAMBIÓ SOLO LA HDS");

                    Stream myStream = openFileDialog1.OpenFile();
                    MemoryStream memoryStream = new MemoryStream();
                    myStream.CopyTo(memoryStream);
                    hdsDoc = memoryStream.ToArray();

                    cSolicitud.HdsSolicitud = hdsDoc;
                    cSolicitud.ExtensionHdsSolicitud = openFileDialog1.SafeFileName;
                    cSolicitud.FichaSolicitud = fichaDoc;
                    cSolicitud.ExtensionFichaSolicitud = extensionFicha;
                }
                else if (!txtRutaFicha.Text.Equals(extensionFicha))
                {
                    //MessageBox.Show("CAMBIÓ SOLO LA FICHA");

                    Stream stream = openFileDialog2.OpenFile();
                    MemoryStream mmStream = new MemoryStream();
                    stream.CopyTo(mmStream);
                    fichaDoc = mmStream.ToArray();

                    cSolicitud.HdsSolicitud = hdsDoc;
                    cSolicitud.ExtensionHdsSolicitud = extensionHds;
                    cSolicitud.FichaSolicitud = fichaDoc;
                    cSolicitud.ExtensionFichaSolicitud = openFileDialog2.SafeFileName;
                }
            }
            else
            {
                //MessageBox.Show("NO CAMBIÓ NINGUNO");

                cSolicitud.HdsSolicitud = hdsDoc;
                cSolicitud.ExtensionHdsSolicitud = extensionHds;
                cSolicitud.FichaSolicitud = fichaDoc;
                cSolicitud.ExtensionFichaSolicitud = extensionFicha;
            }

            cSolicitud.NombreSolicitud = txtNombreSolicitud.Text;
            cSolicitud.FabricanteSolicitud = txtFabricanteSolicitud.Text;
            DateTime dateTime = DateTime.Now;
            cSolicitud.FechaSolicitud = dateTime;
            cSolicitud.StatusSolicitud = status;
            cSolicitud.editarSolicitud(id);
        }

        private void btnCancelarSolcitud_Click(object sender, EventArgs e)
        {
            Solicitudes solicitudes = new Solicitudes(rol, idUsuario);
            this.Hide();
            solicitudes.ShowDialog();
            this.Close();
        }
    }
}
