﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaScreen
{
    public partial class AgregarGaveta : Form
    {
        Clases.CGaveta cGaveta = new Clases.CGaveta();
        Clases.CSustancia cSustancia = new Clases.CSustancia();
        public AgregarGaveta()
        {
            InitializeComponent();
            cGaveta.mostrarGaveta(dataGridView1);
            cSustancia.mostrarAreaCombo(comboBoxArea);
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            cGaveta.seleccionarGaveta(dataGridView1, txtIdGaveta, txtNombreGaveta, comboBoxArea);
        }

        private void comboBoxArea_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            int gavetaExistente = cGaveta.seleccionarGavetaExistente(txtNombreGaveta.Text);
            if (gavetaExistente == 1)
            {
                MessageBox.Show("La gaveta ya existe");
            }
            else
            {
                if (string.IsNullOrEmpty(txtNombreGaveta.Text))
                {
                    MessageBox.Show("Debes ingresar los campos correspondientes");
                }
                else
                {
                    cGaveta.NombreGaveta = txtNombreGaveta.Text;
                    cGaveta.AreaGaveta = comboBoxArea.Text;
                    cGaveta.agregarGaveta();
                    cGaveta.mostrarGaveta(dataGridView1);
                    txtIdGaveta.Text = "";
                    txtNombreGaveta.Text = "";
                }
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtIdGaveta.Text))
            {
                MessageBox.Show("Debes seleccionar una gaveta a editar");
            }
            else
            {
                if (string.IsNullOrEmpty(txtNombreGaveta.Text))
                {
                    MessageBox.Show("Debes ingresar los campos correspondientes");
                }
                else
                {
                    cGaveta.NombreGaveta = txtNombreGaveta.Text;
                    cGaveta.AreaGaveta = comboBoxArea.Text;
                    cGaveta.editarGaveta(txtIdGaveta);
                    cGaveta.mostrarGaveta(dataGridView1);
                    txtIdGaveta.Text = "";
                    txtNombreGaveta.Text = "";
                    MessageBox.Show("Gaveta modificada correctamente");
                }
            }
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtIdGaveta.Text))
            {
                MessageBox.Show("Debes seleccionar una gaveta");
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("¿Seguro que deseas eliminar " + txtNombreGaveta.Text + "?",
                "Eliminar gaveta", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    cGaveta.eliminarGaveta(txtIdGaveta);
                    cGaveta.mostrarGaveta(dataGridView1);
                    txtIdGaveta.Text = "";
                    txtNombreGaveta.Text = "";
                }
            }
        }
    }
}
