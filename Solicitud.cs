﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaScreen
{
    public partial class Solicitud : Form
    {
        Clases.CSolicitud cSolicitud = new Clases.CSolicitud();
        private int rol;
        private int idUsuario;

        public Solicitud(int rol, int idUsuario)
        {
            InitializeComponent();
            this.rol = rol;
            //MessageBox.Show("EL ROL ES: " + rol);
            this.idUsuario = idUsuario;
        }

        private void btnCancelarSolcitud_Click(object sender, EventArgs e)
        {
            Solicitudes solicitudes = new Solicitudes(rol, idUsuario);
            this.Hide();
            solicitudes.ShowDialog();
            this.Close();
        }

        private void btnHds_Click(object sender, EventArgs e)
        {
            //Abrir el File Dialog
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                txtRutaHds.Text = openFileDialog1.FileName;
        }

        private void btnFichaTecnica_Click(object sender, EventArgs e)
        {
            //Abrir el File Dialog
            if (openFileDialog2.ShowDialog() == DialogResult.OK)
                txtRutaFicha.Text = openFileDialog2.FileName;
        }

        private async void btnEnviarSolicitud_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombreSolicitud.Text) || string.IsNullOrWhiteSpace(txtFabricanteSolicitud.Text)
                || string.IsNullOrWhiteSpace(txtRutaFicha.Text) || string.IsNullOrWhiteSpace(txtRutaHds.Text))
            {
                MessageBox.Show("Debes de ingresar todos los campos");
            }
            else
            {
                Task oTask = new Task(addSolicitud);
                oTask.Start();
                await oTask;
                Solicitudes solicitudes = new Solicitudes(rol, idUsuario);
                this.Hide();
                solicitudes.ShowDialog();
                this.Close();
            }
        }

        public void addSolicitud()
        {
            DateTime dateTime = DateTime.Now;
            string status = "EN REVISIÓN";
            byte[] hdsSolicitud = null;
            Stream myStream = openFileDialog1.OpenFile();
            MemoryStream memoryStream = new MemoryStream();
            myStream.CopyTo(memoryStream);
            hdsSolicitud = memoryStream.ToArray();

            byte[] fichaSolicitud = null;
            Stream stream = openFileDialog2.OpenFile();
            MemoryStream memory = new MemoryStream();
            stream.CopyTo(memory);
            fichaSolicitud = memory.ToArray();

            cSolicitud.NombreSolicitud = txtNombreSolicitud.Text;
            cSolicitud.FabricanteSolicitud = txtFabricanteSolicitud.Text;
            cSolicitud.ExtensionHdsSolicitud = openFileDialog1.SafeFileName;
            cSolicitud.HdsSolicitud = hdsSolicitud;
            cSolicitud.ExtensionFichaSolicitud = openFileDialog2.SafeFileName;
            cSolicitud.FichaSolicitud = fichaSolicitud;
            cSolicitud.FechaSolicitud = dateTime;
            cSolicitud.StatusSolicitud = status;
            //Se agrega la solicitud
            cSolicitud.agregarSolicitud();
        }
    }
}
