﻿namespace PruebaScreen
{
    partial class Agregar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Agregar));
            openFileDialog1 = new OpenFileDialog();
            openFileDialog2 = new OpenFileDialog();
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            label5 = new Label();
            label7 = new Label();
            label8 = new Label();
            label9 = new Label();
            label10 = new Label();
            txtUsos = new TextBox();
            txtNombreSustancia = new TextBox();
            comboContenedor = new ComboBox();
            txtCantidad = new TextBox();
            txtConsumos = new TextBox();
            txtRiesgos = new TextBox();
            label4 = new Label();
            checkBoxTapones = new CheckBox();
            label11 = new Label();
            label12 = new Label();
            comboPalabraAdvertencia = new ComboBox();
            label13 = new Label();
            checkBoxLentes = new CheckBox();
            checkBoxBota = new CheckBox();
            checkBoxInflamable = new CheckBox();
            checkBoxExplosivos = new CheckBox();
            btnAgregarhds = new Button();
            label14 = new Label();
            btnAgregarfoto = new Button();
            btnAgregar = new Button();
            btnCancelar = new Button();
            checkBoxOxidante = new CheckBox();
            checkBoxGasPresion = new CheckBox();
            checkBoxCorrosion = new CheckBox();
            checkBoxToxico = new CheckBox();
            checkBoxIrritante = new CheckBox();
            checkBoxSalud = new CheckBox();
            checkBoxAmbiente = new CheckBox();
            label15 = new Label();
            txtTelEmergencia = new TextBox();
            label16 = new Label();
            txtNumeroCas = new TextBox();
            label17 = new Label();
            label6 = new Label();
            txtNombreFabricante = new TextBox();
            txtTelFabricante = new TextBox();
            label18 = new Label();
            label19 = new Label();
            txtDirFabricante = new TextBox();
            txtNumeroOnu = new TextBox();
            label20 = new Label();
            chBoxGaveta = new CheckedListBox();
            checkBoxGuantes = new CheckBox();
            checkBoxMascarilla = new CheckBox();
            checkBoxTraje = new CheckBox();
            txtRutaHds = new TextBox();
            txtRutaFoto = new TextBox();
            label21 = new Label();
            label22 = new Label();
            checkedListArea = new CheckedListBox();
            SuspendLayout();
            // 
            // openFileDialog1
            // 
            openFileDialog1.FileName = "openFileDialog1";
            openFileDialog1.Filter = "PDF (*.pdf)|*.pdf";
            // 
            // openFileDialog2
            // 
            openFileDialog2.FileName = "openFileDialog2";
            openFileDialog2.Filter = "Image Files|*.jpg;*.jpeg;*.png;";
            // 
            // label1
            // 
            label1.Anchor = AnchorStyles.Top;
            label1.Font = new Font("Siemens Sans", 22F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(346, 19);
            label1.Name = "label1";
            label1.Size = new Size(389, 53);
            label1.TabIndex = 62;
            label1.Text = "Agregar sustancia";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label2.Location = new Point(40, 135);
            label2.Name = "label2";
            label2.Size = new Size(119, 48);
            label2.TabIndex = 63;
            label2.Text = "Nombre de\r\nla sustancia";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label3.Location = new Point(40, 256);
            label3.Name = "label3";
            label3.Size = new Size(148, 48);
            label3.TabIndex = 64;
            label3.Text = "Usos\r\nrecomendados";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label5.Location = new Point(40, 1036);
            label5.Name = "label5";
            label5.Size = new Size(53, 24);
            label5.TabIndex = 65;
            label5.Text = "Área";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label7.Location = new Point(40, 517);
            label7.Name = "label7";
            label7.Size = new Size(95, 48);
            label7.TabIndex = 67;
            label7.Text = "Cantidad\r\nestimada";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label8.Location = new Point(619, 376);
            label8.Name = "label8";
            label8.Size = new Size(117, 48);
            label8.TabIndex = 68;
            label8.Text = "Tipo de\r\ncontenedor";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label9.Location = new Point(619, 517);
            label9.Name = "label9";
            label9.Size = new Size(110, 48);
            label9.TabIndex = 69;
            label9.Text = "Consumos\r\npromedios";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label10.Location = new Point(40, 1281);
            label10.Name = "label10";
            label10.Size = new Size(81, 24);
            label10.TabIndex = 70;
            label10.Text = "Riesgos";
            // 
            // txtUsos
            // 
            txtUsos.Location = new Point(212, 235);
            txtUsos.Multiline = true;
            txtUsos.Name = "txtUsos";
            txtUsos.ScrollBars = ScrollBars.Vertical;
            txtUsos.Size = new Size(897, 87);
            txtUsos.TabIndex = 2;
            // 
            // txtNombreSustancia
            // 
            txtNombreSustancia.Location = new Point(212, 152);
            txtNombreSustancia.Name = "txtNombreSustancia";
            txtNombreSustancia.Size = new Size(897, 31);
            txtNombreSustancia.TabIndex = 1;
            // 
            // comboContenedor
            // 
            comboContenedor.DropDownStyle = ComboBoxStyle.DropDownList;
            comboContenedor.FormattingEnabled = true;
            comboContenedor.Items.AddRange(new object[] { "LATA", "BOTELLA", "CUBETA", "GARRAFA", "CILINDRO", "COSTAL", "ENVASE" });
            comboContenedor.Location = new Point(742, 391);
            comboContenedor.Name = "comboContenedor";
            comboContenedor.Size = new Size(367, 33);
            comboContenedor.TabIndex = 4;
            // 
            // txtCantidad
            // 
            txtCantidad.Location = new Point(212, 531);
            txtCantidad.Name = "txtCantidad";
            txtCantidad.Size = new Size(367, 31);
            txtCantidad.TabIndex = 5;
            // 
            // txtConsumos
            // 
            txtConsumos.Location = new Point(742, 534);
            txtConsumos.Name = "txtConsumos";
            txtConsumos.Size = new Size(367, 31);
            txtConsumos.TabIndex = 6;
            // 
            // txtRiesgos
            // 
            txtRiesgos.Location = new Point(212, 1251);
            txtRiesgos.Multiline = true;
            txtRiesgos.Name = "txtRiesgos";
            txtRiesgos.Size = new Size(897, 91);
            txtRiesgos.TabIndex = 15;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label4.Location = new Point(40, 678);
            label4.Name = "label4";
            label4.Size = new Size(83, 24);
            label4.TabIndex = 82;
            label4.Text = "Uso EPP";
            // 
            // checkBoxTapones
            // 
            checkBoxTapones.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxTapones.Image = (Image)resources.GetObject("checkBoxTapones.Image");
            checkBoxTapones.ImageAlign = ContentAlignment.TopCenter;
            checkBoxTapones.Location = new Point(212, 616);
            checkBoxTapones.Name = "checkBoxTapones";
            checkBoxTapones.Size = new Size(150, 150);
            checkBoxTapones.TabIndex = 7;
            checkBoxTapones.Text = "Tapones auditivos";
            checkBoxTapones.TextAlign = ContentAlignment.BottomCenter;
            checkBoxTapones.UseVisualStyleBackColor = true;
            checkBoxTapones.CheckedChanged += checkBoxTapones_CheckedChanged;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label11.Location = new Point(40, 376);
            label11.Name = "label11";
            label11.Size = new Size(120, 48);
            label11.TabIndex = 84;
            label11.Text = "Palabra de \r\nadvertencia";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label12.Location = new Point(626, 1036);
            label12.Name = "label12";
            label12.Size = new Size(75, 24);
            label12.TabIndex = 85;
            label12.Text = "Gaveta";
            // 
            // comboPalabraAdvertencia
            // 
            comboPalabraAdvertencia.DropDownStyle = ComboBoxStyle.DropDownList;
            comboPalabraAdvertencia.FormattingEnabled = true;
            comboPalabraAdvertencia.Items.AddRange(new object[] { "", "ATENCIÓN", "PELIGRO" });
            comboPalabraAdvertencia.Location = new Point(212, 391);
            comboPalabraAdvertencia.Name = "comboPalabraAdvertencia";
            comboPalabraAdvertencia.Size = new Size(367, 33);
            comboPalabraAdvertencia.TabIndex = 3;
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label13.Location = new Point(40, 2026);
            label13.Name = "label13";
            label13.Size = new Size(125, 24);
            label13.TabIndex = 89;
            label13.Text = "Pictogramas";
            // 
            // checkBoxLentes
            // 
            checkBoxLentes.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxLentes.Image = (Image)resources.GetObject("checkBoxLentes.Image");
            checkBoxLentes.ImageAlign = ContentAlignment.TopCenter;
            checkBoxLentes.Location = new Point(566, 616);
            checkBoxLentes.Name = "checkBoxLentes";
            checkBoxLentes.Size = new Size(150, 150);
            checkBoxLentes.TabIndex = 8;
            checkBoxLentes.Text = "Lentes de seguridad";
            checkBoxLentes.TextAlign = ContentAlignment.BottomCenter;
            checkBoxLentes.UseVisualStyleBackColor = true;
            // 
            // checkBoxBota
            // 
            checkBoxBota.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxBota.ForeColor = SystemColors.ControlText;
            checkBoxBota.Image = (Image)resources.GetObject("checkBoxBota.Image");
            checkBoxBota.ImageAlign = ContentAlignment.TopCenter;
            checkBoxBota.Location = new Point(903, 616);
            checkBoxBota.Name = "checkBoxBota";
            checkBoxBota.Size = new Size(150, 150);
            checkBoxBota.TabIndex = 9;
            checkBoxBota.Text = "Zapatos de seguridad";
            checkBoxBota.TextAlign = ContentAlignment.BottomCenter;
            checkBoxBota.UseVisualStyleBackColor = true;
            // 
            // checkBoxInflamable
            // 
            checkBoxInflamable.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxInflamable.Image = (Image)resources.GetObject("checkBoxInflamable.Image");
            checkBoxInflamable.ImageAlign = ContentAlignment.TopCenter;
            checkBoxInflamable.Location = new Point(262, 2083);
            checkBoxInflamable.Name = "checkBoxInflamable";
            checkBoxInflamable.Size = new Size(150, 150);
            checkBoxInflamable.TabIndex = 23;
            checkBoxInflamable.Text = "Inflamable";
            checkBoxInflamable.TextAlign = ContentAlignment.BottomCenter;
            checkBoxInflamable.UseVisualStyleBackColor = true;
            // 
            // checkBoxExplosivos
            // 
            checkBoxExplosivos.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxExplosivos.Image = (Image)resources.GetObject("checkBoxExplosivos.Image");
            checkBoxExplosivos.ImageAlign = ContentAlignment.TopCenter;
            checkBoxExplosivos.Location = new Point(40, 2083);
            checkBoxExplosivos.Name = "checkBoxExplosivos";
            checkBoxExplosivos.Size = new Size(150, 150);
            checkBoxExplosivos.TabIndex = 22;
            checkBoxExplosivos.Text = "Peligro de explosivos";
            checkBoxExplosivos.TextAlign = ContentAlignment.BottomCenter;
            checkBoxExplosivos.UseVisualStyleBackColor = true;
            // 
            // btnAgregarhds
            // 
            btnAgregarhds.BackColor = Color.FromArgb(80, 190, 215);
            btnAgregarhds.FlatAppearance.BorderSize = 0;
            btnAgregarhds.FlatStyle = FlatStyle.Flat;
            btnAgregarhds.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnAgregarhds.ForeColor = Color.White;
            btnAgregarhds.Location = new Point(1046, 2506);
            btnAgregarhds.Name = "btnAgregarhds";
            btnAgregarhds.Size = new Size(63, 31);
            btnAgregarhds.TabIndex = 31;
            btnAgregarhds.Text = "...";
            btnAgregarhds.UseVisualStyleBackColor = false;
            btnAgregarhds.Click += btnAgregarhds_Click;
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new Point(1010, 2753);
            label14.Name = "label14";
            label14.Size = new Size(0, 25);
            label14.TabIndex = 96;
            // 
            // btnAgregarfoto
            // 
            btnAgregarfoto.BackColor = Color.FromArgb(80, 190, 215);
            btnAgregarfoto.FlatAppearance.BorderSize = 0;
            btnAgregarfoto.FlatStyle = FlatStyle.Flat;
            btnAgregarfoto.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnAgregarfoto.ForeColor = Color.White;
            btnAgregarfoto.Location = new Point(1046, 2600);
            btnAgregarfoto.Name = "btnAgregarfoto";
            btnAgregarfoto.Size = new Size(63, 31);
            btnAgregarfoto.TabIndex = 32;
            btnAgregarfoto.Text = "...";
            btnAgregarfoto.UseVisualStyleBackColor = false;
            btnAgregarfoto.Click += btnAgregarfoto_Click;
            // 
            // btnAgregar
            // 
            btnAgregar.BackColor = Color.FromArgb(0, 153, 153);
            btnAgregar.FlatAppearance.BorderSize = 0;
            btnAgregar.FlatStyle = FlatStyle.Flat;
            btnAgregar.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnAgregar.ForeColor = Color.White;
            btnAgregar.Location = new Point(937, 2700);
            btnAgregar.Name = "btnAgregar";
            btnAgregar.Size = new Size(172, 50);
            btnAgregar.TabIndex = 33;
            btnAgregar.Text = "Agregar";
            btnAgregar.UseVisualStyleBackColor = false;
            btnAgregar.Click += btnAgregar_Click;
            // 
            // btnCancelar
            // 
            btnCancelar.BackColor = Color.FromArgb(130, 30, 80);
            btnCancelar.FlatAppearance.BorderSize = 0;
            btnCancelar.FlatStyle = FlatStyle.Flat;
            btnCancelar.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnCancelar.ForeColor = Color.White;
            btnCancelar.Location = new Point(692, 2700);
            btnCancelar.Name = "btnCancelar";
            btnCancelar.Size = new Size(172, 50);
            btnCancelar.TabIndex = 34;
            btnCancelar.Text = "Cancelar";
            btnCancelar.UseVisualStyleBackColor = false;
            btnCancelar.Click += btnCancelar_Click;
            // 
            // checkBoxOxidante
            // 
            checkBoxOxidante.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxOxidante.Image = (Image)resources.GetObject("checkBoxOxidante.Image");
            checkBoxOxidante.ImageAlign = ContentAlignment.TopCenter;
            checkBoxOxidante.Location = new Point(504, 2083);
            checkBoxOxidante.Name = "checkBoxOxidante";
            checkBoxOxidante.Size = new Size(150, 150);
            checkBoxOxidante.TabIndex = 24;
            checkBoxOxidante.Text = "Oxidante";
            checkBoxOxidante.TextAlign = ContentAlignment.BottomCenter;
            checkBoxOxidante.UseVisualStyleBackColor = true;
            // 
            // checkBoxGasPresion
            // 
            checkBoxGasPresion.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxGasPresion.Image = (Image)resources.GetObject("checkBoxGasPresion.Image");
            checkBoxGasPresion.ImageAlign = ContentAlignment.TopCenter;
            checkBoxGasPresion.Location = new Point(751, 2083);
            checkBoxGasPresion.Name = "checkBoxGasPresion";
            checkBoxGasPresion.Size = new Size(150, 150);
            checkBoxGasPresion.TabIndex = 25;
            checkBoxGasPresion.Text = "Gas bajo presión";
            checkBoxGasPresion.TextAlign = ContentAlignment.BottomCenter;
            checkBoxGasPresion.UseVisualStyleBackColor = true;
            // 
            // checkBoxCorrosion
            // 
            checkBoxCorrosion.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxCorrosion.Image = (Image)resources.GetObject("checkBoxCorrosion.Image");
            checkBoxCorrosion.ImageAlign = ContentAlignment.TopCenter;
            checkBoxCorrosion.Location = new Point(959, 2083);
            checkBoxCorrosion.Name = "checkBoxCorrosion";
            checkBoxCorrosion.Size = new Size(150, 150);
            checkBoxCorrosion.TabIndex = 26;
            checkBoxCorrosion.Text = "Corrosión";
            checkBoxCorrosion.TextAlign = ContentAlignment.BottomCenter;
            checkBoxCorrosion.UseVisualStyleBackColor = true;
            // 
            // checkBoxToxico
            // 
            checkBoxToxico.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxToxico.Image = (Image)resources.GetObject("checkBoxToxico.Image");
            checkBoxToxico.ImageAlign = ContentAlignment.TopCenter;
            checkBoxToxico.Location = new Point(151, 2269);
            checkBoxToxico.Name = "checkBoxToxico";
            checkBoxToxico.Size = new Size(150, 150);
            checkBoxToxico.TabIndex = 27;
            checkBoxToxico.Text = "Tóxico";
            checkBoxToxico.TextAlign = ContentAlignment.BottomCenter;
            checkBoxToxico.UseVisualStyleBackColor = true;
            // 
            // checkBoxIrritante
            // 
            checkBoxIrritante.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxIrritante.Image = (Image)resources.GetObject("checkBoxIrritante.Image");
            checkBoxIrritante.ImageAlign = ContentAlignment.TopCenter;
            checkBoxIrritante.Location = new Point(384, 2269);
            checkBoxIrritante.Name = "checkBoxIrritante";
            checkBoxIrritante.Size = new Size(150, 150);
            checkBoxIrritante.TabIndex = 28;
            checkBoxIrritante.Text = "Irritante - Nocivo";
            checkBoxIrritante.TextAlign = ContentAlignment.BottomCenter;
            checkBoxIrritante.UseVisualStyleBackColor = true;
            // 
            // checkBoxSalud
            // 
            checkBoxSalud.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxSalud.Image = (Image)resources.GetObject("checkBoxSalud.Image");
            checkBoxSalud.ImageAlign = ContentAlignment.TopCenter;
            checkBoxSalud.Location = new Point(631, 2269);
            checkBoxSalud.Name = "checkBoxSalud";
            checkBoxSalud.Size = new Size(150, 150);
            checkBoxSalud.TabIndex = 29;
            checkBoxSalud.Text = "Peligro para la salud";
            checkBoxSalud.TextAlign = ContentAlignment.BottomCenter;
            checkBoxSalud.UseVisualStyleBackColor = true;
            // 
            // checkBoxAmbiente
            // 
            checkBoxAmbiente.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxAmbiente.Image = (Image)resources.GetObject("checkBoxAmbiente.Image");
            checkBoxAmbiente.ImageAlign = ContentAlignment.TopCenter;
            checkBoxAmbiente.Location = new Point(848, 2269);
            checkBoxAmbiente.Name = "checkBoxAmbiente";
            checkBoxAmbiente.Size = new Size(150, 150);
            checkBoxAmbiente.TabIndex = 30;
            checkBoxAmbiente.Text = "Daño al medio ambiente";
            checkBoxAmbiente.TextAlign = ContentAlignment.BottomCenter;
            checkBoxAmbiente.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            label15.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label15.Location = new Point(40, 1406);
            label15.Name = "label15";
            label15.Size = new Size(137, 60);
            label15.TabIndex = 109;
            label15.Text = "Teléfono de emergencia";
            // 
            // txtTelEmergencia
            // 
            txtTelEmergencia.Location = new Point(212, 1417);
            txtTelEmergencia.Name = "txtTelEmergencia";
            txtTelEmergencia.Size = new Size(352, 31);
            txtTelEmergencia.TabIndex = 16;
            // 
            // label16
            // 
            label16.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label16.Location = new Point(612, 1406);
            label16.Name = "label16";
            label16.Size = new Size(104, 49);
            label16.TabIndex = 111;
            label16.Text = "Número de CAS";
            // 
            // txtNumeroCas
            // 
            txtNumeroCas.Location = new Point(757, 1417);
            txtNumeroCas.Name = "txtNumeroCas";
            txtNumeroCas.Size = new Size(352, 31);
            txtNumeroCas.TabIndex = 17;
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.Font = new Font("Siemens Sans", 12F, FontStyle.Bold, GraphicsUnit.Point);
            label17.Location = new Point(504, 1657);
            label17.Name = "label17";
            label17.Size = new Size(238, 29);
            label17.TabIndex = 113;
            label17.Text = "Datos del fabricante";
            // 
            // label6
            // 
            label6.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label6.Location = new Point(40, 1732);
            label6.Name = "label6";
            label6.Size = new Size(137, 57);
            label6.TabIndex = 114;
            label6.Text = "Nombre del fabricante";
            // 
            // txtNombreFabricante
            // 
            txtNombreFabricante.Location = new Point(212, 1743);
            txtNombreFabricante.Name = "txtNombreFabricante";
            txtNombreFabricante.Size = new Size(352, 31);
            txtNombreFabricante.TabIndex = 19;
            // 
            // txtTelFabricante
            // 
            txtTelFabricante.Location = new Point(757, 1743);
            txtTelFabricante.Name = "txtTelFabricante";
            txtTelFabricante.Size = new Size(352, 31);
            txtTelFabricante.TabIndex = 20;
            // 
            // label18
            // 
            label18.AutoSize = true;
            label18.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label18.Location = new Point(612, 1743);
            label18.Name = "label18";
            label18.Size = new Size(91, 24);
            label18.TabIndex = 117;
            label18.Text = "Teléfono";
            // 
            // label19
            // 
            label19.AutoSize = true;
            label19.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label19.Location = new Point(40, 1864);
            label19.Name = "label19";
            label19.Size = new Size(99, 24);
            label19.TabIndex = 118;
            label19.Text = "Dirección";
            // 
            // txtDirFabricante
            // 
            txtDirFabricante.Location = new Point(212, 1860);
            txtDirFabricante.Multiline = true;
            txtDirFabricante.Name = "txtDirFabricante";
            txtDirFabricante.ScrollBars = ScrollBars.Vertical;
            txtDirFabricante.Size = new Size(897, 113);
            txtDirFabricante.TabIndex = 21;
            // 
            // txtNumeroOnu
            // 
            txtNumeroOnu.Location = new Point(212, 1539);
            txtNumeroOnu.Name = "txtNumeroOnu";
            txtNumeroOnu.Size = new Size(352, 31);
            txtNumeroOnu.TabIndex = 18;
            // 
            // label20
            // 
            label20.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label20.Location = new Point(40, 1527);
            label20.Name = "label20";
            label20.Size = new Size(104, 56);
            label20.TabIndex = 121;
            label20.Text = "Número de ONU";
            // 
            // chBoxGaveta
            // 
            chBoxGaveta.FormattingEnabled = true;
            chBoxGaveta.Location = new Point(757, 1025);
            chBoxGaveta.Name = "chBoxGaveta";
            chBoxGaveta.Size = new Size(352, 172);
            chBoxGaveta.TabIndex = 14;
            // 
            // checkBoxGuantes
            // 
            checkBoxGuantes.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxGuantes.Image = (Image)resources.GetObject("checkBoxGuantes.Image");
            checkBoxGuantes.ImageAlign = ContentAlignment.TopCenter;
            checkBoxGuantes.Location = new Point(212, 805);
            checkBoxGuantes.Name = "checkBoxGuantes";
            checkBoxGuantes.Size = new Size(150, 150);
            checkBoxGuantes.TabIndex = 10;
            checkBoxGuantes.Text = "Guantes";
            checkBoxGuantes.TextAlign = ContentAlignment.BottomCenter;
            checkBoxGuantes.UseVisualStyleBackColor = true;
            // 
            // checkBoxMascarilla
            // 
            checkBoxMascarilla.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxMascarilla.Image = (Image)resources.GetObject("checkBoxMascarilla.Image");
            checkBoxMascarilla.ImageAlign = ContentAlignment.TopCenter;
            checkBoxMascarilla.Location = new Point(566, 805);
            checkBoxMascarilla.Name = "checkBoxMascarilla";
            checkBoxMascarilla.Size = new Size(150, 150);
            checkBoxMascarilla.TabIndex = 11;
            checkBoxMascarilla.Text = "Mascarilla";
            checkBoxMascarilla.TextAlign = ContentAlignment.BottomCenter;
            checkBoxMascarilla.UseVisualStyleBackColor = true;
            // 
            // checkBoxTraje
            // 
            checkBoxTraje.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxTraje.Image = (Image)resources.GetObject("checkBoxTraje.Image");
            checkBoxTraje.ImageAlign = ContentAlignment.TopCenter;
            checkBoxTraje.Location = new Point(903, 805);
            checkBoxTraje.Name = "checkBoxTraje";
            checkBoxTraje.Size = new Size(150, 150);
            checkBoxTraje.TabIndex = 12;
            checkBoxTraje.Text = "Traje de seguridad";
            checkBoxTraje.TextAlign = ContentAlignment.BottomCenter;
            checkBoxTraje.UseVisualStyleBackColor = true;
            // 
            // txtRutaHds
            // 
            txtRutaHds.Enabled = false;
            txtRutaHds.Location = new Point(178, 2506);
            txtRutaHds.Name = "txtRutaHds";
            txtRutaHds.ReadOnly = true;
            txtRutaHds.Size = new Size(845, 31);
            txtRutaHds.TabIndex = 126;
            // 
            // txtRutaFoto
            // 
            txtRutaFoto.Enabled = false;
            txtRutaFoto.Location = new Point(178, 2600);
            txtRutaFoto.Name = "txtRutaFoto";
            txtRutaFoto.ReadOnly = true;
            txtRutaFoto.Size = new Size(845, 31);
            txtRutaFoto.TabIndex = 127;
            // 
            // label21
            // 
            label21.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label21.Location = new Point(40, 2506);
            label21.Name = "label21";
            label21.Size = new Size(104, 27);
            label21.TabIndex = 128;
            label21.Text = "Subir HDS";
            // 
            // label22
            // 
            label22.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label22.Location = new Point(40, 2600);
            label22.Name = "label22";
            label22.Size = new Size(110, 31);
            label22.TabIndex = 129;
            label22.Text = "Subir foto";
            // 
            // checkedListArea
            // 
            checkedListArea.FormattingEnabled = true;
            checkedListArea.Location = new Point(212, 1025);
            checkedListArea.Name = "checkedListArea";
            checkedListArea.Size = new Size(352, 172);
            checkedListArea.TabIndex = 13;
            // 
            // Agregar
            // 
            AutoScaleDimensions = new SizeF(10F, 25F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoScroll = true;
            AutoSizeMode = AutoSizeMode.GrowAndShrink;
            ClientSize = new Size(1168, 664);
            Controls.Add(checkedListArea);
            Controls.Add(label22);
            Controls.Add(label21);
            Controls.Add(txtRutaFoto);
            Controls.Add(txtRutaHds);
            Controls.Add(checkBoxTraje);
            Controls.Add(checkBoxMascarilla);
            Controls.Add(checkBoxGuantes);
            Controls.Add(chBoxGaveta);
            Controls.Add(label20);
            Controls.Add(txtNumeroOnu);
            Controls.Add(txtDirFabricante);
            Controls.Add(label19);
            Controls.Add(label18);
            Controls.Add(txtTelFabricante);
            Controls.Add(txtNombreFabricante);
            Controls.Add(label6);
            Controls.Add(label17);
            Controls.Add(txtNumeroCas);
            Controls.Add(label16);
            Controls.Add(txtTelEmergencia);
            Controls.Add(label15);
            Controls.Add(checkBoxAmbiente);
            Controls.Add(checkBoxSalud);
            Controls.Add(checkBoxIrritante);
            Controls.Add(checkBoxToxico);
            Controls.Add(checkBoxCorrosion);
            Controls.Add(checkBoxGasPresion);
            Controls.Add(checkBoxOxidante);
            Controls.Add(btnCancelar);
            Controls.Add(btnAgregar);
            Controls.Add(btnAgregarfoto);
            Controls.Add(label14);
            Controls.Add(btnAgregarhds);
            Controls.Add(checkBoxExplosivos);
            Controls.Add(checkBoxInflamable);
            Controls.Add(checkBoxBota);
            Controls.Add(checkBoxLentes);
            Controls.Add(label13);
            Controls.Add(comboPalabraAdvertencia);
            Controls.Add(label12);
            Controls.Add(label11);
            Controls.Add(checkBoxTapones);
            Controls.Add(label4);
            Controls.Add(txtRiesgos);
            Controls.Add(txtConsumos);
            Controls.Add(txtCantidad);
            Controls.Add(comboContenedor);
            Controls.Add(txtNombreSustancia);
            Controls.Add(txtUsos);
            Controls.Add(label10);
            Controls.Add(label9);
            Controls.Add(label8);
            Controls.Add(label7);
            Controls.Add(label5);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "Agregar";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Agregar";
            Load += Agregar_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private OpenFileDialog openFileDialog1;
        private OpenFileDialog openFileDialog2;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label5;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label label10;
        private TextBox txtUsos;
        private TextBox txtNombreSustancia;
        private ComboBox comboContenedor;
        private TextBox txtCantidad;
        private TextBox txtConsumos;
        private TextBox txtRiesgos;
        private Label label4;
        private CheckBox checkBoxTapones;
        private Label label11;
        private Label label12;
        private ComboBox comboPalabraAdvertencia;
        private Label label13;
        private CheckBox checkBoxLentes;
        private CheckBox checkBoxBota;
        private CheckBox checkBoxInflamable;
        private CheckBox checkBoxExplosivos;
        private Button btnAgregarhds;
        private Label label14;
        private Button btnAgregarfoto;
        private Button btnAgregar;
        private Button btnCancelar;
        private CheckBox checkBoxOxidante;
        private CheckBox checkBoxGasPresion;
        private CheckBox checkBoxCorrosion;
        private CheckBox checkBoxToxico;
        private CheckBox checkBoxIrritante;
        private CheckBox checkBoxSalud;
        private CheckBox checkBoxAmbiente;
        private Label label15;
        private TextBox txtTelEmergencia;
        private Label label16;
        private TextBox txtNumeroCas;
        private Label label17;
        private Label label6;
        private TextBox txtNombreFabricante;
        private TextBox txtTelFabricante;
        private Label label18;
        private Label label19;
        private TextBox txtDirFabricante;
        private TextBox txtNumeroOnu;
        private Label label20;
        private CheckedListBox chBoxGaveta;
        private CheckBox checkBoxGuantes;
        private CheckBox checkBoxMascarilla;
        private CheckBox checkBoxTraje;
        private TextBox txtRutaHds;
        private TextBox txtRutaFoto;
        private Label label21;
        private Label label22;
        private CheckedListBox checkedListArea;
    }
}