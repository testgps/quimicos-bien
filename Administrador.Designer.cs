﻿namespace PruebaScreen
{
    partial class Administrador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Administrador));
            btnSalir = new PictureBox();
            label4 = new Label();
            txtBuscar = new TextBox();
            btnBuscar = new PictureBox();
            panel1 = new Panel();
            checkBoxGaveta = new CheckBox();
            checkBoxArea = new CheckBox();
            btnEliminarFiltro = new Button();
            btnFiltro = new Button();
            cmbBoxArea = new ComboBox();
            cmbBoxGaveta = new ComboBox();
            label11 = new Label();
            btnAgregar = new Button();
            btnSolicitudes = new Button();
            panel2 = new Panel();
            lblSolicitudes = new Label();
            btnAddArea = new Button();
            btnAddGaveta = new Button();
            flowLayoutPanel1 = new FlowLayoutPanel();
            btnCargar = new Button();
            btnCargarFiltro = new Button();
            btnCargarBuscar = new Button();
            ((System.ComponentModel.ISupportInitialize)btnSalir).BeginInit();
            ((System.ComponentModel.ISupportInitialize)btnBuscar).BeginInit();
            panel1.SuspendLayout();
            panel2.SuspendLayout();
            SuspendLayout();
            // 
            // btnSalir
            // 
            btnSalir.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            btnSalir.BackColor = Color.Transparent;
            btnSalir.Cursor = Cursors.Hand;
            btnSalir.Image = (Image)resources.GetObject("btnSalir.Image");
            btnSalir.Location = new Point(1254, 18);
            btnSalir.Name = "btnSalir";
            btnSalir.Size = new Size(37, 39);
            btnSalir.SizeMode = PictureBoxSizeMode.StretchImage;
            btnSalir.TabIndex = 34;
            btnSalir.TabStop = false;
            btnSalir.Click += btnSalir_Click_1;
            // 
            // label4
            // 
            label4.Anchor = AnchorStyles.Top;
            label4.BackColor = Color.Transparent;
            label4.Font = new Font("Siemens Sans", 22F, FontStyle.Bold, GraphicsUnit.Point);
            label4.Location = new Point(435, 7);
            label4.Name = "label4";
            label4.Size = new Size(457, 53);
            label4.TabIndex = 26;
            label4.Text = "Listado de Sustancias";
            // 
            // txtBuscar
            // 
            txtBuscar.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            txtBuscar.Location = new Point(147, 63);
            txtBuscar.Name = "txtBuscar";
            txtBuscar.Size = new Size(975, 31);
            txtBuscar.TabIndex = 36;
            txtBuscar.TextChanged += txtBuscar_TextChanged;
            // 
            // btnBuscar
            // 
            btnBuscar.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            btnBuscar.Cursor = Cursors.Hand;
            btnBuscar.Image = (Image)resources.GetObject("btnBuscar.Image");
            btnBuscar.Location = new Point(1157, 56);
            btnBuscar.Name = "btnBuscar";
            btnBuscar.Size = new Size(40, 40);
            btnBuscar.SizeMode = PictureBoxSizeMode.StretchImage;
            btnBuscar.TabIndex = 37;
            btnBuscar.TabStop = false;
            btnBuscar.Click += btnBuscar_Click;
            // 
            // panel1
            // 
            panel1.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            panel1.Controls.Add(checkBoxGaveta);
            panel1.Controls.Add(checkBoxArea);
            panel1.Controls.Add(btnEliminarFiltro);
            panel1.Controls.Add(btnFiltro);
            panel1.Controls.Add(cmbBoxArea);
            panel1.Controls.Add(cmbBoxGaveta);
            panel1.Controls.Add(label11);
            panel1.Controls.Add(label4);
            panel1.Controls.Add(btnSalir);
            panel1.Controls.Add(txtBuscar);
            panel1.Controls.Add(btnBuscar);
            panel1.Location = new Point(0, 0);
            panel1.Name = "panel1";
            panel1.Size = new Size(1333, 174);
            panel1.TabIndex = 44;
            // 
            // checkBoxGaveta
            // 
            checkBoxGaveta.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxGaveta.ImageAlign = ContentAlignment.TopCenter;
            checkBoxGaveta.Location = new Point(574, 115);
            checkBoxGaveta.Name = "checkBoxGaveta";
            checkBoxGaveta.Size = new Size(105, 48);
            checkBoxGaveta.TabIndex = 90;
            checkBoxGaveta.Text = "Activar gavetas";
            checkBoxGaveta.TextAlign = ContentAlignment.MiddleCenter;
            checkBoxGaveta.UseVisualStyleBackColor = true;
            checkBoxGaveta.CheckedChanged += checkBoxGaveta_CheckedChanged;
            // 
            // checkBoxArea
            // 
            checkBoxArea.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxArea.ImageAlign = ContentAlignment.TopCenter;
            checkBoxArea.Location = new Point(150, 115);
            checkBoxArea.Name = "checkBoxArea";
            checkBoxArea.Size = new Size(97, 48);
            checkBoxArea.TabIndex = 89;
            checkBoxArea.Text = "Activar áreas";
            checkBoxArea.TextAlign = ContentAlignment.MiddleCenter;
            checkBoxArea.UseVisualStyleBackColor = true;
            checkBoxArea.CheckedChanged += checkBoxArea_CheckedChanged;
            // 
            // btnEliminarFiltro
            // 
            btnEliminarFiltro.Anchor = AnchorStyles.Top;
            btnEliminarFiltro.BackColor = Color.FromArgb(135, 30, 80);
            btnEliminarFiltro.FlatAppearance.BorderSize = 0;
            btnEliminarFiltro.FlatStyle = FlatStyle.Flat;
            btnEliminarFiltro.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnEliminarFiltro.ForeColor = Color.White;
            btnEliminarFiltro.Location = new Point(1158, 118);
            btnEliminarFiltro.Name = "btnEliminarFiltro";
            btnEliminarFiltro.Size = new Size(156, 41);
            btnEliminarFiltro.TabIndex = 88;
            btnEliminarFiltro.Text = "Eliminar filtros";
            btnEliminarFiltro.UseVisualStyleBackColor = false;
            btnEliminarFiltro.Click += btnEliminarFiltro_Click;
            // 
            // btnFiltro
            // 
            btnFiltro.Anchor = AnchorStyles.Top;
            btnFiltro.BackColor = Color.FromArgb(80, 190, 215);
            btnFiltro.FlatAppearance.BorderSize = 0;
            btnFiltro.FlatStyle = FlatStyle.Flat;
            btnFiltro.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnFiltro.ForeColor = Color.White;
            btnFiltro.Location = new Point(996, 118);
            btnFiltro.Name = "btnFiltro";
            btnFiltro.Size = new Size(156, 41);
            btnFiltro.TabIndex = 46;
            btnFiltro.Text = "Aplicar filtros";
            btnFiltro.UseVisualStyleBackColor = false;
            btnFiltro.Click += btnFiltro_Click;
            // 
            // cmbBoxArea
            // 
            cmbBoxArea.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbBoxArea.FormattingEnabled = true;
            cmbBoxArea.Items.AddRange(new object[] { "Ninguno" });
            cmbBoxArea.Location = new Point(253, 122);
            cmbBoxArea.Name = "cmbBoxArea";
            cmbBoxArea.Size = new Size(295, 33);
            cmbBoxArea.TabIndex = 87;
            // 
            // cmbBoxGaveta
            // 
            cmbBoxGaveta.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbBoxGaveta.FormattingEnabled = true;
            cmbBoxGaveta.Location = new Point(677, 122);
            cmbBoxGaveta.Name = "cmbBoxGaveta";
            cmbBoxGaveta.Size = new Size(295, 33);
            cmbBoxGaveta.TabIndex = 85;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label11.Location = new Point(12, 126);
            label11.Name = "label11";
            label11.Size = new Size(110, 24);
            label11.TabIndex = 86;
            label11.Text = "Filtrar por:";
            // 
            // btnAgregar
            // 
            btnAgregar.BackColor = Color.FromArgb(80, 190, 215);
            btnAgregar.FlatAppearance.BorderSize = 0;
            btnAgregar.FlatStyle = FlatStyle.Flat;
            btnAgregar.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnAgregar.ForeColor = Color.White;
            btnAgregar.Location = new Point(12, 3);
            btnAgregar.Name = "btnAgregar";
            btnAgregar.Size = new Size(214, 41);
            btnAgregar.TabIndex = 27;
            btnAgregar.Text = "+ Agregar sustancia";
            btnAgregar.UseVisualStyleBackColor = false;
            btnAgregar.Click += btnAgregar_Click_1;
            // 
            // btnSolicitudes
            // 
            btnSolicitudes.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            btnSolicitudes.BackColor = Color.FromArgb(0, 153, 153);
            btnSolicitudes.FlatAppearance.BorderSize = 0;
            btnSolicitudes.FlatStyle = FlatStyle.Flat;
            btnSolicitudes.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnSolicitudes.ForeColor = Color.White;
            btnSolicitudes.Location = new Point(1017, 8);
            btnSolicitudes.Name = "btnSolicitudes";
            btnSolicitudes.Size = new Size(274, 41);
            btnSolicitudes.TabIndex = 42;
            btnSolicitudes.Text = "Admin. altas sust. químicas";
            btnSolicitudes.UseVisualStyleBackColor = false;
            btnSolicitudes.Click += btnSolicitudes_Click;
            // 
            // panel2
            // 
            panel2.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            panel2.Controls.Add(lblSolicitudes);
            panel2.Controls.Add(btnAddArea);
            panel2.Controls.Add(btnAddGaveta);
            panel2.Controls.Add(btnSolicitudes);
            panel2.Controls.Add(btnAgregar);
            panel2.Location = new Point(0, 180);
            panel2.Name = "panel2";
            panel2.Size = new Size(1333, 56);
            panel2.TabIndex = 45;
            // 
            // lblSolicitudes
            // 
            lblSolicitudes.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            lblSolicitudes.AutoSize = true;
            lblSolicitudes.BackColor = Color.FromArgb(135, 30, 80);
            lblSolicitudes.FlatStyle = FlatStyle.Flat;
            lblSolicitudes.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            lblSolicitudes.ForeColor = Color.White;
            lblSolicitudes.Location = new Point(1254, 0);
            lblSolicitudes.Name = "lblSolicitudes";
            lblSolicitudes.Size = new Size(50, 24);
            lblSolicitudes.TabIndex = 45;
            lblSolicitudes.Text = "+XX";
            // 
            // btnAddArea
            // 
            btnAddArea.Anchor = AnchorStyles.Top;
            btnAddArea.BackColor = Color.FromArgb(80, 190, 215);
            btnAddArea.FlatAppearance.BorderSize = 0;
            btnAddArea.FlatStyle = FlatStyle.Flat;
            btnAddArea.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnAddArea.ForeColor = Color.White;
            btnAddArea.Location = new Point(411, 3);
            btnAddArea.Name = "btnAddArea";
            btnAddArea.Size = new Size(200, 41);
            btnAddArea.TabIndex = 44;
            btnAddArea.Text = "+ Agregar área";
            btnAddArea.UseVisualStyleBackColor = false;
            btnAddArea.Click += btnAddArea_Click;
            // 
            // btnAddGaveta
            // 
            btnAddGaveta.Anchor = AnchorStyles.Top;
            btnAddGaveta.BackColor = Color.FromArgb(80, 190, 215);
            btnAddGaveta.FlatAppearance.BorderSize = 0;
            btnAddGaveta.FlatStyle = FlatStyle.Flat;
            btnAddGaveta.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnAddGaveta.ForeColor = Color.White;
            btnAddGaveta.Location = new Point(721, 3);
            btnAddGaveta.Name = "btnAddGaveta";
            btnAddGaveta.Size = new Size(200, 41);
            btnAddGaveta.TabIndex = 43;
            btnAddGaveta.Text = "+ Agregar gaveta";
            btnAddGaveta.UseVisualStyleBackColor = false;
            btnAddGaveta.Click += btnAddGaveta_Click;
            // 
            // flowLayoutPanel1
            // 
            flowLayoutPanel1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            flowLayoutPanel1.AutoScroll = true;
            flowLayoutPanel1.FlowDirection = FlowDirection.TopDown;
            flowLayoutPanel1.Location = new Point(12, 242);
            flowLayoutPanel1.Name = "flowLayoutPanel1";
            flowLayoutPanel1.Size = new Size(1321, 499);
            flowLayoutPanel1.TabIndex = 46;
            flowLayoutPanel1.WrapContents = false;
            // 
            // btnCargar
            // 
            btnCargar.Anchor = AnchorStyles.Bottom;
            btnCargar.BackColor = Color.FromArgb(80, 190, 215);
            btnCargar.FlatAppearance.BorderSize = 0;
            btnCargar.FlatStyle = FlatStyle.Flat;
            btnCargar.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnCargar.ForeColor = Color.White;
            btnCargar.Location = new Point(567, 747);
            btnCargar.Name = "btnCargar";
            btnCargar.Size = new Size(200, 41);
            btnCargar.TabIndex = 47;
            btnCargar.Text = "Cargar más";
            btnCargar.UseVisualStyleBackColor = false;
            btnCargar.Click += btnCargar_Click;
            // 
            // btnCargarFiltro
            // 
            btnCargarFiltro.Anchor = AnchorStyles.Bottom;
            btnCargarFiltro.BackColor = Color.FromArgb(80, 190, 215);
            btnCargarFiltro.FlatAppearance.BorderSize = 0;
            btnCargarFiltro.FlatStyle = FlatStyle.Flat;
            btnCargarFiltro.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnCargarFiltro.ForeColor = Color.White;
            btnCargarFiltro.Location = new Point(567, 747);
            btnCargarFiltro.Name = "btnCargarFiltro";
            btnCargarFiltro.Size = new Size(200, 41);
            btnCargarFiltro.TabIndex = 48;
            btnCargarFiltro.Text = "CARGAR MÁS";
            btnCargarFiltro.UseVisualStyleBackColor = false;
            btnCargarFiltro.Visible = false;
            btnCargarFiltro.Click += btnCargarFiltro_Click;
            // 
            // btnCargarBuscar
            // 
            btnCargarBuscar.Anchor = AnchorStyles.Bottom;
            btnCargarBuscar.BackColor = Color.FromArgb(80, 190, 215);
            btnCargarBuscar.FlatAppearance.BorderSize = 0;
            btnCargarBuscar.FlatStyle = FlatStyle.Flat;
            btnCargarBuscar.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnCargarBuscar.ForeColor = Color.White;
            btnCargarBuscar.Location = new Point(567, 747);
            btnCargarBuscar.Name = "btnCargarBuscar";
            btnCargarBuscar.Size = new Size(200, 41);
            btnCargarBuscar.TabIndex = 49;
            btnCargarBuscar.Text = "Cargar MÁS";
            btnCargarBuscar.UseVisualStyleBackColor = false;
            btnCargarBuscar.Visible = false;
            btnCargarBuscar.Click += btnCargarBuscar_Click;
            // 
            // Administrador
            // 
            AutoScaleDimensions = new SizeF(10F, 25F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoScroll = true;
            BackColor = SystemColors.ControlLightLight;
            ClientSize = new Size(1333, 794);
            Controls.Add(btnCargarBuscar);
            Controls.Add(btnCargarFiltro);
            Controls.Add(btnCargar);
            Controls.Add(flowLayoutPanel1);
            Controls.Add(panel2);
            Controls.Add(panel1);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "Administrador";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Listado de Sustancias";
            ((System.ComponentModel.ISupportInitialize)btnSalir).EndInit();
            ((System.ComponentModel.ISupportInitialize)btnBuscar).EndInit();
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            panel2.ResumeLayout(false);
            panel2.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private PictureBox btnSalir;
        private Label label4;
        private TextBox txtBuscar;
        private PictureBox btnBuscar;
        private Panel panel1;
        private Button btnAgregar;
        private Button btnSolicitudes;
        private Panel panel2;
        private FlowLayoutPanel flowLayoutPanel1;
        private Button btnAddGaveta;
        private Button btnAddArea;
        private Label lblSolicitudes;
        private Button btnCargar;
        private ComboBox cmbBoxGaveta;
        private Label label11;
        private Button btnFiltro;
        private ComboBox cmbBoxArea;
        private Button btnEliminarFiltro;
        private Button btnCargarFiltro;
        private Button btnCargarBuscar;
        private CheckBox checkBoxArea;
        private CheckBox checkBoxGaveta;
    }
}