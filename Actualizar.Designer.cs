﻿namespace PruebaScreen
{
    partial class Actualizar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Actualizar));
            openFileDialog1 = new OpenFileDialog();
            openFileDialog2 = new OpenFileDialog();
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            label4 = new Label();
            label5 = new Label();
            label6 = new Label();
            label7 = new Label();
            label8 = new Label();
            textBox1 = new TextBox();
            textBox2 = new TextBox();
            textBox3 = new TextBox();
            textBox4 = new TextBox();
            comboBox1 = new ComboBox();
            comboBox2 = new ComboBox();
            textBox5 = new TextBox();
            checkBox1 = new CheckBox();
            label9 = new Label();
            label10 = new Label();
            label11 = new Label();
            label12 = new Label();
            textBox6 = new TextBox();
            comboBox3 = new ComboBox();
            comboBox4 = new ComboBox();
            label13 = new Label();
            label14 = new Label();
            checkBox2 = new CheckBox();
            button1 = new Button();
            checkBox3 = new CheckBox();
            label15 = new Label();
            label16 = new Label();
            label18 = new Label();
            label19 = new Label();
            label20 = new Label();
            label21 = new Label();
            label22 = new Label();
            txtNombreSustancia = new TextBox();
            txtUsos = new TextBox();
            comboContenedor = new ComboBox();
            txtCantidad = new TextBox();
            txtConsumos = new TextBox();
            label23 = new Label();
            checkBoxTapones = new CheckBox();
            label24 = new Label();
            label25 = new Label();
            label26 = new Label();
            label27 = new Label();
            comboPalabra = new ComboBox();
            checkBoxExplosivos = new CheckBox();
            checkBoxInflamable = new CheckBox();
            btnUpdateHds = new Button();
            btnUpdateFoto = new Button();
            btnUpdateS = new Button();
            btnCancel = new Button();
            checkBoxLentes = new CheckBox();
            checkBoxBotas = new CheckBox();
            checkBoxOxidante = new CheckBox();
            checkBoxBajoPresion = new CheckBox();
            checkBoxCorrosion = new CheckBox();
            checkBoxToxico = new CheckBox();
            checkBoxIrritante = new CheckBox();
            checkBoxPeligroSalud = new CheckBox();
            checkBoxMedioAmbiente = new CheckBox();
            label28 = new Label();
            txtRiesgos = new TextBox();
            label17 = new Label();
            txtTelefonoEmergencia = new TextBox();
            label29 = new Label();
            txtCas = new TextBox();
            label30 = new Label();
            txtOnu = new TextBox();
            label31 = new Label();
            label32 = new Label();
            label33 = new Label();
            label34 = new Label();
            txtNombreFabricante = new TextBox();
            txtTelefonoFabricante = new TextBox();
            txtDireccion = new TextBox();
            checkBoxGaveta = new CheckedListBox();
            checkBoxGuantes = new CheckBox();
            checkBoxMascarilla = new CheckBox();
            checkBoxTraje = new CheckBox();
            txtRutahds = new TextBox();
            label35 = new Label();
            label36 = new Label();
            txtRutaFoto = new TextBox();
            checkedListArea = new CheckedListBox();
            SuspendLayout();
            // 
            // openFileDialog1
            // 
            openFileDialog1.FileName = "openFileDialog1";
            openFileDialog1.Filter = "PDF (.*pdf)|*.pdf";
            // 
            // openFileDialog2
            // 
            openFileDialog2.FileName = "openFileDialog2";
            openFileDialog2.Filter = "Image Files|*.jpg;*.jpeg;*.png;";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Siemens Sans", 24F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(368, 40);
            label1.Name = "label1";
            label1.Size = new Size(470, 58);
            label1.TabIndex = 0;
            label1.Text = "Actualizar sustancia";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(53, 136);
            label2.Name = "label2";
            label2.Size = new Size(103, 50);
            label2.TabIndex = 1;
            label2.Text = "Nombre de\r\nla sustancia";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(53, 244);
            label3.Name = "label3";
            label3.Size = new Size(87, 50);
            label3.TabIndex = 2;
            label3.Text = "Nombre\r\ncomercial";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(53, 353);
            label4.Name = "label4";
            label4.Size = new Size(131, 50);
            label4.TabIndex = 3;
            label4.Text = "Usos\r\nrecomendados";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(53, 497);
            label5.Name = "label5";
            label5.Size = new Size(48, 25);
            label5.TabIndex = 4;
            label5.Text = "Área";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(612, 488);
            label6.Name = "label6";
            label6.Size = new Size(103, 50);
            label6.TabIndex = 5;
            label6.Text = "Tipo de \r\ncontenedor";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new Point(53, 574);
            label7.Name = "label7";
            label7.Size = new Size(84, 50);
            label7.TabIndex = 6;
            label7.Text = "Cantidad\r\nestimada";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new Point(612, 574);
            label8.Name = "label8";
            label8.Size = new Size(99, 50);
            label8.TabIndex = 7;
            label8.Text = "Consumos\r\npromedios";
            // 
            // textBox1
            // 
            textBox1.Location = new Point(218, 170);
            textBox1.Name = "textBox1";
            textBox1.Size = new Size(879, 31);
            textBox1.TabIndex = 8;
            // 
            // textBox2
            // 
            textBox2.Location = new Point(224, 254);
            textBox2.Name = "textBox2";
            textBox2.Size = new Size(879, 31);
            textBox2.TabIndex = 9;
            // 
            // textBox3
            // 
            textBox3.Location = new Point(224, 337);
            textBox3.Multiline = true;
            textBox3.Name = "textBox3";
            textBox3.Size = new Size(879, 100);
            textBox3.TabIndex = 10;
            // 
            // textBox4
            // 
            textBox4.Location = new Point(224, 583);
            textBox4.Name = "textBox4";
            textBox4.Size = new Size(366, 31);
            textBox4.TabIndex = 11;
            // 
            // comboBox1
            // 
            comboBox1.FormattingEnabled = true;
            comboBox1.Location = new Point(224, 494);
            comboBox1.Name = "comboBox1";
            comboBox1.Size = new Size(366, 33);
            comboBox1.TabIndex = 12;
            // 
            // comboBox2
            // 
            comboBox2.FormattingEnabled = true;
            comboBox2.Location = new Point(737, 494);
            comboBox2.Name = "comboBox2";
            comboBox2.Size = new Size(366, 33);
            comboBox2.TabIndex = 13;
            // 
            // textBox5
            // 
            textBox5.Location = new Point(742, 583);
            textBox5.Name = "textBox5";
            textBox5.Size = new Size(361, 31);
            textBox5.TabIndex = 14;
            // 
            // checkBox1
            // 
            checkBox1.Image = (Image)resources.GetObject("checkBox1.Image");
            checkBox1.ImageAlign = ContentAlignment.TopCenter;
            checkBox1.Location = new Point(55, 150);
            checkBox1.Name = "checkBox1";
            checkBox1.Size = new Size(150, 150);
            checkBox1.TabIndex = 15;
            checkBox1.Text = "checkBox1";
            checkBox1.TextAlign = ContentAlignment.BottomCenter;
            checkBox1.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new Point(53, 684);
            label9.Name = "label9";
            label9.Size = new Size(74, 25);
            label9.TabIndex = 16;
            label9.Text = "Riesgos";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new Point(53, 877);
            label10.Name = "label10";
            label10.Size = new Size(77, 25);
            label10.TabIndex = 17;
            label10.Text = "Uso EPP";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new Point(53, 1038);
            label11.Name = "label11";
            label11.Size = new Size(102, 50);
            label11.TabIndex = 18;
            label11.Text = "Palabra de\r\nadvertencia";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new Point(629, 1058);
            label12.Name = "label12";
            label12.Size = new Size(66, 25);
            label12.TabIndex = 19;
            label12.Text = "Gaveta";
            // 
            // textBox6
            // 
            textBox6.Location = new Point(224, 671);
            textBox6.Multiline = true;
            textBox6.Name = "textBox6";
            textBox6.Size = new Size(879, 102);
            textBox6.TabIndex = 20;
            // 
            // comboBox3
            // 
            comboBox3.FormattingEnabled = true;
            comboBox3.Location = new Point(224, 1055);
            comboBox3.Name = "comboBox3";
            comboBox3.Size = new Size(366, 33);
            comboBox3.TabIndex = 21;
            // 
            // comboBox4
            // 
            comboBox4.FormattingEnabled = true;
            comboBox4.Location = new Point(737, 1055);
            comboBox4.Name = "comboBox4";
            comboBox4.Size = new Size(364, 33);
            comboBox4.TabIndex = 22;
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            label13.Location = new Point(53, 1161);
            label13.Name = "label13";
            label13.Size = new Size(110, 25);
            label13.TabIndex = 23;
            label13.Text = "Pictogramas";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new Point(350, 1507);
            label14.Name = "label14";
            label14.Size = new Size(69, 25);
            label14.TabIndex = 24;
            label14.Text = "label14";
            // 
            // checkBox2
            // 
            checkBox2.Image = (Image)resources.GetObject("checkBox2.Image");
            checkBox2.Location = new Point(53, 1221);
            checkBox2.Name = "checkBox2";
            checkBox2.Size = new Size(150, 150);
            checkBox2.TabIndex = 25;
            checkBox2.Text = "checkBox2";
            checkBox2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            button1.Location = new Point(595, 1459);
            button1.Name = "button1";
            button1.Size = new Size(227, 698);
            button1.TabIndex = 26;
            button1.Text = "button1";
            button1.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            checkBox3.Location = new Point(53, 1430);
            checkBox3.Name = "checkBox3";
            checkBox3.Size = new Size(150, 150);
            checkBox3.TabIndex = 27;
            checkBox3.Text = "checkBox3";
            checkBox3.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.Font = new Font("Siemens Sans", 24F, FontStyle.Bold, GraphicsUnit.Point);
            label15.Location = new Point(371, 32);
            label15.Name = "label15";
            label15.Size = new Size(470, 58);
            label15.TabIndex = 0;
            label15.Text = "Actualizar sustancia";
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label16.Location = new Point(50, 147);
            label16.Name = "label16";
            label16.Size = new Size(119, 48);
            label16.TabIndex = 1;
            label16.Text = "Nombre de\r\nla sustancia";
            // 
            // label18
            // 
            label18.AutoSize = true;
            label18.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label18.Location = new Point(50, 258);
            label18.Name = "label18";
            label18.Size = new Size(148, 48);
            label18.TabIndex = 3;
            label18.Text = "Usos\r\nrecomendados";
            // 
            // label19
            // 
            label19.AutoSize = true;
            label19.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label19.Location = new Point(54, 992);
            label19.Name = "label19";
            label19.Size = new Size(53, 24);
            label19.TabIndex = 4;
            label19.Text = "Área";
            // 
            // label20
            // 
            label20.AutoSize = true;
            label20.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label20.Location = new Point(610, 391);
            label20.Name = "label20";
            label20.Size = new Size(117, 48);
            label20.TabIndex = 5;
            label20.Text = "Tipo de\r\ncontenedor";
            // 
            // label21
            // 
            label21.AutoSize = true;
            label21.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label21.Location = new Point(50, 500);
            label21.Name = "label21";
            label21.Size = new Size(95, 48);
            label21.TabIndex = 6;
            label21.Text = "Cantidad\r\nestimada";
            // 
            // label22
            // 
            label22.AutoSize = true;
            label22.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label22.Location = new Point(610, 500);
            label22.Name = "label22";
            label22.Size = new Size(110, 48);
            label22.TabIndex = 7;
            label22.Text = "Consumos\r\npromedios";
            // 
            // txtNombreSustancia
            // 
            txtNombreSustancia.Location = new Point(218, 164);
            txtNombreSustancia.Name = "txtNombreSustancia";
            txtNombreSustancia.Size = new Size(873, 31);
            txtNombreSustancia.TabIndex = 1;
            // 
            // txtUsos
            // 
            txtUsos.Location = new Point(218, 246);
            txtUsos.Multiline = true;
            txtUsos.Name = "txtUsos";
            txtUsos.Size = new Size(873, 101);
            txtUsos.TabIndex = 2;
            // 
            // comboContenedor
            // 
            comboContenedor.DropDownStyle = ComboBoxStyle.DropDownList;
            comboContenedor.FormattingEnabled = true;
            comboContenedor.Items.AddRange(new object[] { "LATA", "BOTELLA", "CUBETA", "GARRAFA", "CILINDRO", "COSTAL", "ENVASE" });
            comboContenedor.Location = new Point(742, 401);
            comboContenedor.Name = "comboContenedor";
            comboContenedor.Size = new Size(349, 33);
            comboContenedor.TabIndex = 4;
            // 
            // txtCantidad
            // 
            txtCantidad.Location = new Point(218, 517);
            txtCantidad.Name = "txtCantidad";
            txtCantidad.Size = new Size(349, 31);
            txtCantidad.TabIndex = 5;
            // 
            // txtConsumos
            // 
            txtConsumos.Location = new Point(743, 518);
            txtConsumos.Name = "txtConsumos";
            txtConsumos.Size = new Size(348, 31);
            txtConsumos.TabIndex = 6;
            // 
            // label23
            // 
            label23.AutoSize = true;
            label23.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label23.Location = new Point(50, 658);
            label23.Name = "label23";
            label23.Size = new Size(83, 24);
            label23.TabIndex = 16;
            label23.Text = "Uso EPP";
            // 
            // checkBoxTapones
            // 
            checkBoxTapones.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxTapones.Image = (Image)resources.GetObject("checkBoxTapones.Image");
            checkBoxTapones.ImageAlign = ContentAlignment.TopCenter;
            checkBoxTapones.Location = new Point(216, 595);
            checkBoxTapones.Name = "checkBoxTapones";
            checkBoxTapones.Size = new Size(150, 150);
            checkBoxTapones.TabIndex = 7;
            checkBoxTapones.Text = "Tapones auditivos";
            checkBoxTapones.TextAlign = ContentAlignment.BottomCenter;
            checkBoxTapones.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            label24.AutoSize = true;
            label24.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label24.Location = new Point(50, 391);
            label24.Name = "label24";
            label24.Size = new Size(120, 48);
            label24.TabIndex = 18;
            label24.Text = "Palabra de\r\nadvertencia";
            // 
            // label25
            // 
            label25.AutoSize = true;
            label25.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label25.Location = new Point(612, 992);
            label25.Name = "label25";
            label25.Size = new Size(75, 24);
            label25.TabIndex = 19;
            label25.Text = "Gaveta";
            // 
            // label26
            // 
            label26.AutoSize = true;
            label26.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label26.Location = new Point(52, 1908);
            label26.Name = "label26";
            label26.Size = new Size(125, 24);
            label26.TabIndex = 20;
            label26.Text = "Pictogramas";
            // 
            // label27
            // 
            label27.AutoSize = true;
            label27.Location = new Point(979, 2574);
            label27.Name = "label27";
            label27.Size = new Size(0, 25);
            label27.TabIndex = 21;
            // 
            // comboPalabra
            // 
            comboPalabra.DropDownStyle = ComboBoxStyle.DropDownList;
            comboPalabra.FormattingEnabled = true;
            comboPalabra.Items.AddRange(new object[] { "", "ATENCIÓN", "PELIGRO" });
            comboPalabra.Location = new Point(216, 401);
            comboPalabra.Name = "comboPalabra";
            comboPalabra.Size = new Size(349, 33);
            comboPalabra.TabIndex = 3;
            // 
            // checkBoxExplosivos
            // 
            checkBoxExplosivos.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxExplosivos.Image = (Image)resources.GetObject("checkBoxExplosivos.Image");
            checkBoxExplosivos.ImageAlign = ContentAlignment.TopCenter;
            checkBoxExplosivos.Location = new Point(52, 1960);
            checkBoxExplosivos.Name = "checkBoxExplosivos";
            checkBoxExplosivos.Size = new Size(150, 150);
            checkBoxExplosivos.TabIndex = 22;
            checkBoxExplosivos.Text = "Peligro de explosivos";
            checkBoxExplosivos.TextAlign = ContentAlignment.BottomCenter;
            checkBoxExplosivos.UseVisualStyleBackColor = true;
            // 
            // checkBoxInflamable
            // 
            checkBoxInflamable.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxInflamable.Image = (Image)resources.GetObject("checkBoxInflamable.Image");
            checkBoxInflamable.ImageAlign = ContentAlignment.TopCenter;
            checkBoxInflamable.Location = new Point(270, 1960);
            checkBoxInflamable.Name = "checkBoxInflamable";
            checkBoxInflamable.Size = new Size(150, 150);
            checkBoxInflamable.TabIndex = 23;
            checkBoxInflamable.Text = "Inflamable";
            checkBoxInflamable.TextAlign = ContentAlignment.BottomCenter;
            checkBoxInflamable.UseVisualStyleBackColor = true;
            // 
            // btnUpdateHds
            // 
            btnUpdateHds.BackColor = Color.FromArgb(80, 190, 215);
            btnUpdateHds.FlatAppearance.BorderColor = Color.White;
            btnUpdateHds.FlatAppearance.BorderSize = 0;
            btnUpdateHds.FlatStyle = FlatStyle.Flat;
            btnUpdateHds.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnUpdateHds.ForeColor = Color.White;
            btnUpdateHds.Location = new Point(1024, 2352);
            btnUpdateHds.Name = "btnUpdateHds";
            btnUpdateHds.Size = new Size(61, 31);
            btnUpdateHds.TabIndex = 31;
            btnUpdateHds.Text = "...";
            btnUpdateHds.UseVisualStyleBackColor = false;
            btnUpdateHds.Click += btnUpdateHds_Click;
            // 
            // btnUpdateFoto
            // 
            btnUpdateFoto.BackColor = Color.FromArgb(80, 190, 215);
            btnUpdateFoto.FlatStyle = FlatStyle.Flat;
            btnUpdateFoto.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnUpdateFoto.ForeColor = Color.White;
            btnUpdateFoto.Location = new Point(1024, 2442);
            btnUpdateFoto.Name = "btnUpdateFoto";
            btnUpdateFoto.Size = new Size(61, 31);
            btnUpdateFoto.TabIndex = 32;
            btnUpdateFoto.Text = "...";
            btnUpdateFoto.UseVisualStyleBackColor = false;
            btnUpdateFoto.Click += btnUpdateFoto_Click;
            // 
            // btnUpdateS
            // 
            btnUpdateS.BackColor = Color.FromArgb(0, 153, 153);
            btnUpdateS.FlatStyle = FlatStyle.Flat;
            btnUpdateS.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnUpdateS.ForeColor = Color.White;
            btnUpdateS.Location = new Point(913, 2521);
            btnUpdateS.Name = "btnUpdateS";
            btnUpdateS.Size = new Size(172, 50);
            btnUpdateS.TabIndex = 33;
            btnUpdateS.Text = "Actualizar";
            btnUpdateS.UseVisualStyleBackColor = false;
            btnUpdateS.Click += btnUpdateS_Click;
            // 
            // btnCancel
            // 
            btnCancel.BackColor = Color.FromArgb(130, 30, 80);
            btnCancel.FlatStyle = FlatStyle.Flat;
            btnCancel.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnCancel.ForeColor = Color.White;
            btnCancel.Location = new Point(708, 2521);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(172, 50);
            btnCancel.TabIndex = 34;
            btnCancel.Text = "Cancelar";
            btnCancel.UseVisualStyleBackColor = false;
            btnCancel.Click += btnCancel_Click;
            // 
            // checkBoxLentes
            // 
            checkBoxLentes.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxLentes.Image = (Image)resources.GetObject("checkBoxLentes.Image");
            checkBoxLentes.ImageAlign = ContentAlignment.TopCenter;
            checkBoxLentes.Location = new Point(584, 595);
            checkBoxLentes.Name = "checkBoxLentes";
            checkBoxLentes.Size = new Size(150, 150);
            checkBoxLentes.TabIndex = 8;
            checkBoxLentes.Text = "Lentes de seguridad";
            checkBoxLentes.TextAlign = ContentAlignment.BottomCenter;
            checkBoxLentes.UseVisualStyleBackColor = true;
            // 
            // checkBoxBotas
            // 
            checkBoxBotas.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxBotas.Image = (Image)resources.GetObject("checkBoxBotas.Image");
            checkBoxBotas.ImageAlign = ContentAlignment.TopCenter;
            checkBoxBotas.Location = new Point(939, 595);
            checkBoxBotas.Name = "checkBoxBotas";
            checkBoxBotas.Size = new Size(150, 150);
            checkBoxBotas.TabIndex = 9;
            checkBoxBotas.Text = "Zapatos de seguridad";
            checkBoxBotas.TextAlign = ContentAlignment.BottomCenter;
            checkBoxBotas.UseVisualStyleBackColor = true;
            // 
            // checkBoxOxidante
            // 
            checkBoxOxidante.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxOxidante.Image = (Image)resources.GetObject("checkBoxOxidante.Image");
            checkBoxOxidante.ImageAlign = ContentAlignment.TopCenter;
            checkBoxOxidante.Location = new Point(511, 1960);
            checkBoxOxidante.Name = "checkBoxOxidante";
            checkBoxOxidante.Size = new Size(150, 150);
            checkBoxOxidante.TabIndex = 24;
            checkBoxOxidante.Text = "Oxidante";
            checkBoxOxidante.TextAlign = ContentAlignment.BottomCenter;
            checkBoxOxidante.UseVisualStyleBackColor = true;
            // 
            // checkBoxBajoPresion
            // 
            checkBoxBajoPresion.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxBajoPresion.Image = (Image)resources.GetObject("checkBoxBajoPresion.Image");
            checkBoxBajoPresion.ImageAlign = ContentAlignment.TopCenter;
            checkBoxBajoPresion.Location = new Point(745, 1960);
            checkBoxBajoPresion.Name = "checkBoxBajoPresion";
            checkBoxBajoPresion.Size = new Size(150, 150);
            checkBoxBajoPresion.TabIndex = 25;
            checkBoxBajoPresion.Text = "Gas bajo presión";
            checkBoxBajoPresion.TextAlign = ContentAlignment.BottomCenter;
            checkBoxBajoPresion.UseVisualStyleBackColor = true;
            // 
            // checkBoxCorrosion
            // 
            checkBoxCorrosion.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxCorrosion.Image = (Image)resources.GetObject("checkBoxCorrosion.Image");
            checkBoxCorrosion.ImageAlign = ContentAlignment.TopCenter;
            checkBoxCorrosion.Location = new Point(952, 1960);
            checkBoxCorrosion.Name = "checkBoxCorrosion";
            checkBoxCorrosion.Size = new Size(150, 150);
            checkBoxCorrosion.TabIndex = 26;
            checkBoxCorrosion.Text = "Corrosión";
            checkBoxCorrosion.TextAlign = ContentAlignment.BottomCenter;
            checkBoxCorrosion.UseVisualStyleBackColor = true;
            // 
            // checkBoxToxico
            // 
            checkBoxToxico.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxToxico.Image = (Image)resources.GetObject("checkBoxToxico.Image");
            checkBoxToxico.ImageAlign = ContentAlignment.TopCenter;
            checkBoxToxico.Location = new Point(139, 2139);
            checkBoxToxico.Name = "checkBoxToxico";
            checkBoxToxico.Size = new Size(150, 150);
            checkBoxToxico.TabIndex = 27;
            checkBoxToxico.Text = "Tóxico";
            checkBoxToxico.TextAlign = ContentAlignment.BottomCenter;
            checkBoxToxico.UseVisualStyleBackColor = true;
            // 
            // checkBoxIrritante
            // 
            checkBoxIrritante.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxIrritante.Image = (Image)resources.GetObject("checkBoxIrritante.Image");
            checkBoxIrritante.ImageAlign = ContentAlignment.TopCenter;
            checkBoxIrritante.Location = new Point(378, 2139);
            checkBoxIrritante.Name = "checkBoxIrritante";
            checkBoxIrritante.Size = new Size(150, 150);
            checkBoxIrritante.TabIndex = 28;
            checkBoxIrritante.Text = "Irritante - Nocivo";
            checkBoxIrritante.TextAlign = ContentAlignment.BottomCenter;
            checkBoxIrritante.UseVisualStyleBackColor = true;
            // 
            // checkBoxPeligroSalud
            // 
            checkBoxPeligroSalud.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxPeligroSalud.Image = (Image)resources.GetObject("checkBoxPeligroSalud.Image");
            checkBoxPeligroSalud.ImageAlign = ContentAlignment.TopCenter;
            checkBoxPeligroSalud.Location = new Point(633, 2139);
            checkBoxPeligroSalud.Name = "checkBoxPeligroSalud";
            checkBoxPeligroSalud.Size = new Size(150, 150);
            checkBoxPeligroSalud.TabIndex = 29;
            checkBoxPeligroSalud.Text = "Peligro para la salud";
            checkBoxPeligroSalud.TextAlign = ContentAlignment.BottomCenter;
            checkBoxPeligroSalud.UseVisualStyleBackColor = true;
            // 
            // checkBoxMedioAmbiente
            // 
            checkBoxMedioAmbiente.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxMedioAmbiente.Image = (Image)resources.GetObject("checkBoxMedioAmbiente.Image");
            checkBoxMedioAmbiente.ImageAlign = ContentAlignment.TopCenter;
            checkBoxMedioAmbiente.Location = new Point(878, 2139);
            checkBoxMedioAmbiente.Name = "checkBoxMedioAmbiente";
            checkBoxMedioAmbiente.Size = new Size(150, 150);
            checkBoxMedioAmbiente.TabIndex = 30;
            checkBoxMedioAmbiente.Text = "Daño al medio ambiente";
            checkBoxMedioAmbiente.TextAlign = ContentAlignment.BottomCenter;
            checkBoxMedioAmbiente.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            label28.AutoSize = true;
            label28.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label28.Location = new Point(54, 1142);
            label28.Name = "label28";
            label28.Size = new Size(81, 24);
            label28.TabIndex = 42;
            label28.Text = "Riesgos";
            // 
            // txtRiesgos
            // 
            txtRiesgos.Location = new Point(220, 1138);
            txtRiesgos.Multiline = true;
            txtRiesgos.Name = "txtRiesgos";
            txtRiesgos.Size = new Size(873, 112);
            txtRiesgos.TabIndex = 15;
            // 
            // label17
            // 
            label17.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label17.Location = new Point(52, 1305);
            label17.Name = "label17";
            label17.Size = new Size(144, 49);
            label17.TabIndex = 44;
            label17.Text = "Teléfono de emergencia";
            // 
            // txtTelefonoEmergencia
            // 
            txtTelefonoEmergencia.Location = new Point(220, 1317);
            txtTelefonoEmergencia.Name = "txtTelefonoEmergencia";
            txtTelefonoEmergencia.Size = new Size(351, 31);
            txtTelefonoEmergencia.TabIndex = 16;
            // 
            // label29
            // 
            label29.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label29.Location = new Point(612, 1305);
            label29.Name = "label29";
            label29.Size = new Size(104, 49);
            label29.TabIndex = 46;
            label29.Text = "Número de CAS";
            // 
            // txtCas
            // 
            txtCas.Location = new Point(745, 1317);
            txtCas.Name = "txtCas";
            txtCas.Size = new Size(348, 31);
            txtCas.TabIndex = 17;
            // 
            // label30
            // 
            label30.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label30.Location = new Point(54, 1425);
            label30.Name = "label30";
            label30.Size = new Size(104, 52);
            label30.TabIndex = 48;
            label30.Text = "Número de ONU";
            // 
            // txtOnu
            // 
            txtOnu.Location = new Point(218, 1437);
            txtOnu.Name = "txtOnu";
            txtOnu.Size = new Size(349, 31);
            txtOnu.TabIndex = 18;
            // 
            // label31
            // 
            label31.AutoSize = true;
            label31.Font = new Font("Siemens Sans", 12F, FontStyle.Bold, GraphicsUnit.Point);
            label31.Location = new Point(476, 1542);
            label31.Name = "label31";
            label31.Size = new Size(238, 29);
            label31.TabIndex = 50;
            label31.Text = "Datos del fabricante";
            // 
            // label32
            // 
            label32.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label32.Location = new Point(52, 1598);
            label32.Name = "label32";
            label32.Size = new Size(144, 57);
            label32.TabIndex = 51;
            label32.Text = "Nombre del fabricante";
            // 
            // label33
            // 
            label33.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label33.Location = new Point(610, 1605);
            label33.Name = "label33";
            label33.Size = new Size(104, 31);
            label33.TabIndex = 52;
            label33.Text = "Teléfono";
            // 
            // label34
            // 
            label34.AutoSize = true;
            label34.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label34.Location = new Point(52, 1728);
            label34.Name = "label34";
            label34.Size = new Size(99, 24);
            label34.TabIndex = 53;
            label34.Text = "Dirección";
            // 
            // txtNombreFabricante
            // 
            txtNombreFabricante.Location = new Point(216, 1605);
            txtNombreFabricante.Name = "txtNombreFabricante";
            txtNombreFabricante.Size = new Size(349, 31);
            txtNombreFabricante.TabIndex = 19;
            // 
            // txtTelefonoFabricante
            // 
            txtTelefonoFabricante.Location = new Point(743, 1601);
            txtTelefonoFabricante.Name = "txtTelefonoFabricante";
            txtTelefonoFabricante.Size = new Size(348, 31);
            txtTelefonoFabricante.TabIndex = 20;
            // 
            // txtDireccion
            // 
            txtDireccion.Location = new Point(218, 1724);
            txtDireccion.Multiline = true;
            txtDireccion.Name = "txtDireccion";
            txtDireccion.Size = new Size(873, 139);
            txtDireccion.TabIndex = 21;
            // 
            // checkBoxGaveta
            // 
            checkBoxGaveta.FormattingEnabled = true;
            checkBoxGaveta.Location = new Point(745, 966);
            checkBoxGaveta.Name = "checkBoxGaveta";
            checkBoxGaveta.Size = new Size(346, 144);
            checkBoxGaveta.TabIndex = 14;
            // 
            // checkBoxGuantes
            // 
            checkBoxGuantes.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxGuantes.Image = (Image)resources.GetObject("checkBoxGuantes.Image");
            checkBoxGuantes.ImageAlign = ContentAlignment.TopCenter;
            checkBoxGuantes.Location = new Point(216, 782);
            checkBoxGuantes.Name = "checkBoxGuantes";
            checkBoxGuantes.Size = new Size(150, 150);
            checkBoxGuantes.TabIndex = 10;
            checkBoxGuantes.Text = "Guantes";
            checkBoxGuantes.TextAlign = ContentAlignment.BottomCenter;
            checkBoxGuantes.UseVisualStyleBackColor = true;
            // 
            // checkBoxMascarilla
            // 
            checkBoxMascarilla.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxMascarilla.Image = (Image)resources.GetObject("checkBoxMascarilla.Image");
            checkBoxMascarilla.ImageAlign = ContentAlignment.TopCenter;
            checkBoxMascarilla.Location = new Point(584, 782);
            checkBoxMascarilla.Name = "checkBoxMascarilla";
            checkBoxMascarilla.Size = new Size(150, 150);
            checkBoxMascarilla.TabIndex = 11;
            checkBoxMascarilla.Text = "Mascarilla";
            checkBoxMascarilla.TextAlign = ContentAlignment.BottomCenter;
            checkBoxMascarilla.UseVisualStyleBackColor = true;
            // 
            // checkBoxTraje
            // 
            checkBoxTraje.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxTraje.Image = (Image)resources.GetObject("checkBoxTraje.Image");
            checkBoxTraje.ImageAlign = ContentAlignment.TopCenter;
            checkBoxTraje.Location = new Point(939, 782);
            checkBoxTraje.Name = "checkBoxTraje";
            checkBoxTraje.Size = new Size(150, 150);
            checkBoxTraje.TabIndex = 12;
            checkBoxTraje.Text = "Traje de Seguridad";
            checkBoxTraje.TextAlign = ContentAlignment.BottomCenter;
            checkBoxTraje.UseVisualStyleBackColor = true;
            // 
            // txtRutahds
            // 
            txtRutahds.Enabled = false;
            txtRutahds.Location = new Point(174, 2352);
            txtRutahds.Name = "txtRutahds";
            txtRutahds.ReadOnly = true;
            txtRutahds.Size = new Size(831, 31);
            txtRutahds.TabIndex = 61;
            // 
            // label35
            // 
            label35.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label35.Location = new Point(52, 2342);
            label35.Name = "label35";
            label35.Size = new Size(116, 52);
            label35.TabIndex = 62;
            label35.Text = "Actualizar HDS";
            // 
            // label36
            // 
            label36.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label36.Location = new Point(52, 2431);
            label36.Name = "label36";
            label36.Size = new Size(116, 52);
            label36.TabIndex = 63;
            label36.Text = "Actualizar foto";
            // 
            // txtRutaFoto
            // 
            txtRutaFoto.Enabled = false;
            txtRutaFoto.Location = new Point(174, 2442);
            txtRutaFoto.Name = "txtRutaFoto";
            txtRutaFoto.ReadOnly = true;
            txtRutaFoto.Size = new Size(831, 31);
            txtRutaFoto.TabIndex = 64;
            // 
            // checkedListArea
            // 
            checkedListArea.FormattingEnabled = true;
            checkedListArea.Location = new Point(220, 966);
            checkedListArea.Name = "checkedListArea";
            checkedListArea.Size = new Size(349, 144);
            checkedListArea.TabIndex = 13;
            // 
            // Actualizar
            // 
            AutoScroll = true;
            ClientSize = new Size(1168, 664);
            Controls.Add(checkedListArea);
            Controls.Add(txtRutaFoto);
            Controls.Add(label36);
            Controls.Add(label35);
            Controls.Add(txtRutahds);
            Controls.Add(checkBoxTraje);
            Controls.Add(checkBoxMascarilla);
            Controls.Add(checkBoxGuantes);
            Controls.Add(checkBoxGaveta);
            Controls.Add(txtDireccion);
            Controls.Add(txtTelefonoFabricante);
            Controls.Add(txtNombreFabricante);
            Controls.Add(label34);
            Controls.Add(label33);
            Controls.Add(label32);
            Controls.Add(label31);
            Controls.Add(txtOnu);
            Controls.Add(label30);
            Controls.Add(txtCas);
            Controls.Add(label29);
            Controls.Add(txtTelefonoEmergencia);
            Controls.Add(label17);
            Controls.Add(txtRiesgos);
            Controls.Add(label28);
            Controls.Add(checkBoxMedioAmbiente);
            Controls.Add(checkBoxPeligroSalud);
            Controls.Add(checkBoxIrritante);
            Controls.Add(checkBoxToxico);
            Controls.Add(checkBoxCorrosion);
            Controls.Add(checkBoxBajoPresion);
            Controls.Add(checkBoxOxidante);
            Controls.Add(checkBoxBotas);
            Controls.Add(checkBoxLentes);
            Controls.Add(btnCancel);
            Controls.Add(btnUpdateS);
            Controls.Add(btnUpdateFoto);
            Controls.Add(btnUpdateHds);
            Controls.Add(checkBoxInflamable);
            Controls.Add(checkBoxExplosivos);
            Controls.Add(comboPalabra);
            Controls.Add(label27);
            Controls.Add(label26);
            Controls.Add(label25);
            Controls.Add(label24);
            Controls.Add(checkBoxTapones);
            Controls.Add(label23);
            Controls.Add(txtConsumos);
            Controls.Add(txtCantidad);
            Controls.Add(comboContenedor);
            Controls.Add(txtUsos);
            Controls.Add(txtNombreSustancia);
            Controls.Add(label22);
            Controls.Add(label21);
            Controls.Add(label20);
            Controls.Add(label19);
            Controls.Add(label18);
            Controls.Add(label16);
            Controls.Add(label15);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "Actualizar";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Actualizar";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private OpenFileDialog openFileDialog1;
        private OpenFileDialog openFileDialog2;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private TextBox textBox1;
        private TextBox textBox2;
        private TextBox textBox3;
        private TextBox textBox4;
        private ComboBox comboBox1;
        private ComboBox comboBox2;
        private TextBox textBox5;
        private CheckBox checkBox1;
        private Label label9;
        private Label label10;
        private Label label11;
        private Label label12;
        private TextBox textBox6;
        private ComboBox comboBox3;
        private ComboBox comboBox4;
        private Label label13;
        private Label label14;
        private CheckBox checkBox2;
        private Button button1;
        private CheckBox checkBox3;
        private Label label15;
        private Label label16;
        private Label label18;
        private Label label19;
        private Label label20;
        private Label label21;
        private Label label22;
        private TextBox txtNombreSustancia;
        private TextBox txtUsos;
        private ComboBox comboContenedor;
        private TextBox txtCantidad;
        private TextBox txtConsumos;
        private Label label23;
        private CheckBox checkBoxTapones;
        private Label label24;
        private Label label25;
        private Label label26;
        private Label label27;
        private ComboBox comboPalabra;
        private CheckBox checkBoxExplosivos;
        private CheckBox checkBoxInflamable;
        private Button btnUpdateHds;
        private Button btnUpdateFoto;
        private Button btnUpdateS;
        private Button btnCancel;
        private CheckBox checkBoxLentes;
        private CheckBox checkBoxBotas;
        private CheckBox checkBoxOxidante;
        private CheckBox checkBoxBajoPresion;
        private CheckBox checkBoxCorrosion;
        private CheckBox checkBoxToxico;
        private CheckBox checkBoxIrritante;
        private CheckBox checkBoxPeligroSalud;
        private CheckBox checkBoxMedioAmbiente;
        private Label label28;
        private TextBox txtRiesgos;
        private Label label17;
        private TextBox txtTelefonoEmergencia;
        private Label label29;
        private TextBox txtCas;
        private Label label30;
        private TextBox txtOnu;
        private Label label31;
        private Label label32;
        private Label label33;
        private Label label34;
        private TextBox txtNombreFabricante;
        private TextBox txtTelefonoFabricante;
        private TextBox txtDireccion;
        private CheckedListBox checkBoxGaveta;
        private CheckBox checkBoxGuantes;
        private CheckBox checkBoxMascarilla;
        private CheckBox checkBoxTraje;
        private TextBox txtRutahds;
        private Label label35;
        private Label label36;
        private TextBox txtRutaFoto;
        private CheckedListBox checkedListArea;
    }
}