using System.Threading;
namespace PruebaScreen
{
    public partial class Form1 : Form
    {
        Clases.CUsuario cUsuario = new Clases.CUsuario();
        Loading loading;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
            /*this.MaximumSize = SystemInformation.PrimaryMonitorMaximizedWindowSize;
            this.WindowState = FormWindowState.Maximized;*/
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private async void button1_Click(object sender, EventArgs e)
        {
            //Traemos las cadenas de texto que est�n en los inputs del form y
            // se pasan a variables
            cUsuario.User = txtUser.Text;
            cUsuario.PasswordUsuario = txtPassword.Text;

            int rol = cUsuario.loginUsuario();
            int idUsuario = cUsuario.seleccionarUsuario();

            if (string.IsNullOrWhiteSpace(cUsuario.User) || string.IsNullOrWhiteSpace(cUsuario.PasswordUsuario))
            {
                MessageBox.Show("Debes llenar todos los campos");
            }
            else if (rol == 1 || rol == 4)
            {
                //cUsuario.activarSesion();
                Show();
                Task oTask = new Task(Algo);
                oTask.Start();
                await oTask;
                Administrador admin = new Administrador(rol, idUsuario);
                this.Hide();
                Esconder();
                admin.ShowDialog();
                this.Close();
            }

            else if (rol == 3)
            {
                Show();
                Task oTask = new Task(Algo);
                oTask.Start();
                await oTask;
                Solicitudes solicitudes = new Solicitudes(rol, idUsuario);
                this.Hide();
                Esconder();
                solicitudes.ShowDialog();
                this.Close();
            }

            else if (rol == 2)
            {
                Show();
                Task oTask = new Task(Algo);
                oTask.Start();
                await oTask;
                Form2 ventana = new Form2();
                this.Hide();
                Esconder();
                ventana.ShowDialog();
                this.Close();
            }
            else if (rol == 0)
            {
                MessageBox.Show("Usuario y/o contrase�a incorrectas");
            }
        }
        public void Algo()
        {
            Thread.Sleep(3000);
        }

        public void Show()
        {
            loading = new Loading();
            loading.Show();
        }

        public void Esconder()
        {
            if(loading!=null)
                loading.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}