﻿namespace PruebaScreen
{
    partial class Solicitudes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Solicitudes));
            label1 = new Label();
            btnSolicitar = new Button();
            btnSalirSolicitud = new PictureBox();
            btnRegresar = new Button();
            panel2 = new Panel();
            flowLayoutPanel1 = new FlowLayoutPanel();
            btnEliminarSoli = new Button();
            ((System.ComponentModel.ISupportInitialize)btnSalirSolicitud).BeginInit();
            panel2.SuspendLayout();
            SuspendLayout();
            // 
            // label1
            // 
            label1.Anchor = AnchorStyles.Top;
            label1.Font = new Font("Siemens Sans", 24F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(450, 22);
            label1.Name = "label1";
            label1.Size = new Size(269, 68);
            label1.TabIndex = 0;
            label1.Text = "Solicitudes";
            label1.Click += label1_Click;
            // 
            // btnSolicitar
            // 
            btnSolicitar.BackColor = Color.FromArgb(0, 153, 153);
            btnSolicitar.FlatAppearance.BorderSize = 0;
            btnSolicitar.FlatStyle = FlatStyle.Flat;
            btnSolicitar.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnSolicitar.ForeColor = Color.White;
            btnSolicitar.Location = new Point(40, 104);
            btnSolicitar.Name = "btnSolicitar";
            btnSolicitar.Size = new Size(132, 46);
            btnSolicitar.TabIndex = 1;
            btnSolicitar.Text = "Solicitar";
            btnSolicitar.UseVisualStyleBackColor = false;
            btnSolicitar.Click += btnSolicitar_Click;
            // 
            // btnSalirSolicitud
            // 
            btnSalirSolicitud.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            btnSalirSolicitud.Cursor = Cursors.Hand;
            btnSalirSolicitud.Image = (Image)resources.GetObject("btnSalirSolicitud.Image");
            btnSalirSolicitud.Location = new Point(1068, 19);
            btnSalirSolicitud.Name = "btnSalirSolicitud";
            btnSalirSolicitud.Size = new Size(50, 50);
            btnSalirSolicitud.SizeMode = PictureBoxSizeMode.StretchImage;
            btnSalirSolicitud.TabIndex = 11;
            btnSalirSolicitud.TabStop = false;
            btnSalirSolicitud.Click += btnSalirSolicitud_Click;
            // 
            // btnRegresar
            // 
            btnRegresar.BackColor = Color.FromArgb(80, 190, 215);
            btnRegresar.FlatAppearance.BorderSize = 0;
            btnRegresar.FlatStyle = FlatStyle.Flat;
            btnRegresar.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnRegresar.ForeColor = Color.White;
            btnRegresar.Location = new Point(40, 19);
            btnRegresar.Name = "btnRegresar";
            btnRegresar.Size = new Size(132, 50);
            btnRegresar.TabIndex = 12;
            btnRegresar.Text = "Regresar";
            btnRegresar.UseVisualStyleBackColor = false;
            btnRegresar.Click += btnRegresar_Click;
            // 
            // panel2
            // 
            panel2.Controls.Add(btnEliminarSoli);
            panel2.Controls.Add(label1);
            panel2.Controls.Add(btnRegresar);
            panel2.Controls.Add(btnSalirSolicitud);
            panel2.Controls.Add(btnSolicitar);
            panel2.Dock = DockStyle.Top;
            panel2.Location = new Point(0, 0);
            panel2.Name = "panel2";
            panel2.Size = new Size(1168, 168);
            panel2.TabIndex = 13;
            // 
            // flowLayoutPanel1
            // 
            flowLayoutPanel1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            flowLayoutPanel1.AutoScroll = true;
            flowLayoutPanel1.FlowDirection = FlowDirection.TopDown;
            flowLayoutPanel1.Location = new Point(12, 173);
            flowLayoutPanel1.Name = "flowLayoutPanel1";
            flowLayoutPanel1.Size = new Size(1156, 488);
            flowLayoutPanel1.TabIndex = 47;
            flowLayoutPanel1.WrapContents = false;
            // 
            // btnEliminarSoli
            // 
            btnEliminarSoli.BackColor = Color.Red;
            btnEliminarSoli.FlatAppearance.BorderSize = 0;
            btnEliminarSoli.FlatStyle = FlatStyle.Flat;
            btnEliminarSoli.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnEliminarSoli.ForeColor = Color.White;
            btnEliminarSoli.Location = new Point(909, 104);
            btnEliminarSoli.Name = "btnEliminarSoli";
            btnEliminarSoli.Size = new Size(209, 46);
            btnEliminarSoli.TabIndex = 13;
            btnEliminarSoli.Text = "Eliminar solicitudes";
            btnEliminarSoli.UseVisualStyleBackColor = false;
            btnEliminarSoli.Click += btnEliminarSoli_Click;
            // 
            // Solicitudes
            // 
            AutoScaleDimensions = new SizeF(10F, 25F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoScroll = true;
            ClientSize = new Size(1168, 664);
            Controls.Add(flowLayoutPanel1);
            Controls.Add(panel2);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "Solicitudes";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Solicitudes";
            Load += Solicitudes_Load;
            ((System.ComponentModel.ISupportInitialize)btnSalirSolicitud).EndInit();
            panel2.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private Label label1;
        private Button btnSolicitar;
        private PictureBox btnSalirSolicitud;
        private Button btnRegresar;
        private Panel panel2;
        private FlowLayoutPanel flowLayoutPanel1;
        private Button btnEliminarSoli;
    }
}