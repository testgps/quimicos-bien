﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaScreen
{
    public partial class Delete : Form
    {
        public Delete()
        {
            InitializeComponent();
        }

        private void Delete_Load(object sender, EventArgs e)
        {
            MiEliminar.Load("eliminando.gif");
            MiEliminar.Location = new Point(this.Width / 2 - MiEliminar.Width / 2,
                this.Height / 2 - MiEliminar.Height / 2);
        }
    }
}
