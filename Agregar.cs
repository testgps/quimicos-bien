﻿using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Kernel.Geom;
using iText.Kernel.Font;
using iText.IO.Font.Constants;
using iText.Layout.Properties;
using iText.IO.Image;
using iText.Layout.Borders;
using System.Data;
using PruebaScreen.Clases;

namespace PruebaScreen
{
    public partial class Agregar : Form
    {
        Clases.CSustancia objectSustancia = new Clases.CSustancia();
        Clases.CPictograma cPictograma = new Clases.CPictograma();
        Clases.CEquipopp cEquipopp = new Clases.CEquipopp();
        Guardando guardando;
        List<byte[]> pictogramas = new List<byte[]>();
        List<byte[]> epps = new List<byte[]>();
        private int rol;
        private int idUsuario;

        public Agregar(int idUsuario, int rol)
        {
            InitializeComponent();
            this.idUsuario = idUsuario;
            this.rol = rol;
            objectSustancia.mostrarGaveta(chBoxGaveta);
            objectSustancia.mostrarArea(checkedListArea);
        }

        private void Agregar_Load(object sender, EventArgs e)
        {

        }

        private void btnAgregarhds_Click(object sender, EventArgs e)
        {
            //Abrir el File Dialog
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                txtRutaHds.Text = openFileDialog1.FileName;
        }

        private void btnAgregarfoto_Click(object sender, EventArgs e)
        {
            // Abrir el File Dialog
            if (openFileDialog2.ShowDialog() == DialogResult.OK)
                txtRutaFoto.Text = openFileDialog2.FileName;

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Administrador administrador = new Administrador(rol, idUsuario); //Llama al form principal de iniciar sesión
            this.Hide(); //Esconde la pantalla actual
            administrador.ShowDialog(); //Nos lleva a la pantalla que queremos
            this.Close(); //Cierra la pantalla actual
        }

        private async void btnAgregar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombreSustancia.Text) || string.IsNullOrWhiteSpace(txtUsos.Text) ||
                string.IsNullOrWhiteSpace(comboContenedor.Text) ||
                string.IsNullOrWhiteSpace(txtRiesgos.Text) || string.IsNullOrWhiteSpace(txtTelEmergencia.Text) ||
                string.IsNullOrWhiteSpace(txtNumeroCas.Text) || string.IsNullOrWhiteSpace(txtNumeroOnu.Text) ||
                string.IsNullOrWhiteSpace(txtNombreFabricante.Text) || string.IsNullOrWhiteSpace(txtTelFabricante.Text) ||
                string.IsNullOrWhiteSpace(txtDirFabricante.Text) || string.IsNullOrEmpty(txtRutaHds.Text))
            {
                MessageBox.Show("Debes de llenar todos los campos");
            }
            else
            {
                Task oTask = new Task(Algo);
                oTask.Start();
                addSustancia();
                await oTask;
                Administrador guardar = new Administrador(rol, idUsuario);
                this.Hide();
                guardar.ShowDialog();
                this.Close();
            }
        }

        //Métodos para hacer un Loading
        public void Algo()
        {
            //Tiempo que esperará 
            Thread.Sleep(3000);
        }

        public void Show()
        {
            //Inicializa el form salir y lo muestra
            guardando = new Guardando();
            guardando.Show();
        }

        public void Esconder()
        {
            //Método para cerrar el form salir
            if (guardando != null)
                guardando.Close();
        }

        private void checkBoxTapones_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void addSustancia()
        {

            Clases.CEquipopp objectEquipo = new Clases.CEquipopp();
            Clases.CPictograma objectPictograma = new Clases.CPictograma();
            ////////////////////////////////////////////////////////
            String nombreSustancia = txtNombreSustancia.Text;
            String palabraAdvertencia = comboPalabraAdvertencia.Text;
            String riesgosSustancia = txtRiesgos.Text;
            String numeroCas = txtNumeroCas.Text;
            String numeroOnu = txtNumeroOnu.Text;
            String telEmergencia = txtTelEmergencia.Text;
            String nombreFabricante = txtNombreFabricante.Text;
            String telFabricante = txtTelFabricante.Text;
            String direccionFabricante = txtDirFabricante.Text;

            byte[] pdfBytes = null;
            List<int> picto = new List<int>();
            List<int> equipopp = new List<int>();

            List<Clases.CPictograma> listaPicto = new List<Clases.CPictograma>();
            listaPicto = cPictograma.selectPictograma();

            List<Clases.CEquipopp> listaEpp = new List<Clases.CEquipopp>();
            listaEpp = cEquipopp.selectEpp();
            /////////////////////////////////////////////////////
            using (MemoryStream memoryStreamPdf = new MemoryStream())
            {
                // Crear un escritor PDF asociado al MemoryStream
                using (PdfWriter pdfWriter = new PdfWriter(memoryStreamPdf)) //LINE 149
                {
                    // Crear un documento PDF
                    using (PdfDocument pdf = new PdfDocument(pdfWriter))
                    {
                        // Crear un objeto Document
                        using (Document documento = new Document(pdf, PageSize.LETTER))
                        {
                            var nombreEtiqueta = @"Etiqueta " + nombreSustancia + ".pdf";
                            objectSustancia.EtiquetaExtension = nombreEtiqueta;
                            //MessageBox.Show("EL NOMBRE DE LA ETIQUETA EN CREARPDF(): " + nombreEtiqueta);
                            PdfFont fontColumnas = PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD);
                            PdfFont fontContenido = PdfFontFactory.CreateFont(StandardFonts.HELVETICA);

                            documento.SetMargins(60, 20, 55, 20);
                            documento.SetFont(fontColumnas);

                            ///////////////////////////////////////////

                            // a table with three columns
                            Table table = new Table(UnitValue.CreatePercentArray(3)).UseAllAvailableWidth();
                            // the cell object
                            Cell cell;
                            // we add a cell with colspan 3
                            cell = new Cell(1, 3).Add(new Paragraph("ETIQUETA DE SEGURIDAD"));
                            table.AddCell(cell);
                            // now we add a cell with rowspan 2
                            cell = new Cell(2, 1).Add(new Paragraph("" + nombreSustancia));
                            table.AddCell(cell);
                            // now we add a cell with rowspan 2
                            cell = new Cell(2, 1).Add(new Paragraph("" + palabraAdvertencia));
                            table.AddCell(cell);
                            // we add the four remaining cells with addCell()
                            //table.AddCell("row 1; cell 1");
                            table.AddCell("CAS: " + numeroCas);
                            //Table.AddCell("row 2; cell 1");
                            table.AddCell("ONU: " + numeroOnu);

                            if (checkBoxCorrosion.Checked)
                            {
                                CPictograma pictogramaCorrosion = listaPicto.FirstOrDefault(p => p.IdPictograma == 1); // Suponiendo que el ID 1 corresponde a la imagen de corrosión
                                if (pictogramaCorrosion != null)
                                {
                                    pictogramas.Add(pictogramaCorrosion.ImagenPictograma);
                                    picto.Add(1);
                                }
                            }
                            if (checkBoxAmbiente.Checked)
                            {
                                CPictograma pictogramaMedioAmbiente = listaPicto.FirstOrDefault(p => p.IdPictograma == 2);
                                if (pictogramaMedioAmbiente != null)
                                {
                                    pictogramas.Add(pictogramaMedioAmbiente.ImagenPictograma);
                                    picto.Add(2);
                                }
                            }
                            if (checkBoxExplosivos.Checked)
                            {
                                CPictograma pictogramaExplosivos = listaPicto.FirstOrDefault(p => p.IdPictograma == 3);
                                if (pictogramaExplosivos != null)
                                {
                                    pictogramas.Add(pictogramaExplosivos.ImagenPictograma);
                                    picto.Add(3);
                                }
                            }
                            if (checkBoxGasPresion.Checked)
                            {
                                CPictograma pictogramaGas = listaPicto.FirstOrDefault(p => p.IdPictograma == 4);
                                if (pictogramaGas != null)
                                {
                                    pictogramas.Add(pictogramaGas.ImagenPictograma);
                                    picto.Add(4);
                                }
                            }
                            if (checkBoxInflamable.Checked)
                            {
                                CPictograma pictogramaInflamable = listaPicto.FirstOrDefault(p => p.IdPictograma == 5);
                                if (pictogramaInflamable != null)
                                {
                                    pictogramas.Add(pictogramaInflamable.ImagenPictograma);
                                    picto.Add(5);
                                }
                            }
                            if (checkBoxIrritante.Checked)
                            {
                                CPictograma pictogramaIrritante = listaPicto.FirstOrDefault(p => p.IdPictograma == 6);
                                if (pictogramaIrritante != null)
                                {
                                    pictogramas.Add(pictogramaIrritante.ImagenPictograma);
                                    picto.Add(6);
                                }
                            }
                            if (checkBoxOxidante.Checked)
                            {
                                CPictograma pictogramaOxidante = listaPicto.FirstOrDefault(p => p.IdPictograma == 7);
                                if (pictogramaOxidante != null)
                                {
                                    pictogramas.Add(pictogramaOxidante.ImagenPictograma);
                                    picto.Add(7);
                                }
                            }
                            if (checkBoxSalud.Checked)
                            {
                                CPictograma pictogramaSalud = listaPicto.FirstOrDefault(p => p.IdPictograma == 8);
                                if (pictogramaSalud != null)
                                {
                                    pictogramas.Add(pictogramaSalud.ImagenPictograma);
                                    picto.Add(8);
                                }
                            }
                            if (checkBoxToxico.Checked)
                            {
                                CPictograma pictogramaToxico = listaPicto.FirstOrDefault(p => p.IdPictograma == 9);
                                if (pictogramaToxico != null)
                                {
                                    pictogramas.Add(pictogramaToxico.ImagenPictograma);
                                    picto.Add(9);
                                }
                            }

                            Table pictograma = new Table(3).SetBorder(Border.NO_BORDER);
                            pictograma.SetBorder(Border.NO_BORDER);
                            foreach (byte[] imageData in pictogramas)
                            {
                                var foto = new iText.Layout.Element.Image(ImageDataFactory.Create(imageData)).SetWidth(50);
                                pictograma.AddCell(foto).SetBorder(Border.NO_BORDER);
                            }
                            cell = new Cell(3, 1).Add(pictograma);
                            table.AddCell(cell);
                            //table.AddCell(pictograma).SetBorder(Border.NO_BORDER);

                            // now we add a cell with rowspan 2
                            cell = new Cell(3, 3).Add(new Paragraph("" + riesgosSustancia).SetFont(fontContenido));
                            table.AddCell(cell);

                            // now we add a cell with rowspan 2
                            cell = new Cell(4, 1).Add(new Paragraph("Num. de Emergencia"));
                            table.AddCell(cell);

                            // now we add a cell with rowspan 2
                            cell = new Cell(4, 2).Add(new Paragraph("Fabricante"));
                            table.AddCell(cell);

                            // now we add a cell with rowspan 2
                            cell = new Cell(3, 1).Add(new Paragraph("" + telEmergencia).SetFont(fontContenido));
                            table.AddCell(cell);

                            // now we add a cell with rowspan 2
                            cell = new Cell(3, 2).Add(new Paragraph(nombreFabricante + "\n" + telFabricante + "\n" + direccionFabricante).SetFont(fontContenido));
                            table.AddCell(cell);

                            ////////// VALIDACION DE CHECK EPP ///////////////////

                            cell = new Cell(5, 3).Add(new Paragraph("USO DE EPP"));
                            table.AddCell(cell);

                            if (checkBoxBota.Checked)
                            {
                                CEquipopp botas = listaEpp.FirstOrDefault(p => p.IdEquipo == 1);
                                if (botas != null)
                                {
                                    epps.Add(botas.ImagenEquipo);
                                    equipopp.Add(1);
                                }
                            }
                            if (checkBoxTapones.Checked)
                            {
                                CEquipopp tapones = listaEpp.FirstOrDefault(p => p.IdEquipo == 2);
                                if (tapones != null)
                                {
                                    epps.Add(tapones.ImagenEquipo);
                                    equipopp.Add(2);
                                }
                            }
                            if (checkBoxGuantes.Checked)
                            {
                                CEquipopp guantes = listaEpp.FirstOrDefault(p => p.IdEquipo == 3);
                                if (guantes != null)
                                {
                                    epps.Add(guantes.ImagenEquipo);
                                    equipopp.Add(3);
                                }
                            }
                            if (checkBoxLentes.Checked)
                            {
                                CEquipopp lentes = listaEpp.FirstOrDefault(p => p.IdEquipo == 4);
                                if (lentes != null)
                                {
                                    epps.Add(lentes.ImagenEquipo);
                                    equipopp.Add(4);
                                }
                            }
                            if (checkBoxMascarilla.Checked)
                            {
                                CEquipopp mascarilla = listaEpp.FirstOrDefault(p => p.IdEquipo == 5);
                                if (mascarilla != null)
                                {
                                    epps.Add(mascarilla.ImagenEquipo);
                                    equipopp.Add(5);
                                }
                            }
                            if (checkBoxTraje.Checked)
                            {
                                CEquipopp traje = listaEpp.FirstOrDefault(p => p.IdEquipo == 6);
                                if (traje != null)
                                {
                                    epps.Add(traje.ImagenEquipo);
                                    equipopp.Add(6);
                                }
                            }

                            Table epp = new Table(6).SetBorder(Border.NO_BORDER);
                            epp.SetBorder(Border.NO_BORDER);
                            foreach (byte[] pics in epps)
                            {
                                var foto = new iText.Layout.Element.Image(ImageDataFactory.Create(pics)).SetWidth(50);
                                epp.AddCell(foto).SetBorder(Border.NO_BORDER);
                                //pictograma.AddCell("").SetBorder(Border.NO_BORDER);
                            }
                            cell = new Cell(6, 3).Add(epp);
                            table.AddCell(cell);
                            /////////////////////////////////////////////
                            documento.Add(table);
                            //////////////////////////////////////////
                        }
                    }
                }
                // Convertir el MemoryStream en un arreglo de bytes
                pdfBytes = memoryStreamPdf.ToArray();
                objectSustancia.EtiquetaSustancia = pdfBytes;
            }

            ////////////////////////////////////////////////////////////

            byte[] archivo = null;
            Stream myStream = openFileDialog1.OpenFile();
            MemoryStream memoryStream = new MemoryStream();
            myStream.CopyTo(memoryStream);
            archivo = memoryStream.ToArray();
            //MessageBox.Show("EL PRIMER DOCUMENTO ES: "+ archivo.ToString());

            byte[] imagen = null;
            try
            {
                Stream stream = openFileDialog2.OpenFile();
                MemoryStream mmStream = new MemoryStream();
                stream.CopyTo(mmStream);
                imagen = mmStream.ToArray();
                objectSustancia.ImagenExtension = openFileDialog2.SafeFileName;
            }
            catch (Exception e)
            {
                //MessageBox.Show("OCURRIÓ UN ERROR: " + e);
                txtRutaFoto.Text = "sustancia.png";
                string direccion = AppDomain.CurrentDomain.BaseDirectory;
                string img = direccion + @"sustancia.png";
                imagen = File.ReadAllBytes(img);
                objectSustancia.ImagenExtension = img;
                //MessageBox.Show("LONGITUD DE LA IMAGEN: " + imagen.Length);
            }

            string extensionEtiqueta = objectSustancia.EtiquetaExtension;
            //MessageBox.Show("EXTENSION DE LA ETIQUETS EN ADDSUSTANCIA(): " + extensionEtiqueta);

            //Agregar
            objectSustancia.NombreSustancia = txtNombreSustancia.Text;
            objectSustancia.UsoRecomendado = txtUsos.Text;
            objectSustancia.PalabraAdvertencia = comboPalabraAdvertencia.Text;
            objectSustancia.TelefonoEmergencia = txtTelEmergencia.Text;
            objectSustancia.CantidadEstimada = txtCantidad.Text;
            objectSustancia.TipoContenedor = comboContenedor.Text;
            objectSustancia.ConsumoPromedio = txtConsumos.Text;
            objectSustancia.HdsExtension = openFileDialog1.SafeFileName;
            objectSustancia.HdsSustancia = archivo;
            //objectSustancia.ImagenExtension = openFileDialog2.SafeFileName;
            objectSustancia.ImagenSustancia = imagen;
            objectSustancia.RiesgosSustancia = txtRiesgos.Text;
            objectSustancia.NumeroCas = txtNumeroCas.Text;
            objectSustancia.NumeroOnu = txtNumeroOnu.Text;
            objectSustancia.NombreFabricante = txtNombreFabricante.Text;
            objectSustancia.TelefonoFabricante = txtTelFabricante.Text;
            objectSustancia.DireccionFabricante = txtDirFabricante.Text;
            objectSustancia.EtiquetaExtension = extensionEtiqueta;

            objectSustancia.agregarSustancia();

            int id = objectSustancia.UltimoIdInsertado();
            //MessageBox.Show("EL ID INSERTADO ES: " + id.ToString());
            objectSustancia.actualizarGaveta(chBoxGaveta, id);
            objectSustancia.actualizarArea(checkedListArea, id);
            objectPictograma.addPictogramas(picto, id);
            objectEquipo.addEquipopp(equipopp, id);
        }
    }
}