﻿namespace PruebaScreen
{
    partial class Solicitud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Solicitud));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNombreSolicitud = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFabricanteSolicitud = new System.Windows.Forms.TextBox();
            this.btnHds = new System.Windows.Forms.Button();
            this.btnFichaTecnica = new System.Windows.Forms.Button();
            this.btnEnviarSolicitud = new System.Windows.Forms.Button();
            this.btnCancelarSolcitud = new System.Windows.Forms.Button();
            this.txtRutaHds = new System.Windows.Forms.TextBox();
            this.txtRutaFicha = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Siemens Sans", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(165, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(877, 44);
            this.label1.TabIndex = 0;
            this.label1.Text = "Solicitud de aprobación de nueva sustancia química";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(53, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 63);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre del químico";
            // 
            // txtNombreSolicitud
            // 
            this.txtNombreSolicitud.Location = new System.Drawing.Point(225, 139);
            this.txtNombreSolicitud.Name = "txtNombreSolicitud";
            this.txtNombreSolicitud.Size = new System.Drawing.Size(866, 31);
            this.txtNombreSolicitud.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(53, 231);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 38);
            this.label3.TabIndex = 3;
            this.label3.Text = "Fabricante";
            // 
            // txtFabricanteSolicitud
            // 
            this.txtFabricanteSolicitud.Location = new System.Drawing.Point(225, 231);
            this.txtFabricanteSolicitud.Multiline = true;
            this.txtFabricanteSolicitud.Name = "txtFabricanteSolicitud";
            this.txtFabricanteSolicitud.Size = new System.Drawing.Size(866, 135);
            this.txtFabricanteSolicitud.TabIndex = 4;
            // 
            // btnHds
            // 
            this.btnHds.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnHds.Location = new System.Drawing.Point(991, 420);
            this.btnHds.Name = "btnHds";
            this.btnHds.Size = new System.Drawing.Size(60, 31);
            this.btnHds.TabIndex = 5;
            this.btnHds.Text = "...";
            this.btnHds.UseVisualStyleBackColor = true;
            this.btnHds.Click += new System.EventHandler(this.btnHds_Click);
            // 
            // btnFichaTecnica
            // 
            this.btnFichaTecnica.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnFichaTecnica.Location = new System.Drawing.Point(991, 495);
            this.btnFichaTecnica.Name = "btnFichaTecnica";
            this.btnFichaTecnica.Size = new System.Drawing.Size(60, 31);
            this.btnFichaTecnica.TabIndex = 6;
            this.btnFichaTecnica.Text = "...";
            this.btnFichaTecnica.UseVisualStyleBackColor = true;
            this.btnFichaTecnica.Click += new System.EventHandler(this.btnFichaTecnica_Click);
            // 
            // btnEnviarSolicitud
            // 
            this.btnEnviarSolicitud.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnEnviarSolicitud.Location = new System.Drawing.Point(984, 583);
            this.btnEnviarSolicitud.Name = "btnEnviarSolicitud";
            this.btnEnviarSolicitud.Size = new System.Drawing.Size(172, 50);
            this.btnEnviarSolicitud.TabIndex = 7;
            this.btnEnviarSolicitud.Text = "Aceptar";
            this.btnEnviarSolicitud.UseVisualStyleBackColor = true;
            this.btnEnviarSolicitud.Click += new System.EventHandler(this.btnEnviarSolicitud_Click);
            // 
            // btnCancelarSolcitud
            // 
            this.btnCancelarSolcitud.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnCancelarSolcitud.Location = new System.Drawing.Point(795, 583);
            this.btnCancelarSolcitud.Name = "btnCancelarSolcitud";
            this.btnCancelarSolcitud.Size = new System.Drawing.Size(172, 50);
            this.btnCancelarSolcitud.TabIndex = 8;
            this.btnCancelarSolcitud.Text = "Cancelar";
            this.btnCancelarSolcitud.UseVisualStyleBackColor = true;
            this.btnCancelarSolcitud.Click += new System.EventHandler(this.btnCancelarSolcitud_Click);
            // 
            // txtRutaHds
            // 
            this.txtRutaHds.Location = new System.Drawing.Point(225, 420);
            this.txtRutaHds.Name = "txtRutaHds";
            this.txtRutaHds.ReadOnly = true;
            this.txtRutaHds.Size = new System.Drawing.Size(760, 31);
            this.txtRutaHds.TabIndex = 9;
            // 
            // txtRutaFicha
            // 
            this.txtRutaFicha.Location = new System.Drawing.Point(225, 495);
            this.txtRutaFicha.Name = "txtRutaFicha";
            this.txtRutaFicha.ReadOnly = true;
            this.txtRutaFicha.Size = new System.Drawing.Size(760, 31);
            this.txtRutaFicha.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(53, 424);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 37);
            this.label4.TabIndex = 11;
            this.label4.Text = "Subir HDS";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(53, 485);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 50);
            this.label5.TabIndex = 12;
            this.label5.Text = "Subir ficha técnica";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "PDF (*.pdf)|*.pdf";
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            this.openFileDialog2.Filter = "PDF (*.pdf)|*.pdf";
            // 
            // Solicitud
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1168, 664);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtRutaFicha);
            this.Controls.Add(this.txtRutaHds);
            this.Controls.Add(this.btnCancelarSolcitud);
            this.Controls.Add(this.btnEnviarSolicitud);
            this.Controls.Add(this.btnFichaTecnica);
            this.Controls.Add(this.btnHds);
            this.Controls.Add(this.txtFabricanteSolicitud);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNombreSolicitud);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Solicitud";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Solicitud";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private Label label2;
        private TextBox txtNombreSolicitud;
        private Label label3;
        private TextBox txtFabricanteSolicitud;
        private Button btnHds;
        private Button btnFichaTecnica;
        private Button btnEnviarSolicitud;
        private Button btnCancelarSolcitud;
        private TextBox txtRutaHds;
        private TextBox txtRutaFicha;
        private Label label4;
        private Label label5;
        private OpenFileDialog openFileDialog1;
        private OpenFileDialog openFileDialog2;
    }
}