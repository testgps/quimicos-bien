﻿using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Kernel.Geom;
using iText.Kernel.Font;
using iText.IO.Font.Constants;
using iText.Layout.Properties;
using iText.IO.Image;
using iText.Layout.Borders;
using PruebaScreen.Clases;

namespace PruebaScreen
{
    public partial class Actualizar : Form
    {
        Clases.CSustancia cSustancia = new Clases.CSustancia();
        Clases.CPictograma cPictograma = new Clases.CPictograma();
        Clases.CEquipopp cEquipopp = new Clases.CEquipopp();
        List<byte[]> pictogramas = new List<byte[]>();
        List<byte[]> epps = new List<byte[]>();

        Guardando guardando;
        private int indice;
        private int rol;
        private int idUsuario;

        public Actualizar(int indice, int rol, int idUsuario)
        {
            InitializeComponent();
            this.indice = indice;
            this.rol = rol;
            this.idUsuario = idUsuario;
            List<Clases.CSustancia> lista = new List<Clases.CSustancia>();
            lista = cSustancia.sustanciaId(indice);
            int n = lista.Count();
            cSustancia.mostrarGaveta(checkBoxGaveta);
            cSustancia.mostrarArea(checkedListArea);
            for (int i = 0; i < n; i++)
            {
                txtNombreSustancia.Text = lista[i].NombreSustancia.ToString();
                txtUsos.Text = lista[i].UsoRecomendado.ToString();
                comboContenedor.Text = lista[i].TipoContenedor.ToString();
                txtCantidad.Text = lista[i].CantidadEstimada.ToString();
                txtConsumos.Text = lista[i].ConsumoPromedio.ToString();
                comboPalabra.Text = lista[i].PalabraAdvertencia.ToString();
                txtRiesgos.Text = lista[i].RiesgosSustancia.ToString();
                txtTelefonoEmergencia.Text = lista[i].TelefonoEmergencia.ToString();
                txtRutahds.Text = lista[i].HdsExtension.ToString();
                txtRutaFoto.Text = lista[i].ImagenExtension.ToString();
                txtCas.Text = lista[i].NumeroCas.ToString();
                txtOnu.Text = lista[i].NumeroOnu.ToString();
                txtNombreFabricante.Text = lista[i].NombreFabricante.ToString();
                txtTelefonoFabricante.Text = lista[i].TelefonoFabricante.ToString();
                txtDireccion.Text = lista[i].DireccionFabricante.ToString();
            }
            cSustancia.SeleccionarItems(checkBoxGaveta, indice);
            cSustancia.SeleccionarArea(checkedListArea, indice);
            cEquipopp.selectEquipopp(indice);

            List<int> elementosSeleccionados = cPictograma.selectPictogramas(indice);
            checkBoxCorrosion.Checked = false;
            checkBoxMedioAmbiente.Checked = false;
            checkBoxExplosivos.Checked = false;
            checkBoxBajoPresion.Checked = false;
            checkBoxInflamable.Checked = false;
            checkBoxIrritante.Checked = false;
            checkBoxOxidante.Checked = false;
            checkBoxPeligroSalud.Checked = false;
            checkBoxToxico.Checked = false;
            foreach (int elemento in elementosSeleccionados)
            {
                switch (elemento)
                {
                    case 1:
                        checkBoxCorrosion.Checked = true;
                        break;
                    case 2:
                        checkBoxMedioAmbiente.Checked = true;
                        break;
                    case 3:
                        checkBoxExplosivos.Checked = true;
                        break;
                    case 4:
                        checkBoxBajoPresion.Checked = true;
                        break;
                    case 5:
                        checkBoxInflamable.Checked = true;
                        break;
                    case 6:
                        checkBoxIrritante.Checked = true;
                        break;
                    case 7:
                        checkBoxOxidante.Checked = true;
                        break;
                    case 8:
                        checkBoxPeligroSalud.Checked = true;
                        break;
                    case 9:
                        checkBoxToxico.Checked = true;
                        break;
                }
            }
            List<int> elementosequipo = cEquipopp.selectEquipopp(indice);
            checkBoxBotas.Checked = false;
            checkBoxTapones.Checked = false;
            checkBoxLentes.Checked = false;
            checkBoxGuantes.Checked = false;
            checkBoxMascarilla.Checked = false;
            checkBoxTraje.Checked = false;
            foreach (int elemento in elementosequipo)
            {
                switch (elemento)
                {
                    case 1:
                        checkBoxBotas.Checked = true;
                        break;
                    case 2:
                        checkBoxTapones.Checked = true;
                        break;
                    case 3:
                        checkBoxLentes.Checked = true;
                        break;
                    case 4:
                        checkBoxGuantes.Checked = true;
                        break;
                    case 5:
                        checkBoxMascarilla.Checked = true;
                        break;
                    case 6:
                        checkBoxTraje.Checked = true;
                        break;
                }
            }
        }

        private void btnUpdateHds_Click(object sender, EventArgs e)
        {
            //Abrir el File Dialog
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtRutahds.Text = openFileDialog1.FileName;
            }
        }

        private void btnUpdateFoto_Click(object sender, EventArgs e)
        {
            //Abrir el File Dialog
            if (openFileDialog2.ShowDialog() == DialogResult.OK)
            {
                txtRutaFoto.Text = openFileDialog2.FileName;
            }
        }

        private async void btnUpdateS_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtNombreSustancia.Text) || string.IsNullOrWhiteSpace(txtUsos.Text) ||
                    string.IsNullOrWhiteSpace(comboContenedor.Text) ||
                    string.IsNullOrWhiteSpace(txtRiesgos.Text) ||
                    string.IsNullOrWhiteSpace(txtTelefonoEmergencia.Text) || string.IsNullOrWhiteSpace(txtCas.Text) ||
                    string.IsNullOrWhiteSpace(txtOnu.Text) || string.IsNullOrWhiteSpace(txtNombreFabricante.Text) ||
                    string.IsNullOrWhiteSpace(txtTelefonoFabricante.Text) || string.IsNullOrWhiteSpace(txtDireccion.Text) ||
                    string.IsNullOrEmpty(txtRutahds.Text) || string.IsNullOrEmpty(txtRutaFoto.Text))
                {
                    MessageBox.Show("Debes de llenar todos los campos");
                }
                else
                {
                    await Task.Run(updateSustancia); // LINE 201
                    MessageBox.Show("Se actualizó correctamente");
                    Administrador guardar = new Administrador(rol, idUsuario);
                    this.Hide();
                    guardar.ShowDialog();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se puede actualizar");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Administrador administrador = new Administrador(rol, idUsuario); //Llama al form principal de iniciar sesión
            this.Hide(); //Esconde la pantalla actual
            administrador.ShowDialog(); //Nos lleva a la pantalla que queremos
            this.Close(); //Cierra la pantalla actual
        }

        private void updateSustancia()
        {
            /////////////////////////////////
            List<Clases.CSustancia> lista = new List<Clases.CSustancia>();
            lista = cSustancia.sustanciaId(indice);
            int id = indice;
            lista[0].IdSustancia = id;

            //CONSULTAMOS EL DOC E IMAGEN
            byte[] archivo = lista[0].HdsSustancia;
            string extensionDoc = lista[0].HdsExtension.ToString();
            cSustancia.HdsExtension = extensionDoc;

            byte[] imagen = lista[0].ImagenSustancia;
            string extensionImg = lista[0].ImagenExtension.ToString();
            cSustancia.ImagenExtension = extensionImg;

            /////////// VALIDACIONES DE DOCUMENTOS 
            if (!txtRutahds.Text.Equals(extensionDoc) && !txtRutaFoto.Text.Equals(extensionImg))
            {
                //MessageBox.Show("CAMBIARON LOS 2");

                Stream myStream = openFileDialog1.OpenFile();
                MemoryStream memoryStream = new MemoryStream();
                myStream.CopyTo(memoryStream);
                archivo = memoryStream.ToArray();

                Stream stream = openFileDialog2.OpenFile();
                MemoryStream mmStream = new MemoryStream();
                stream.CopyTo(mmStream);
                imagen = mmStream.ToArray();

                cSustancia.HdsSustancia = archivo;
                cSustancia.HdsExtension = openFileDialog1.SafeFileName;
                cSustancia.ImagenSustancia = imagen;
                cSustancia.ImagenExtension = openFileDialog2.SafeFileName;
            }
            else if (!txtRutahds.Text.Equals(extensionDoc) || !txtRutaFoto.Text.Equals(extensionImg))
            {
                if (!txtRutahds.Text.Equals(extensionDoc))
                {
                    Stream myStream = openFileDialog1.OpenFile();
                    MemoryStream memoryStream = new MemoryStream();
                    myStream.CopyTo(memoryStream);
                    archivo = memoryStream.ToArray();

                    cSustancia.HdsSustancia = archivo;
                    cSustancia.HdsExtension = openFileDialog1.SafeFileName;
                    cSustancia.ImagenSustancia = imagen;
                    cSustancia.ImagenExtension = extensionImg;
                }
                else if (!txtRutaFoto.Text.Equals(extensionImg))
                {
                    Stream stream = openFileDialog2.OpenFile();
                    MemoryStream mmStream = new MemoryStream();
                    stream.CopyTo(mmStream);
                    imagen = mmStream.ToArray();

                    cSustancia.HdsSustancia = archivo;
                    cSustancia.HdsExtension = extensionDoc;
                    cSustancia.ImagenSustancia = imagen;
                    cSustancia.ImagenExtension = openFileDialog2.SafeFileName;
                }
            }
            else
            {
                cSustancia.HdsSustancia = archivo;
                cSustancia.HdsExtension = extensionDoc;
                cSustancia.ImagenSustancia = imagen;
                cSustancia.ImagenExtension = extensionImg;
            }

            cSustancia.NombreSustancia = txtNombreSustancia.Text;
            cSustancia.UsoRecomendado = txtUsos.Text;
            string palabraAdvertencia = string.Empty;
            comboPalabra.Invoke((MethodInvoker)(() =>
            {
                palabraAdvertencia = comboPalabra.Text;
            }));
            cSustancia.PalabraAdvertencia = palabraAdvertencia;
            cSustancia.TelefonoEmergencia = txtTelefonoEmergencia.Text;
            cSustancia.CantidadEstimada = txtCantidad.Text;
            string tipoContenedor = string.Empty;
            comboContenedor.Invoke((MethodInvoker)(() =>
            {
                tipoContenedor = comboContenedor.Text;
            }));
            cSustancia.TipoContenedor = tipoContenedor;
            cSustancia.ConsumoPromedio = txtConsumos.Text;
            cSustancia.RiesgosSustancia = txtRiesgos.Text;
            cSustancia.NumeroCas = txtCas.Text;
            cSustancia.NumeroOnu = txtOnu.Text;
            cSustancia.NombreFabricante = txtNombreFabricante.Text;
            cSustancia.TelefonoFabricante = txtTelefonoFabricante.Text;
            cSustancia.DireccionFabricante = txtDireccion.Text;

            cSustancia.actualizarGaveta(checkBoxGaveta, id);
            cSustancia.actualizarArea(checkedListArea, id);

            crearPdf(); //LINE 326
            cSustancia.editarSustancia(id);
            ///////////
        }

        private void crearPdf()
        {
            String nombreSustancia = txtNombreSustancia.Text;
            string palabraAdvertencia = string.Empty;
            comboPalabra.Invoke((MethodInvoker)(() =>
            {
                palabraAdvertencia = comboPalabra.Text;
            }));
            cSustancia.PalabraAdvertencia = palabraAdvertencia;
            String riesgosSustancia = txtRiesgos.Text;
            String numeroCas = txtCas.Text;
            String numeroOnu = txtOnu.Text;
            String telEmergencia = txtTelefonoEmergencia.Text;
            String nombreFabricante = txtNombreFabricante.Text;
            String telFabricante = txtTelefonoFabricante.Text;
            String direccionFabricante = txtDireccion.Text;

            int id = indice;
            byte[] pdfBytes = null;
            List<int> picto = new List<int>();
            List<int> equipopp = new List<int>();
            List<Clases.CPictograma> listaPicto = new List<Clases.CPictograma>();
            listaPicto = cPictograma.selectPictograma();

            List<Clases.CEquipopp> listaEpp = new List<CEquipopp>();
            listaEpp = cEquipopp.selectEpp();
            /////////////////////////////////////////////////////
            using (MemoryStream memoryStreamPdf = new MemoryStream())
            {
                // Crear un escritor PDF asociado al MemoryStream
                using (PdfWriter pdfWriter = new PdfWriter(memoryStreamPdf)) ////LINE 361
                {
                    // Crear un documento PDF
                    using (PdfDocument pdf = new PdfDocument(pdfWriter))
                    {
                        // Crear un objeto Document
                        using (Document documento = new Document(pdf, PageSize.LETTER))
                        {
                            var nombreEtiqueta = @"Etiqueta " + nombreSustancia + ".pdf";
                            cSustancia.EtiquetaExtension = nombreEtiqueta;
                            //MessageBox.Show("EL NOMBRE DE LA ETIQUETA EN CREARPDF(): " + nombreEtiqueta);
                            PdfFont fontColumnas = PdfFontFactory.CreateFont(StandardFonts.HELVETICA_BOLD);
                            PdfFont fontContenido = PdfFontFactory.CreateFont(StandardFonts.HELVETICA);

                            documento.SetMargins(60, 20, 55, 20);
                            documento.SetFont(fontColumnas);

                            ///////////////////////////////////////////

                            // a table with three columns
                            Table table = new Table(UnitValue.CreatePercentArray(3)).UseAllAvailableWidth();
                            // the cell object
                            Cell cell;
                            // we add a cell with colspan 3
                            cell = new Cell(1, 3).Add(new Paragraph("ETIQUETA DE SEGURIDAD"));
                            table.AddCell(cell);
                            // now we add a cell with rowspan 2
                            cell = new Cell(2, 1).Add(new Paragraph("" + nombreSustancia));
                            table.AddCell(cell);
                            // now we add a cell with rowspan 2
                            cell = new Cell(2, 1).Add(new Paragraph("" + palabraAdvertencia));
                            table.AddCell(cell);
                            // we add the four remaining cells with addCell()
                            //table.AddCell("row 1; cell 1");
                            table.AddCell("CAS: " + numeroCas);
                            //Table.AddCell("row 2; cell 1");
                            table.AddCell("ONU: " + numeroOnu);

                            // VALIDACION DE LOS CHECKBOX PICTOGRAMAS
                            if (checkBoxCorrosion.Checked)
                            {
                                CPictograma pictogramaCorrosion = listaPicto.FirstOrDefault(p => p.IdPictograma == 1); // Suponiendo que el ID 1 corresponde a la imagen de corrosión
                                if (pictogramaCorrosion != null)
                                {
                                    pictogramas.Add(pictogramaCorrosion.ImagenPictograma);
                                    picto.Add(1);
                                }
                            }
                            if (checkBoxMedioAmbiente.Checked)
                            {
                                CPictograma pictogramaMedioAmbiente = listaPicto.FirstOrDefault(p => p.IdPictograma == 2);
                                if (pictogramaMedioAmbiente != null)
                                {
                                    pictogramas.Add(pictogramaMedioAmbiente.ImagenPictograma);
                                    picto.Add(2);
                                }
                            }
                            if (checkBoxExplosivos.Checked)
                            {
                                CPictograma pictogramaExplosivos = listaPicto.FirstOrDefault(p => p.IdPictograma == 3);
                                if (pictogramaExplosivos != null)
                                {
                                    pictogramas.Add(pictogramaExplosivos.ImagenPictograma);
                                    picto.Add(3);
                                }
                            }
                            if (checkBoxBajoPresion.Checked)
                            {
                                CPictograma pictogramaGas = listaPicto.FirstOrDefault(p => p.IdPictograma == 4);
                                if (pictogramaGas != null)
                                {
                                    pictogramas.Add(pictogramaGas.ImagenPictograma);
                                    picto.Add(4);
                                }
                            }
                            if (checkBoxInflamable.Checked)
                            {
                                CPictograma pictogramaInflamable = listaPicto.FirstOrDefault(p => p.IdPictograma == 5);
                                if (pictogramaInflamable != null)
                                {
                                    pictogramas.Add(pictogramaInflamable.ImagenPictograma);
                                    picto.Add(5);
                                }
                            }
                            if (checkBoxIrritante.Checked)
                            {
                                CPictograma pictogramaIrritante = listaPicto.FirstOrDefault(p => p.IdPictograma == 6);
                                if (pictogramaIrritante != null)
                                {
                                    pictogramas.Add(pictogramaIrritante.ImagenPictograma);
                                    picto.Add(6);
                                }
                            }
                            if (checkBoxOxidante.Checked)
                            {
                                CPictograma pictogramaOxidante = listaPicto.FirstOrDefault(p => p.IdPictograma == 7);
                                if (pictogramaOxidante != null)
                                {
                                    pictogramas.Add(pictogramaOxidante.ImagenPictograma);
                                    picto.Add(7);
                                }
                            }
                            if (checkBoxPeligroSalud.Checked)
                            {
                                CPictograma pictogramaSalud = listaPicto.FirstOrDefault(p => p.IdPictograma == 8);
                                if (pictogramaSalud != null)
                                {
                                    pictogramas.Add(pictogramaSalud.ImagenPictograma);
                                    picto.Add(8);
                                }
                            }
                            if (checkBoxToxico.Checked)
                            {
                                CPictograma pictogramaToxico = listaPicto.FirstOrDefault(p => p.IdPictograma == 9);
                                if (pictogramaToxico != null)
                                {
                                    pictogramas.Add(pictogramaToxico.ImagenPictograma);
                                    picto.Add(9);
                                }
                            }

                            Table pictograma = new Table(3).SetBorder(Border.NO_BORDER);
                            pictograma.SetBorder(Border.NO_BORDER);
                            foreach (byte[] imageData in pictogramas)
                            {
                                var foto = new iText.Layout.Element.Image(ImageDataFactory.Create(imageData)).SetWidth(50);
                                pictograma.AddCell(foto).SetBorder(Border.NO_BORDER);
                            }
                            cell = new Cell(3, 1).Add(pictograma);
                            table.AddCell(cell);
                            //table.AddCell(pictograma).SetBorder(Border.NO_BORDER);

                            // now we add a cell with rowspan 2
                            cell = new Cell(3, 3).Add(new Paragraph("" + riesgosSustancia).SetFont(fontContenido));
                            table.AddCell(cell);

                            // now we add a cell with rowspan 2
                            cell = new Cell(4, 1).Add(new Paragraph("Num. de Emergencia"));
                            table.AddCell(cell);

                            // now we add a cell with rowspan 2
                            cell = new Cell(4, 2).Add(new Paragraph("Fabricante"));
                            table.AddCell(cell);

                            // now we add a cell with rowspan 2
                            cell = new Cell(3, 1).Add(new Paragraph("" + telEmergencia).SetFont(fontContenido));
                            table.AddCell(cell);

                            // now we add a cell with rowspan 2
                            cell = new Cell(3, 2).Add(new Paragraph(nombreFabricante + "\n" + telFabricante + "\n" + direccionFabricante).SetFont(fontContenido));
                            table.AddCell(cell);

                            ////////// VALIDACION DE CHECK EPP ///////////////////

                            cell = new Cell(5, 3).Add(new Paragraph("USO DE EPP"));
                            table.AddCell(cell);

                            if (checkBoxBotas.Checked)
                            {
                                CEquipopp botas = listaEpp.FirstOrDefault(p => p.IdEquipo == 1);
                                if (botas != null)
                                {
                                    epps.Add(botas.ImagenEquipo);
                                    equipopp.Add(1);
                                }
                            }
                            if (checkBoxTapones.Checked)
                            {
                                CEquipopp tapones = listaEpp.FirstOrDefault(p => p.IdEquipo == 2);
                                if (tapones != null)
                                {
                                    epps.Add(tapones.ImagenEquipo);
                                    equipopp.Add(2);
                                }
                            }
                            if (checkBoxGuantes.Checked)
                            {
                                CEquipopp guantes = listaEpp.FirstOrDefault(p => p.IdEquipo == 3);
                                if (guantes != null)
                                {
                                    epps.Add(guantes.ImagenEquipo);
                                    equipopp.Add(3);
                                }
                            }
                            if (checkBoxLentes.Checked)
                            {
                                CEquipopp lentes = listaEpp.FirstOrDefault(p => p.IdEquipo == 4);
                                if (lentes != null)
                                {
                                    epps.Add(lentes.ImagenEquipo);
                                    equipopp.Add(4);
                                }
                            }
                            if (checkBoxMascarilla.Checked)
                            {
                                CEquipopp mascarilla = listaEpp.FirstOrDefault(p => p.IdEquipo == 5);
                                if (mascarilla != null)
                                {
                                    epps.Add(mascarilla.ImagenEquipo);
                                    equipopp.Add(5);
                                }
                            }
                            if (checkBoxTraje.Checked)
                            {
                                CEquipopp traje = listaEpp.FirstOrDefault(p => p.IdEquipo == 6);
                                if (traje != null)
                                {
                                    epps.Add(traje.ImagenEquipo);
                                    equipopp.Add(6);
                                }
                            }

                            Table epp = new Table(6).SetBorder(Border.NO_BORDER);
                            epp.SetBorder(Border.NO_BORDER);
                            foreach (byte[] pics in epps)
                            {
                                var foto = new iText.Layout.Element.Image(ImageDataFactory.Create(pics)).SetWidth(50);
                                epp.AddCell(foto).SetBorder(Border.NO_BORDER);
                                //pictograma.AddCell("").SetBorder(Border.NO_BORDER);
                            }
                            cell = new Cell(6, 3).Add(epp);
                            table.AddCell(cell);
                            /////////////////////////////////////////////
                            documento.Add(table);
                            //////////////////////////////////////////
                        }
                    }
                }
                // Convertir el MemoryStream en un arreglo de bytes
                pdfBytes = memoryStreamPdf.ToArray();

                cSustancia.EtiquetaSustancia = pdfBytes;
                //MessageBox.Show("PDF BYTES: "+ pdfBytes.Length);

                cPictograma.addPictogramas(picto, id);
                cEquipopp.addEquipopp(equipopp, id);
            }
        }
    }
}
