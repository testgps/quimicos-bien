﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaScreen
{
    public partial class AgregarArea : Form
    {
        Clases.CArea cArea = new Clases.CArea();
        public AgregarArea()
        {
            InitializeComponent();
            cArea.mostrarArea(dataGridView1);
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            cArea.seleccionarArea(dataGridView1, txtIdArea, txtNombreArea);
        } 

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            int areaExistente = cArea.seleccionarAreaExistente(txtNombreArea.Text);
            if (areaExistente == 1)
            {
                MessageBox.Show("El área ya existe");
            }
            else
            {
                if (string.IsNullOrEmpty(txtNombreArea.Text))
                {
                    MessageBox.Show("Debes ingresar los campos correspondientes");
                }
                else
                {
                    cArea.NombreArea = txtNombreArea.Text;
                    cArea.agregarArea();
                    cArea.mostrarArea(dataGridView1);
                    txtIdArea.Text = "";
                    txtNombreArea.Text = "";
                }
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            int areaExistente = cArea.seleccionarAreaExistente(txtNombreArea.Text);
            if (areaExistente == 1)
            {
                MessageBox.Show("El área ya existe");
            }
            else
            {
                if (string.IsNullOrEmpty(txtIdArea.Text))
                {
                    MessageBox.Show("Debes seleccionar un área a editar");
                }
                else
                {
                    if (string.IsNullOrEmpty(txtNombreArea.Text))
                    {
                        MessageBox.Show("Debes ingresar los campos correspondientes");
                    }
                    else
                    {
                        cArea.NombreArea = txtNombreArea.Text;
                        cArea.editarArea(txtIdArea);
                        cArea.mostrarArea(dataGridView1);
                        txtIdArea.Text = "";
                        txtNombreArea.Text = "";
                        MessageBox.Show("Área modificada correctamente");
                    }
                }
            }
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtIdArea.Text))
            {
                MessageBox.Show("Debes seleccionar un área");
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("¿Seguro que deseas eliminar " + txtNombreArea.Text + "?",
                "Eliminar área", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    cArea.eliminarArea(txtIdArea);
                    cArea.mostrarArea(dataGridView1);
                    txtIdArea.Text = "";
                    txtNombreArea.Text = "";
                }
            }
        }
    }
}
