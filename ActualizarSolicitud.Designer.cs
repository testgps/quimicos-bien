﻿namespace PruebaScreen
{
    partial class ActualizarSolicitud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActualizarSolicitud));
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRutaFicha = new System.Windows.Forms.TextBox();
            this.txtRutaHds = new System.Windows.Forms.TextBox();
            this.btnCancelarSolcitud = new System.Windows.Forms.Button();
            this.btnActualizarSolicitud = new System.Windows.Forms.Button();
            this.btnFichaTecnica = new System.Windows.Forms.Button();
            this.btnHds = new System.Windows.Forms.Button();
            this.txtFabricanteSolicitud = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNombreSolicitud = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(33, 482);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 50);
            this.label5.TabIndex = 25;
            this.label5.Text = "Actualizar ficha técnica";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(33, 408);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 51);
            this.label4.TabIndex = 24;
            this.label4.Text = "Actualizar HDS";
            // 
            // txtRutaFicha
            // 
            this.txtRutaFicha.Location = new System.Drawing.Point(205, 492);
            this.txtRutaFicha.Name = "txtRutaFicha";
            this.txtRutaFicha.ReadOnly = true;
            this.txtRutaFicha.Size = new System.Drawing.Size(760, 31);
            this.txtRutaFicha.TabIndex = 23;
            // 
            // txtRutaHds
            // 
            this.txtRutaHds.Location = new System.Drawing.Point(205, 417);
            this.txtRutaHds.Name = "txtRutaHds";
            this.txtRutaHds.ReadOnly = true;
            this.txtRutaHds.Size = new System.Drawing.Size(760, 31);
            this.txtRutaHds.TabIndex = 22;
            // 
            // btnCancelarSolcitud
            // 
            this.btnCancelarSolcitud.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnCancelarSolcitud.Location = new System.Drawing.Point(775, 580);
            this.btnCancelarSolcitud.Name = "btnCancelarSolcitud";
            this.btnCancelarSolcitud.Size = new System.Drawing.Size(172, 50);
            this.btnCancelarSolcitud.TabIndex = 21;
            this.btnCancelarSolcitud.Text = "Cancelar";
            this.btnCancelarSolcitud.UseVisualStyleBackColor = true;
            this.btnCancelarSolcitud.Click += new System.EventHandler(this.btnCancelarSolcitud_Click);
            // 
            // btnActualizarSolicitud
            // 
            this.btnActualizarSolicitud.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnActualizarSolicitud.Location = new System.Drawing.Point(964, 580);
            this.btnActualizarSolicitud.Name = "btnActualizarSolicitud";
            this.btnActualizarSolicitud.Size = new System.Drawing.Size(172, 50);
            this.btnActualizarSolicitud.TabIndex = 20;
            this.btnActualizarSolicitud.Text = "Aceptar";
            this.btnActualizarSolicitud.UseVisualStyleBackColor = true;
            this.btnActualizarSolicitud.Click += new System.EventHandler(this.btnActualizarSolicitud_Click);
            // 
            // btnFichaTecnica
            // 
            this.btnFichaTecnica.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnFichaTecnica.Location = new System.Drawing.Point(971, 492);
            this.btnFichaTecnica.Name = "btnFichaTecnica";
            this.btnFichaTecnica.Size = new System.Drawing.Size(60, 31);
            this.btnFichaTecnica.TabIndex = 19;
            this.btnFichaTecnica.Text = "...";
            this.btnFichaTecnica.UseVisualStyleBackColor = true;
            this.btnFichaTecnica.Click += new System.EventHandler(this.btnFichaTecnica_Click);
            // 
            // btnHds
            // 
            this.btnHds.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnHds.Location = new System.Drawing.Point(971, 417);
            this.btnHds.Name = "btnHds";
            this.btnHds.Size = new System.Drawing.Size(60, 31);
            this.btnHds.TabIndex = 18;
            this.btnHds.Text = "...";
            this.btnHds.UseVisualStyleBackColor = true;
            this.btnHds.Click += new System.EventHandler(this.btnHds_Click);
            // 
            // txtFabricanteSolicitud
            // 
            this.txtFabricanteSolicitud.Location = new System.Drawing.Point(205, 228);
            this.txtFabricanteSolicitud.Multiline = true;
            this.txtFabricanteSolicitud.Name = "txtFabricanteSolicitud";
            this.txtFabricanteSolicitud.Size = new System.Drawing.Size(866, 135);
            this.txtFabricanteSolicitud.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(33, 228);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 38);
            this.label3.TabIndex = 16;
            this.label3.Text = "Fabricante";
            // 
            // txtNombreSolicitud
            // 
            this.txtNombreSolicitud.Location = new System.Drawing.Point(205, 136);
            this.txtNombreSolicitud.Name = "txtNombreSolicitud";
            this.txtNombreSolicitud.Size = new System.Drawing.Size(866, 31);
            this.txtNombreSolicitud.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(33, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 63);
            this.label2.TabIndex = 14;
            this.label2.Text = "Nombre del químico";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Siemens Sans", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(72, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1049, 44);
            this.label1.TabIndex = 13;
            this.label1.Text = "Actualizar solicitud de aprobación de nueva sustancia química";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            // 
            // ActualizarSolicitud
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1168, 664);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtRutaFicha);
            this.Controls.Add(this.txtRutaHds);
            this.Controls.Add(this.btnCancelarSolcitud);
            this.Controls.Add(this.btnActualizarSolicitud);
            this.Controls.Add(this.btnFichaTecnica);
            this.Controls.Add(this.btnHds);
            this.Controls.Add(this.txtFabricanteSolicitud);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNombreSolicitud);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ActualizarSolicitud";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ActualizarSolicitud";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label5;
        private Label label4;
        private TextBox txtRutaFicha;
        private TextBox txtRutaHds;
        private Button btnCancelarSolcitud;
        private Button btnActualizarSolicitud;
        private Button btnFichaTecnica;
        private Button btnHds;
        private TextBox txtFabricanteSolicitud;
        private Label label3;
        private TextBox txtNombreSolicitud;
        private Label label2;
        private Label label1;
        private OpenFileDialog openFileDialog1;
        private OpenFileDialog openFileDialog2;
    }
}