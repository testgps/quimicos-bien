﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebaScreen.Clases;
using System.Data;
using System.Data.SqlClient;

namespace PruebaScreen.Clases
{
    public class CEquipopp
    {
        private int idEquipo;
        private string nombreEquipo;
        private byte[] imagenEquipo;

        public int IdEquipo { get => idEquipo; set => idEquipo = value; }
        public string NombreEquipo { get => nombreEquipo; set => nombreEquipo = value; }
        public byte[] ImagenEquipo { get => imagenEquipo; set => imagenEquipo = value; }

        CConexion objectConexion = new CConexion();

        public void agregarEquipo()
        {
            try
            {
                SqlCommand comando = new SqlCommand("INSERT INTO EquipoProteccionPersonal VALUES(@nombreEquipo, @equipo)",
                    objectConexion.establecerConexion());

                comando.CommandType = CommandType.Text;
                comando.Parameters.AddWithValue("@nombreEquipo", NombreEquipo);
                comando.Parameters.AddWithValue("@equipo", ImagenEquipo);
                comando.ExecuteNonQuery();

                //MessageBox.Show("Se ejecutó correctamentee");
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró insertar. Consulte a su administrador.");
            }
        }

        public void addEquipopp(List<int> equipopp, int id)
        {
            try
            {
                int idSustancia = id;
                // Eliminar los registros existentes en la tabla SustanciaPictograma para el idSustancia específico
                string deleteQuery = "DELETE FROM SustanciaEpp WHERE idSustancia = @idSustancia";
                SqlCommand deleteCommand = new SqlCommand(deleteQuery,
                    objectConexion.establecerConexion());
                deleteCommand.Parameters.AddWithValue("@idSustancia", idSustancia);
                deleteCommand.ExecuteNonQuery();
                objectConexion.cerrarConexion();

                foreach (int i in equipopp)
                {
                    string query = "INSERT INTO SustanciaEpp (idSustancia, idEquipo) " +
                        "VALUES (@idSustancia, @idEquipo)";
                    using (SqlCommand command = new SqlCommand(query, objectConexion.establecerConexion()))
                    {
                        command.Parameters.AddWithValue("@idSustancia", idSustancia);
                        command.Parameters.AddWithValue("@idEquipo", i);
                        command.ExecuteNonQuery();
                        objectConexion.cerrarConexion();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error seleccionando los EPP. Consulte a su administrador");
            }
        }

        public List<int> selectEquipopp(int indice)
        {
            List<int> elementosSeleccionados = new List<int>();
            try
            {
                string query = "SELECT idEquipo FROM SustanciaEpp WHERE idSustancia = " + indice;
                SqlCommand command = new SqlCommand(query, objectConexion.establecerConexion());
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int idEquipo = Convert.ToInt32(reader["idEquipo"]);
                    elementosSeleccionados.Add(idEquipo);

                }
                objectConexion.cerrarConexion();
                return elementosSeleccionados;
            }
            catch (Exception e)
            {
                MessageBox.Show("Error seleccionando items de EPP. Consulte a su administrador");
                return elementosSeleccionados;
            }
        }

        public List<CEquipopp> GetEquipoById(int indice)
        {
            List<CEquipopp> lista = new List<CEquipopp>();
            DataTable dt = new DataTable();
            string SQL = string.Empty;
            SQL += "SELECT SustanciaEpp.idEquipo, EquipoProteccionPersonal.imagenEquipo";
            SQL += " FROM Sustancia, SustanciaEpp, EquipoProteccionPersonal";
            SQL += " WHERE SustanciaEpp.idSustancia = Sustancia.idSustancia";
            SQL += " AND SustanciaEpp.idEquipo = EquipoProteccionPersonal.idEquipo AND SustanciaEpp.idSustancia = " + indice;
            SqlCommand command = new SqlCommand(SQL, objectConexion.establecerConexion());
            SqlDataReader reader = command.ExecuteReader();
            dt.Load(reader);
            reader.Close();
            objectConexion.cerrarConexion();
            foreach (DataRow row in dt.Rows)
            {
                CEquipopp objeto = new CEquipopp();
                objeto.IdEquipo = Convert.ToInt32(row["idEquipo"].ToString());
                objeto.ImagenEquipo = (byte[])row["imagenEquipo"];
                lista.Add(objeto);
            }
            return lista;
        }

        public List<CEquipopp> selectEpp()
        {
            List<CEquipopp> lista = new List<CEquipopp>();
            try
            {
                string SQL = string.Empty;
                SQL += "SELECT idEquipo, imagenEquipo FROM EquipoProteccionPersonal ORDER BY idEquipo ASC";

                SqlCommand command = new SqlCommand(SQL, objectConexion.establecerConexion());

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                DataSet dataset = new DataSet();

                sqlDataAdapter.Fill(dataset, "Epp");
                objectConexion.cerrarConexion();

                foreach (DataRow row in dataset.Tables[0].Rows)
                {
                    CEquipopp objeto = new CEquipopp();
                    objeto.IdEquipo = Convert.ToInt32(row["idEquipo"]);
                    objeto.ImagenEquipo = (byte[])row["imagenEquipo"];
                    lista.Add(objeto);
                }
                objectConexion.cerrarConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("NO SE PUDIERON SELECCIONAR LOS EPP. Consulte a su administrador");
            }
            return lista;
        }

        public void eliminarSustanciaEpp(int id)
        {
            try
            {
                int idSustancia = id;

                // Eliminar los registros existentes en la tabla SustanciaGaveta para el idSustancia específico
                string deleteQuery = "DELETE FROM SustanciaEpp WHERE idSustancia = @idSustancia";
                SqlCommand deleteCommand = new SqlCommand(deleteQuery,
                    objectConexion.establecerConexion());
                deleteCommand.Parameters.AddWithValue("@idSustancia", idSustancia);
                deleteCommand.ExecuteNonQuery();
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error eliminando el epp. Consulte a su administrador.");
            }
        }
    }
}
