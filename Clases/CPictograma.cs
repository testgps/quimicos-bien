﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebaScreen.Clases;

namespace PruebaScreen.Clases
{
    public class CPictograma
    {
        private int idPictograma;
        private string nombrePictograma;
        private byte[] imagenPictograma;

        CConexion objectConexion = new CConexion();

        public int IdPictograma { get => idPictograma; set => idPictograma = value; }
        public string NombrePictograma { get => nombrePictograma; set => nombrePictograma = value; }
        public byte[] ImagenPictograma { get => imagenPictograma; set => imagenPictograma = value; }

        public void agregarPictogramas()
        {
            try
            {
                SqlCommand comando = new SqlCommand("INSERT INTO Pictograma VALUES(@nombrePictograma, @pictograma)",
                    objectConexion.establecerConexion());

                comando.CommandType = CommandType.Text;
                comando.Parameters.AddWithValue("@nombrePictograma", NombrePictograma);
                comando.Parameters.AddWithValue("@pictograma", ImagenPictograma);
                comando.ExecuteNonQuery();

                MessageBox.Show("Se ejecutó correctamente.");
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró insertar. Consulte a su administrador.");
            }
        }

        public void addPictogramas(List<int> picto, int id)
        {
            try
            {
                int idSustancia = id;
                // Eliminar los registros existentes en la tabla SustanciaPictograma para el idSustancia específico
                string deleteQuery = "DELETE FROM SustanciaPictograma WHERE idSustancia = @idSustancia";
                SqlCommand deleteCommand = new SqlCommand(deleteQuery,
                    objectConexion.establecerConexion());
                deleteCommand.Parameters.AddWithValue("@idSustancia", idSustancia);
                deleteCommand.ExecuteNonQuery();
                objectConexion.cerrarConexion();

                foreach (int i in picto)
                {
                    string query = "INSERT INTO SustanciaPictograma (idSustancia, idPictograma) " +
                        "VALUES (@idSustancia, @idPictograma)";
                    using (SqlCommand command = new SqlCommand(query, objectConexion.establecerConexion()))
                    {
                        command.Parameters.AddWithValue("@idSustancia", idSustancia);
                        command.Parameters.AddWithValue("@idPictograma", i);
                        command.ExecuteNonQuery();
                        objectConexion.cerrarConexion();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error agregando los pictogramas. Consulte a su administrador.");
            }
        }

        public List<int> selectPictogramas(int indice)
        {
            List<int> elementosSeleccionados = new List<int>();
            try
            {
                string query = "SELECT idPictograma FROM SustanciaPictograma WHERE idSustancia = " + indice;
                SqlCommand command = new SqlCommand(query, objectConexion.establecerConexion());
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int idPictograma = Convert.ToInt32(reader["idPictograma"]);
                    elementosSeleccionados.Add(idPictograma);
                }
                objectConexion.cerrarConexion();
                return elementosSeleccionados;
            }
            catch (Exception e)
            {
                MessageBox.Show("Error seleccionando items de pictogramas. Consulte a su administrador.");
                return elementosSeleccionados;
            }
        }

        public List<CPictograma> GetPictogramaById(int indice)
        {
            List<CPictograma> lista = new List<CPictograma>();
            DataTable dt = new DataTable();
            string SQL = string.Empty;
            SQL += "SELECT SustanciaPictograma.idPictograma, Pictograma.imagenPictograma";
            SQL += " FROM Sustancia, SustanciaPictograma, Pictograma";
            SQL += " WHERE SustanciaPictograma.idSustancia = Sustancia.idSustancia";
            SQL += " AND SustanciaPictograma.idPictograma = Pictograma.idPictograma AND SustanciaPictograma.idSustancia = " + indice;
            SqlCommand command = new SqlCommand(SQL, objectConexion.establecerConexion());
            SqlDataReader reader = command.ExecuteReader();
            dt.Load(reader);
            reader.Close();
            objectConexion.cerrarConexion();
            foreach (DataRow row in dt.Rows)
            {
                CPictograma objeto = new CPictograma();
                objeto.IdPictograma = Convert.ToInt32(row["idPictograma"].ToString());
                objeto.ImagenPictograma = (byte[])row["ImagenPictograma"];
                lista.Add(objeto);
            }
            return lista;
        }

        public List<CPictograma> selectPictograma()
        {
            List<CPictograma> lista = new List<CPictograma>();
            try
            {
                string SQL = string.Empty;
                SQL += "SELECT idPictograma, imagenPictograma FROM Pictograma ORDER BY idPictograma ASC";

                SqlCommand command = new SqlCommand(SQL, objectConexion.establecerConexion());

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                DataSet dataset = new DataSet();

                sqlDataAdapter.Fill(dataset, "Pictograma");
                objectConexion.cerrarConexion();

                foreach (DataRow row in dataset.Tables[0].Rows)
                {
                    CPictograma objeto = new CPictograma();
                    objeto.IdPictograma = Convert.ToInt32(row["idPictograma"]);
                    objeto.ImagenPictograma = (byte[])row["imagenPictograma"];
                    lista.Add(objeto);
                }
                objectConexion.cerrarConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("NO SE PUDIERON SELECCIONAR LOS PICTOGRAMAS. Consulte a su administrador");
            }
            return lista;
        }

        public void eliminarSustanciaPictograma(int id)
        {
            try
            {
                int idSustancia = id;

                // Eliminar los registros existentes en la tabla SustanciaGaveta para el idSustancia específico
                string deleteQuery = "DELETE FROM SustanciaPictograma WHERE idSustancia = @idSustancia";
                SqlCommand deleteCommand = new SqlCommand(deleteQuery,
                    objectConexion.establecerConexion());
                deleteCommand.Parameters.AddWithValue("@idSustancia", idSustancia);
                deleteCommand.ExecuteNonQuery();
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error eliminando pictogramas. Consulte a su administrador.");
            }
        }
    }
}
