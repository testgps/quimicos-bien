﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebaScreen.Clases;

namespace PruebaScreen.Clases
{
    public class CGaveta
    {
        private int idGaveta;
        private string nombreGaveta;
        private string areaGaveta;

        CConexion objectConexion = new CConexion();

        public int IdGaveta { get => idGaveta; set => idGaveta = value; }
        public string NombreGaveta { get => nombreGaveta; set => nombreGaveta = value; }
        public string AreaGaveta { get => areaGaveta; set => areaGaveta = value; }

        public void agregarGaveta()
        {
            int id = ultimaGavetaInsertada() + 1;
            try
            {
                SqlCommand comando = new SqlCommand("INSERT INTO Gaveta VALUES("+ id + ", @nombreGaveta, @areaGaveta)",
                    objectConexion.establecerConexion());

                comando.CommandType = CommandType.Text;
                comando.Parameters.AddWithValue("@nombreGaveta", NombreGaveta);
                comando.Parameters.AddWithValue("@areaGaveta", AreaGaveta);
                comando.ExecuteNonQuery();

                MessageBox.Show("Se insertó correctamente la gaveta");
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró insertar. Consulte a su administrador");
            }
        }

        public int ultimaGavetaInsertada()
        {
            int lastInsertedId = 0;
            try
            {
                string query = "SELECT TOP 1 idGaveta FROM Gaveta " +
                    "ORDER BY idGaveta DESC";
                SqlCommand command = new SqlCommand(query, objectConexion.establecerConexion());
                lastInsertedId = (int)command.ExecuteScalar();
                objectConexion.cerrarConexion();
                return lastInsertedId;
            }
            catch (Exception e)
            {
                MessageBox.Show("Error obteniendo los datos");
                return lastInsertedId;
            }
        }

        public List<CGaveta> GetGavetaById(int indice)
        {
            List<CGaveta> lista = new List<CGaveta>();
            DataTable dt = new DataTable();
            string SQL = string.Empty;
            SQL += "SELECT SustanciaGaveta.idGaveta, Gaveta.nombreGaveta";
            SQL += " FROM Sustancia, SustanciaGaveta, Gaveta";
            SQL += " WHERE SustanciaGaveta.idSustancia = Sustancia.idSustancia";
            SQL += " AND SustanciaGaveta.idGaveta = Gaveta.idGaveta AND SustanciaGaveta.idSustancia = " + indice;
            SqlCommand command = new SqlCommand(SQL, objectConexion.establecerConexion());
            SqlDataReader reader = command.ExecuteReader();
            dt.Load(reader);
            reader.Close();
            objectConexion.cerrarConexion();
            foreach (DataRow row in dt.Rows)
            {
                CGaveta objeto = new CGaveta();
                objeto.IdGaveta = Convert.ToInt32(row["idGaveta"].ToString());
                objeto.NombreGaveta = row["nombreGaveta"].ToString();
                lista.Add(objeto);
            }
            return lista;
        }

        public void mostrarGaveta(DataGridView tablaGaveta)
        {
            try
            {
                tablaGaveta.DataSource = null;
                SqlDataAdapter adapter = new SqlDataAdapter("SELECT idGaveta, nombreGaveta, areaGaveta " +
                    "FROM Gaveta ORDER BY idGaveta ASC",
                    objectConexion.establecerConexion());
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                tablaGaveta.DataSource = dt;
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se lograron mostrar las gavetas.");
            }
        }

        public void seleccionarGaveta(DataGridView tablaGaveta, TextBox paramId, TextBox paramNombre, ComboBox paramArea)
        {
            try
            {
                paramId.Text = tablaGaveta.CurrentRow.Cells[0].Value.ToString();
                paramNombre.Text = tablaGaveta.CurrentRow.Cells[1].Value.ToString();
                paramArea.Text = tablaGaveta.CurrentRow.Cells[2].Value.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se logró seleccionar. Consulte a su administrador.");
            }
        }

        public int seleccionarGavetaExistente(string paramNombre)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM Gaveta" +
                    " WHERE nombreGaveta = '" + paramNombre +"'", objectConexion.establecerConexion());

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                if (dt.Rows.Count == 1)
                {
                    return 1;
                }
                else if (dt.Rows.Count == 0)
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrió un error de conexión. Consulte a su administrador.");
            }
            finally
            {
                objectConexion.cerrarConexion();
            }
            return 0;
        }

        public void editarGaveta(TextBox paramId)
        {
            try
            {
                SqlCommand comando = new SqlCommand("UPDATE Gaveta SET nombreGaveta = @nombreGaveta," +
                    "areaGaveta = @areaGaveta WHERE Gaveta.idGaveta = '" + paramId.Text + "';",
                    objectConexion.establecerConexion());

                comando.Parameters.AddWithValue("@nombreGaveta", NombreGaveta);
                comando.Parameters.AddWithValue("@areaGaveta", AreaGaveta);
                comando.ExecuteNonQuery();
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró modificar. Consulte a su administrador.");
            }
        }

        public void eliminarGaveta(TextBox paramId)
        {
            try
            {
                String query = "DELETE FROM Gaveta WHERE Gaveta.idGaveta = '" + paramId.Text + "';";

                SqlCommand sqlCommand = new SqlCommand(query, objectConexion.establecerConexion());
                SqlDataReader reader = sqlCommand.ExecuteReader();

                MessageBox.Show("Se eliminó correctamente");

                while (reader.Read())
                {

                }
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró eliminar. Consulte a su administrador.");
            }
        }

        public void eliminarSustanciaGaveta(int id)
        {
            try
            {
                int idSustancia = id;

                // Eliminar los registros existentes en la tabla SustanciaGaveta para el idSustancia específico
                string deleteQuery = "DELETE FROM SustanciaGaveta WHERE idSustancia = @idSustancia";
                SqlCommand deleteCommand = new SqlCommand(deleteQuery,
                    objectConexion.establecerConexion());
                deleteCommand.Parameters.AddWithValue("@idSustancia", idSustancia);
                deleteCommand.ExecuteNonQuery();
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error eliminando items de gavetas. Consulte a su administrador.");
            }
        }
    }
}