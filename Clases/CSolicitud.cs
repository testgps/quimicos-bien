﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualBasic.ApplicationServices;
using System.Windows.Forms;

namespace PruebaScreen.Clases
{
    public class CSolicitud
    {
        private int idSolicitud;
        private string nombreSolicitud;
        private string fabricanteSolicitud;
        private DateTime fechaSolicitud;
        private string extensionHdsSolicitud;
        private byte[] hdsSolicitud;
        private string extensionFichaSolicitud;
        private byte[] fichaSolicitud;
        private string statusSolicitud;

        public int IdSolicitud { get => idSolicitud; set => idSolicitud = value; }
        public string NombreSolicitud { get => nombreSolicitud; set => nombreSolicitud = value; }
        public string FabricanteSolicitud { get => fabricanteSolicitud; set => fabricanteSolicitud = value; }
        public DateTime FechaSolicitud { get => fechaSolicitud; set => fechaSolicitud = value; }
        public string ExtensionHdsSolicitud { get => extensionHdsSolicitud; set => extensionHdsSolicitud = value; }
        public byte[] HdsSolicitud { get => hdsSolicitud; set => hdsSolicitud = value; }
        public string ExtensionFichaSolicitud { get => extensionFichaSolicitud; set => extensionFichaSolicitud = value; }
        public byte[] FichaSolicitud { get => fichaSolicitud; set => fichaSolicitud = value; }
        public string StatusSolicitud { get => statusSolicitud; set => statusSolicitud = value; }

        CConexion objectConexion = new CConexion();

        public void agregarSolicitud()
        {
            int id = UltimaSolicitudInsertada() + 1;
            try
            {
                SqlCommand comando = new SqlCommand("INSERT INTO Solicitud (idSolicitud, nombreSolicitud, fabricanteSolicitud, " +
                    "fechaSolicitud, extensionHdsSolicitud, hdsSolicitud, extensionFichaSolicitud, fichaSolicitud, statusSolicitud) " +
                    "VALUES("+ id +",@nombreSolicitud, @fabricanteSolicitud, @fechaSolicitud, @extensionHdsSolicitud, " +
                    "@hdsSolicitud, @extensionFichaSolicitud, @fichaSolicitud, @statusSolicitud)",
                    objectConexion.establecerConexion());

                comando.CommandType = CommandType.Text;
                comando.Parameters.AddWithValue("@nombreSolicitud", NombreSolicitud);
                comando.Parameters.AddWithValue("@fabricanteSolicitud", FabricanteSolicitud);
                comando.Parameters.AddWithValue("@fechaSolicitud", FechaSolicitud);
                comando.Parameters.AddWithValue("@extensionHdsSolicitud", ExtensionHdsSolicitud);
                comando.Parameters.AddWithValue("@hdsSolicitud", HdsSolicitud);
                comando.Parameters.AddWithValue("@extensionFichaSolicitud", ExtensionFichaSolicitud);
                comando.Parameters.AddWithValue("@fichaSolicitud", FichaSolicitud);
                comando.Parameters.AddWithValue("@statusSolicitud", StatusSolicitud);
                comando.ExecuteNonQuery();

                MessageBox.Show("Se insertó la solicitud correctamente");
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró insertar. Consulte a su administrador."+ e);
            }
        }

        public int UltimaSolicitudInsertada()
        {
            int lastInsertedId = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT TOP 1 idSolicitud FROM Solicitud " +
                    "ORDER BY idSolicitud DESC",objectConexion.establecerConexion());

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                objectConexion.cerrarConexion();

                if (dt.Rows.Count == 1)
                {
                    lastInsertedId = Convert.ToInt32(dt.Rows[0][0]);
                    return lastInsertedId;
                    //MessageBox.Show("EL ULTIMO ID ES: "+ lastInsertedId);
                }
                else if (dt.Rows.Count == 0)
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrió un error de conexión" + e);
            }
            finally
            {
                objectConexion.cerrarConexion();
            }
            return 0;
        }

        public List<CSolicitud> selectSolicitud()
        {
            List<CSolicitud> lista = new List<CSolicitud>();
            try
            {
                string SQL = string.Empty;
                SQL += "SELECT idSolicitud, nombreSolicitud, fabricanteSolicitud, fechaSolicitud, statusSolicitud";
                SQL += " FROM";
                SQL += " Solicitud ORDER BY fechaSolicitud DESC";
                SqlCommand command = new SqlCommand(SQL, objectConexion.establecerConexion());
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                DataSet dataset = new DataSet();

                sqlDataAdapter.Fill(dataset, "Solicitud");
                objectConexion.cerrarConexion();

                foreach (DataRow row in dataset.Tables[0].Rows)
                {
                    CSolicitud objeto = new CSolicitud();
                    objeto.IdSolicitud = Convert.ToInt32(row["idSolicitud"].ToString());
                    objeto.NombreSolicitud = row["nombreSolicitud"].ToString();
                    objeto.FabricanteSolicitud = row["fabricanteSolicitud"].ToString();
                    objeto.FechaSolicitud = (DateTime)row["fechaSolicitud"];
                    objeto.StatusSolicitud = row["statusSolicitud"].ToString();
                    lista.Add(objeto);
                }
                objectConexion.cerrarConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("NO SE PUDIERON SELECCIONAR LAS SUSTANCIAS. Consulte a su administrador");
            }
            return lista;
        }

        public List<CSolicitud> solicitudId(int indice)
        {
            List<CSolicitud> lista = new List<CSolicitud>();
            DataTable dt = new DataTable();
            string SQL = string.Empty;
            SQL += "SELECT *";
            SQL += " FROM Solicitud";
            SQL += " WHERE idSolicitud =" + indice + " ORDER BY idSolicitud ASC";
            SqlCommand command = new SqlCommand(SQL, objectConexion.establecerConexion());
            SqlDataReader reader = command.ExecuteReader();
            dt.Load(reader);
            reader.Close();
            objectConexion.cerrarConexion();
            foreach (DataRow row in dt.Rows)
            {
                CSolicitud objeto = new CSolicitud();
                objeto.IdSolicitud = Convert.ToInt32(row["idSolicitud"].ToString());
                objeto.NombreSolicitud = row["nombreSolicitud"].ToString();
                objeto.FabricanteSolicitud = row["fabricanteSolicitud"].ToString();
                objeto.FechaSolicitud = (DateTime)row["fechaSolicitud"];
                objeto.ExtensionHdsSolicitud = row["extensionHdsSolicitud"].ToString();
                objeto.HdsSolicitud = (byte[])row["hdsSolicitud"];
                objeto.ExtensionFichaSolicitud = row["extensionFichaSolicitud"].ToString();
                objeto.FichaSolicitud = (byte[])row["fichaSolicitud"];
                objeto.StatusSolicitud = row["statusSolicitud"].ToString();
                lista.Add(objeto);
            }
            return lista;
        }

        public void editarStatus(int indice)
        {
            try
            {
                SqlCommand comando = new SqlCommand("UPDATE Solicitud SET statusSolicitud = @statusSolicitud " +
                    "WHERE idSolicitud = '" + indice.ToString() + "'",
                    objectConexion.establecerConexion());

                comando.CommandType = CommandType.Text;
                comando.Parameters.AddWithValue("@statusSolicitud", StatusSolicitud);
                
                comando.ExecuteNonQuery();

                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró modificar la SUSTANCIA. Consulte a su administrador.");
            }
        }

        public void editarSolicitud(int indice)
        {
            try
            {
                SqlCommand comando = new SqlCommand("UPDATE Solicitud SET nombreSolicitud = @nombreSolicitud, fabricanteSolicitud = @fabricanteSolicitud, " +
                    "fechaSolicitud = @fechaSolicitud, extensionHdsSolicitud = @extensionHdsSolicitud, hdsSolicitud = @hdsSolicitud, " +
                    "extensionFichaSolicitud = @extensionFichaSolicitud, fichaSolicitud = @fichaSolicitud, statusSolicitud = @statusSolicitud " +
                    "WHERE idSolicitud = '" + indice.ToString() + "'",
                    objectConexion.establecerConexion());

                comando.CommandType = CommandType.Text;
                comando.Parameters.AddWithValue("@nombreSolicitud", NombreSolicitud);
                comando.Parameters.AddWithValue("@fabricanteSolicitud", FabricanteSolicitud);
                comando.Parameters.AddWithValue("@fechaSolicitud", FechaSolicitud);
                comando.Parameters.AddWithValue("@extensionHdsSolicitud", ExtensionHdsSolicitud);
                comando.Parameters.AddWithValue("@hdsSolicitud", HdsSolicitud);
                comando.Parameters.AddWithValue("@extensionFichaSolicitud", ExtensionFichaSolicitud);
                comando.Parameters.AddWithValue("@fichaSolicitud", FichaSolicitud);
                comando.Parameters.AddWithValue("@statusSolicitud", StatusSolicitud);

                comando.ExecuteNonQuery();

                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró modificarRR la SOLICITUD. Consulte a su administrador.");
            }
        }

        public int selectSolicitudes()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM Solicitud", 
                    objectConexion.establecerConexion());

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                int cantidad = dt.Rows.Count;
                objectConexion.cerrarConexion();
                if (dt.Rows.Count == 0)
                {
                    return 0;
                }
                return cantidad;
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrió un error de conexión. Consulte a su administrador.");
            }
            finally
            {
                objectConexion.cerrarConexion();
            }
            return 0;
        }

        public void eliminarSolicitudes()
        {
            try
            {
                String query = "TRUNCATE TABLE Solicitud";

                SqlCommand sqlCommand = new SqlCommand(query, objectConexion.establecerConexion());
                SqlDataReader reader = sqlCommand.ExecuteReader();

                MessageBox.Show("Se eliminaron las solicitudes correctamente");

                while (reader.Read())
                {

                }
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró eliminar. Consulte a su administrador");
            }
        }
    }
}
