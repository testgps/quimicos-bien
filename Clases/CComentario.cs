﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaScreen.Clases
{
    public class CComentario
    {
        private int idUsuario;
        private int idSolicitud;
        private string usuario;
        private string comentario;

        public int IdUsuario { get => idUsuario; set => idUsuario = value; }
        public int IdSolicitud { get => idSolicitud; set => idSolicitud = value; }
        public string Usuario { get => usuario; set => usuario = value; }
        public string Comentario { get => comentario; set => comentario = value; }

        CConexion objectConexion = new CConexion();

        public List<CComentario> GetComentarioById(int indice)
        {
            List<CComentario> lista = new List<CComentario>();
            DataTable dt = new DataTable();
            string SQL = string.Empty;
            SQL += "SELECT Usuario.idUsuario, Usuario.usuario, comentario ";
            SQL += " FROM Usuario, Comentario, Solicitud";
            SQL += " WHERE Usuario.idUsuario = Comentario.idUsuario";
            SQL += " AND Solicitud.idSolicitud = Comentario.idSolicitud AND Comentario.idSolicitud = " + indice;
            SqlCommand command = new SqlCommand(SQL, objectConexion.establecerConexion());
            SqlDataReader reader = command.ExecuteReader();
            dt.Load(reader);
            reader.Close();
            objectConexion.cerrarConexion();
            foreach (DataRow row in dt.Rows)
            {
                CComentario objeto = new CComentario();
                objeto.IdUsuario = Convert.ToInt32(row["idUsuario"].ToString());
                objeto.Usuario = row["usuario"].ToString();
                objeto.Comentario = row["comentario"].ToString();
                lista.Add(objeto);
            }
            return lista;
        }

        public void agregarComentario ()
        {
            try
            {
                SqlCommand comando = new SqlCommand("INSERT INTO Comentario " +
                    "VALUES(@idSolicitud, @idUsuario, @comentario)",
                    objectConexion.establecerConexion());

                comando.CommandType = CommandType.Text;
                comando.Parameters.AddWithValue("@idSolicitud", IdSolicitud);
                comando.Parameters.AddWithValue("@idUsuario", IdUsuario);
                comando.Parameters.AddWithValue("@comentario", Comentario);
                comando.ExecuteNonQuery();

                MessageBox.Show("Se insertó el comentario correctamente");
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró insertar. Consulte a su administrador");
            }
        }
    }
}
