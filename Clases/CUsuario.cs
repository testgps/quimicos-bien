﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using PruebaScreen.Clases;

namespace PruebaScreen.Clases
{
    public class CUsuario
    {
        private int idUsuario;
        private string user;
        private string passwordUsuario;
        private int rolUsuario;
        private int sesionUsuario;
        private int id;

        public int IdUsuario { get => idUsuario; set => idUsuario = value; }
        public string User { get => user; set => user = value; }
        public string PasswordUsuario { get => passwordUsuario; set => passwordUsuario = value; }
        public int RolUsuario { get => rolUsuario; set => rolUsuario = value; }
        public int SesionUsuario { get => sesionUsuario; set => sesionUsuario = value; }


        CConexion objectConexion = new CConexion();

        public int loginUsuario ()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT usuario, rolUsuario " +
                                                "FROM Usuario " +
                                                "WHERE usuario = @usuario COLLATE SQL_Latin1_General_CP1_CS_AS " +
                                                "AND CONVERT(VARCHAR(MAX), DECRYPTBYPASSPHRASE('password', password)) = @password", 
                                                objectConexion.establecerConexion());
                cmd.Parameters.AddWithValue("usuario", User);
                cmd.Parameters.AddWithValue("password", PasswordUsuario);

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                objectConexion.cerrarConexion();

                if (dt.Rows.Count == 1)
                {
                    //MessageBox.Show("HAY UN RESULTADO");
                    if (dt.Rows[0][1].ToString() == "1")
                    {
                        //MessageBox.Show("eres admin");
                        return 1;
                    }
                    if (dt.Rows[0][1].ToString() == "2")
                    {
                        return 2;
                    }
                    if (dt.Rows[0][1].ToString() == "3")
                    {
                        return 3;
                    }
                    if (dt.Rows[0][1].ToString() == "4")
                    {
                        return 4;
                    }
                }
                else if (dt.Rows.Count == 0)
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrió un error de conexión");
            }
            finally
            {
                objectConexion.cerrarConexion();
            }
            return 0;
        }

        public int seleccionarUsuario ()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT idUsuario " +
                                                "FROM Usuario " +
                                                "WHERE usuario = @usuario " +
                                                "AND CONVERT(VARCHAR(MAX), DECRYPTBYPASSPHRASE('password', password)) = @password",
                                                objectConexion.establecerConexion());
                cmd.Parameters.AddWithValue("usuario", User);
                cmd.Parameters.AddWithValue("password", PasswordUsuario);

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                if (dt.Rows.Count == 1)
                {
                    int id = Convert.ToInt32(cmd.ExecuteScalar());
                    IdUsuario = id;
                    //MessageBox.Show("EL ID ES:"+ id.ToString());
                    return id;
                }
                else if (dt.Rows.Count == 0)
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrió un error de conexión");
            }
            finally
            {
                objectConexion.cerrarConexion();
            }
            return 0;
        }

        public int SeleccionarRol()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT rolUsuario " +
                                                "FROM Usuario " +
                                                "WHERE usuario = @usuario " +
                                                "AND CONVERT(VARCHAR(MAX), DECRYPTBYPASSPHRASE('password', password)) = @password",
                                                objectConexion.establecerConexion());
                cmd.Parameters.AddWithValue("usuario", User);
                cmd.Parameters.AddWithValue("password", PasswordUsuario);

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                if (dt.Rows.Count == 1)
                {
                    int rol = Convert.ToInt32(cmd.ExecuteScalar());
                    RolUsuario = rol;
                    //MessageBox.Show("EL ROL ES:"+ rol.ToString());
                    return id;
                }
                else if (dt.Rows.Count == 0)
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrió un error de conexión.");
            }
            finally
            {
                objectConexion.cerrarConexion();
            }
            return 0;
        }

        public void activarSesion()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UPDATE Usuario SET sesionUsuario = 1 WHERE idUsuario = @id",
                                                objectConexion.establecerConexion());
                cmd.Parameters.AddWithValue("id", IdUsuario);

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrió un error de conexión.");
            }
            finally
            {
                objectConexion.cerrarConexion();
            }
        }

        public void cerrarSesion()
        {
            objectConexion.cerrarConexion();
        }
    }
}