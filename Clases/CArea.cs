﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaScreen.Clases
{
    public class CArea
    {
        private int idArea;
        private string nombreArea;
        CConexion objectConexion = new CConexion();

        public int IdArea { get => idArea; set => idArea = value; }
        public string NombreArea { get => nombreArea; set => nombreArea = value; }

        public List<CArea> GetAreaById(int indice)
        {
            List<CArea> lista = new List<CArea>();
            DataTable dt = new DataTable();
            string SQL = string.Empty;
            SQL += "SELECT SustanciaArea.idArea, Area.nombreArea";
            SQL += " FROM Sustancia, SustanciaArea, Area";
            SQL += " WHERE SustanciaArea.idSustancia = Sustancia.idSustancia";
            SQL += " AND SustanciaArea.idArea = Area.idArea AND SustanciaArea.idSustancia = " + indice;
            SqlCommand command = new SqlCommand(SQL, objectConexion.establecerConexion());
            SqlDataReader reader = command.ExecuteReader();
            dt.Load(reader);
            reader.Close();
            objectConexion.cerrarConexion();
            foreach (DataRow row in dt.Rows)
            {
                CArea objeto = new CArea();
                objeto.IdArea = Convert.ToInt32(row["idArea"].ToString());
                objeto.NombreArea = row["nombreArea"].ToString();
                lista.Add(objeto);
            }
            return lista;
        }

        public void agregarArea()
        {
            int id = ultimaAreaInsertada() + 1;
            try
            {
                SqlCommand comando = new SqlCommand("INSERT INTO Area VALUES("+ id + ",@nombreArea)",
                    objectConexion.establecerConexion());

                comando.CommandType = CommandType.Text;
                comando.Parameters.AddWithValue("@nombreArea", NombreArea);
                comando.ExecuteNonQuery();

                MessageBox.Show("Se insertó correctamente el área");
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró insertar. Consulte a su administrador");
            }
        }

        public int ultimaAreaInsertada()
        {
            int lastInsertedId = 0;
            try
            {
                string query = "SELECT TOP 1 idArea FROM Area " +
                    "ORDER BY idArea DESC";
                SqlCommand command = new SqlCommand(query, objectConexion.establecerConexion());
                lastInsertedId = (int)command.ExecuteScalar();
                objectConexion.cerrarConexion();
                return lastInsertedId;
            }
            catch (Exception e)
            {
                MessageBox.Show("Error obteniendo los datos");
                return lastInsertedId;
            }
        }

        public void mostrarArea(DataGridView tablaArea)
        {
            try
            {
                tablaArea.DataSource = null;
                SqlDataAdapter adapter = new SqlDataAdapter("SELECT idArea, nombreArea " +
                    "FROM Area ORDER BY idArea ASC",
                    objectConexion.establecerConexion());
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                tablaArea.DataSource = dt;
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se lograron mostrar las áreas.");
            }
        }

        public void seleccionarArea(DataGridView tablaArea, TextBox paramId, TextBox paramNombre)
        {
            try
            {
                paramId.Text = tablaArea.CurrentRow.Cells[0].Value.ToString();
                paramNombre.Text = tablaArea.CurrentRow.Cells[1].Value.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se logró seleccionar. Consulte a su administrador");
            }
        }

        public int seleccionarAreaExistente(string paramNombre)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM Area" +
                    " WHERE nombreArea = '" + paramNombre + "'", objectConexion.establecerConexion());

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                if (dt.Rows.Count == 1)
                {
                    return 1;
                }
                else if (dt.Rows.Count == 0)
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrió un error de conexión");
            }
            finally
            {
                objectConexion.cerrarConexion();
            }
            return 0;
        }

        public void editarArea(TextBox paramId)
        {
            try
            {
                SqlCommand comando = new SqlCommand("UPDATE Area SET nombreArea = @nombreArea" +
                    " WHERE Area.idArea = '" + paramId.Text + "';",
                    objectConexion.establecerConexion());

                comando.Parameters.AddWithValue("@nombreArea", NombreArea);
                comando.ExecuteNonQuery();
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró modificar. Consulte a su administrador");
            }
        }

        public void eliminarArea(TextBox paramId)
        {
            try
            {
                String query = "DELETE FROM Area WHERE Area.idArea = '" + paramId.Text + "';";

                SqlCommand sqlCommand = new SqlCommand(query, objectConexion.establecerConexion());
                SqlDataReader reader = sqlCommand.ExecuteReader();

                MessageBox.Show("Se eliminó correctamente");

                while (reader.Read())
                {

                }
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró eliminar. Consulte a su administrador");
            }
        }

        public void eliminarSustanciaArea(int id)
        {
            try
            {
                int idSustancia = id;

                // Eliminar los registros existentes en la tabla SustanciaGaveta para el idSustancia específico
                string deleteQuery = "DELETE FROM SustanciaArea WHERE idSustancia = @idSustancia";
                SqlCommand deleteCommand = new SqlCommand(deleteQuery,
                    objectConexion.establecerConexion());
                deleteCommand.Parameters.AddWithValue("@idSustancia", idSustancia);
                deleteCommand.ExecuteNonQuery();
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error eliminando las áreas. Consulte a su administrador.");
            }
        }
    }
}
