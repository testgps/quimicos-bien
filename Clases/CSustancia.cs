﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PruebaScreen.Clases;

namespace PruebaScreen.Clases
{
    public class CSustancia
    {
        private int idSustancia;
        private string nombreSustancia;
        private string usoRecomendado;
        private string palabraAdvertencia;
        private string telefonoEmergencia;
        private string area;
        private string cantidadEstimada;
        private string tipoContenedor;
        private string consumoPromedio;
        private string hdsExtension;
        private byte[] hdsSustancia;
        private string imagenExtension;
        private byte[] imagenSustancia;
        private string riesgosSustancia;
        private string numeroCas;
        private string numeroOnu;
        private string nombreFabricante;
        private string telefonoFabricante;
        private string direccionFabricante;
        private string etiquetaExtension;
        private byte[] etiquetaSustancia;

        public int IdSustancia { get => idSustancia; set => idSustancia = value; }
        public string NombreSustancia { get => nombreSustancia; set => nombreSustancia = value; }
        public string UsoRecomendado { get => usoRecomendado; set => usoRecomendado = value; }
        public string PalabraAdvertencia { get => palabraAdvertencia; set => palabraAdvertencia = value; }
        public string TelefonoEmergencia { get => telefonoEmergencia; set => telefonoEmergencia = value; }
        public string Area { get => area; set => area = value; }
        public string CantidadEstimada { get => cantidadEstimada; set => cantidadEstimada = value; }
        public string TipoContenedor { get => tipoContenedor; set => tipoContenedor = value; }
        public string HdsExtension { get => hdsExtension; set => hdsExtension = value; }
        public byte[] HdsSustancia { get => hdsSustancia; set => hdsSustancia = value; }
        public string ImagenExtension { get => imagenExtension; set => imagenExtension = value; }
        public byte[] ImagenSustancia { get => imagenSustancia; set => imagenSustancia = value; }
        public string RiesgosSustancia { get => riesgosSustancia; set => riesgosSustancia = value; }
        public string ConsumoPromedio { get => consumoPromedio; set => consumoPromedio = value; }
        public string NumeroCas { get => numeroCas; set => numeroCas = value; }
        public string NumeroOnu { get => numeroOnu; set => numeroOnu = value; }
        public string NombreFabricante { get => nombreFabricante; set => nombreFabricante = value; }
        public string TelefonoFabricante { get => telefonoFabricante; set => telefonoFabricante = value; }
        public string DireccionFabricante { get => direccionFabricante; set => direccionFabricante = value; }
        public string EtiquetaExtension { get => etiquetaExtension; set => etiquetaExtension = value; }
        public byte[] EtiquetaSustancia { get => etiquetaSustancia; set => etiquetaSustancia = value; }

        CConexion objectConexion = new CConexion();

        public void agregarSustancia()
        {
            int id = UltimoIdInsertado() + 1;
            try
            {
                SqlCommand comando = new SqlCommand("INSERT INTO Sustancia (idSustancia ,nombreSustancia, usoRecomendado, palabraAdvertencia, " +
                    "telefonoEmergencia, cantidadEstimada, tipoContenedor, consumoPromedio, hdsExtension, hdsSustancia, " +
                    "imagenExtension, imagenSustancia, riesgosSustancia, numeroCas, numeroOnu, nombreFabricante, telefonoFabricante, " +
                    "direccionFabricante, etiquetaExtension, etiquetaSustancia) " +
                    "VALUES("+ id +", @nombreSustancia, @usoRecomendado, @palabraAdvertencia, " +
                    "@telefonoEmergencia, @cantidadEstimada, @tipoContenedor, @consumoPromedio, @hdsExtension, @hdsSustancia, " +
                    "@imagenExtension, @imagenSustancia, @riesgosSustancia, @numeroCas, @numeroOnu, @nombreFabricante, @telefonoFabricante, @direccionFabricante, " +
                    "@etiquetaExtension, @etiquetaSustancia)",
                    objectConexion.establecerConexion());

                comando.CommandType = CommandType.Text;
                comando.Parameters.AddWithValue("@nombreSustancia", NombreSustancia);
                comando.Parameters.AddWithValue("@usoRecomendado", UsoRecomendado);
                comando.Parameters.AddWithValue("@palabraAdvertencia", PalabraAdvertencia);
                comando.Parameters.AddWithValue("@telefonoEmergencia", TelefonoEmergencia);
                comando.Parameters.AddWithValue("@cantidadEstimada", CantidadEstimada);
                comando.Parameters.AddWithValue("@tipoContenedor", TipoContenedor);
                comando.Parameters.AddWithValue("@consumoPromedio", ConsumoPromedio);
                comando.Parameters.AddWithValue("@hdsExtension", HdsExtension);
                comando.Parameters.AddWithValue("@hdsSustancia", HdsSustancia);
                comando.Parameters.AddWithValue("@imagenExtension", ImagenExtension);
                comando.Parameters.AddWithValue("@imagenSustancia", ImagenSustancia);
                comando.Parameters.AddWithValue("@riesgosSustancia", RiesgosSustancia);
                comando.Parameters.AddWithValue("@numeroCas", NumeroCas);
                comando.Parameters.AddWithValue("@numeroOnu", NumeroOnu);
                comando.Parameters.AddWithValue("@nombreFabricante", NombreFabricante);
                comando.Parameters.AddWithValue("@telefonoFabricante", TelefonoFabricante);
                comando.Parameters.AddWithValue("@direccionFabricante", DireccionFabricante);
                comando.Parameters.AddWithValue("@etiquetaExtension", EtiquetaExtension);
                comando.Parameters.AddWithValue("@etiquetaSustancia", EtiquetaSustancia);
                comando.ExecuteNonQuery();

                MessageBox.Show("Se ejecutó correctamente");
                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró insertar. Consulte a su administrador.");
            }
        }

        public List<CSustancia> selectSustancias()
        {
            List<CSustancia> lista = new List<CSustancia>();
            try
            {
                string SQL = string.Empty;
                SQL += "SELECT idSustancia, nombreSustancia, usoRecomendado, palabraAdvertencia, imagenSustancia";
                SQL += " FROM";
                SQL += " Sustancia ORDER BY nombreSustancia ASC";

                SqlCommand command = new SqlCommand(SQL, objectConexion.establecerConexion());

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                DataSet dataset = new DataSet();

                sqlDataAdapter.Fill(dataset, "Sustancia");
                objectConexion.cerrarConexion();

                foreach (DataRow row in dataset.Tables[0].Rows)
                {
                    CSustancia objeto = new CSustancia();
                    objeto.IdSustancia = Convert.ToInt32(row["idSustancia"].ToString());
                    objeto.NombreSustancia = row["nombreSustancia"].ToString();
                    objeto.UsoRecomendado = row["usoRecomendado"].ToString();
                    objeto.PalabraAdvertencia = row["palabraAdvertencia"].ToString();
                    objeto.ImagenSustancia = (byte[])row["imagenSustancia"];
                    lista.Add(objeto);
                }
                objectConexion.cerrarConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("NO SE PUDIERON SELECCIONAR LAS SUSTANCIAS. Consulte a su administrador");
            }
            return lista;
        }

        public void editarSustancia(int indice)
        {
            try
            {
                SqlCommand comando = new SqlCommand("UPDATE Sustancia SET nombreSustancia = @nombreSustancia," +
                    "usoRecomendado = @usoRecomendado, palabraAdvertencia = @palabraAdvertencia, telefonoEmergencia = @telefonoEmergencia," +
                    "cantidadEstimada = @cantidadEstimada, tipoContenedor = @tipoContenedor, consumoPromedio = @consumoPromedio, hdsExtension = @hdsExtension," +
                    "hdsSustancia = @hdsSustancia, imagenExtension = @imagenExtension, imagenSustancia = @imagenSustancia, riesgosSustancia = @riesgosSustancia, " +
                    "numeroCas = @numeroCas, numeroOnu = @numeroOnu, nombreFabricante = @nombreFabricante, telefonoFabricante = @telefonoFabricante, " +
                    "direccionFabricante = @direccionFabricante, etiquetaExtension = @etiquetaExtension, etiquetaSustancia = @etiquetaSustancia " +
                    "WHERE idSustancia = '" + indice.ToString() + "'",
                    objectConexion.establecerConexion());

                comando.CommandType = CommandType.Text;
                comando.Parameters.AddWithValue("@nombreSustancia", NombreSustancia);
                comando.Parameters.AddWithValue("@usoRecomendado", UsoRecomendado);
                comando.Parameters.AddWithValue("@palabraAdvertencia", PalabraAdvertencia);
                comando.Parameters.AddWithValue("@telefonoEmergencia", TelefonoEmergencia);
                comando.Parameters.AddWithValue("@cantidadEstimada", CantidadEstimada);
                comando.Parameters.AddWithValue("@tipoContenedor", TipoContenedor);
                comando.Parameters.AddWithValue("@consumoPromedio", ConsumoPromedio);
                comando.Parameters.AddWithValue("@hdsExtension", HdsExtension);
                comando.Parameters.AddWithValue("@hdsSustancia", HdsSustancia);
                comando.Parameters.AddWithValue("@imagenExtension", ImagenExtension);
                comando.Parameters.AddWithValue("@imagenSustancia", ImagenSustancia);
                comando.Parameters.AddWithValue("@riesgosSustancia", RiesgosSustancia);
                comando.Parameters.AddWithValue("@numeroCas", NumeroCas);
                comando.Parameters.AddWithValue("@numeroOnu", NumeroOnu);
                comando.Parameters.AddWithValue("@nombreFabricante", NombreFabricante);
                comando.Parameters.AddWithValue("@telefonoFabricante", TelefonoFabricante);
                comando.Parameters.AddWithValue("@direccionFabricante", DireccionFabricante);
                comando.Parameters.AddWithValue("@etiquetaExtension", EtiquetaExtension);
                comando.Parameters.AddWithValue("@etiquetaSustancia", EtiquetaSustancia);

                comando.ExecuteNonQuery();

                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró modificar la SUSTANCIA. Consulte a su administrador.");
            }
        }

        public void eliminarSustancia(int indice)
        {
            try
            {
                String query = "DELETE FROM Sustancia WHERE Sustancia.idSustancia = '" + indice.ToString() + "';";

                SqlCommand sqlCommand = new SqlCommand(query, objectConexion.establecerConexion());
                SqlDataReader reader = sqlCommand.ExecuteReader();

                //MessageBox.Show("Se eliminó correctamenteeeeEEe");

                while (reader.Read())
                {

                }

                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró eliminar la SUSTANCIA. Consulte a su administrador.");
            }
        }

        public List<CSustancia> sustanciaId(int indice) 
        {
            List<CSustancia> lista = new List<CSustancia>();
            DataTable dt = new DataTable();
            string SQL = string.Empty;
            SQL += "SELECT *";
            SQL += " FROM Sustancia";
            SQL += " WHERE idSustancia =" + indice + " ORDER BY idSustancia ASC";
            SqlCommand command = new SqlCommand(SQL, objectConexion.establecerConexion());
            SqlDataReader reader = command.ExecuteReader();
            dt.Load(reader);
            reader.Close();
            objectConexion.cerrarConexion();
            foreach (DataRow row in dt.Rows)
            {
                CSustancia objeto = new CSustancia();
                objeto.IdSustancia = Convert.ToInt32(row["idSustancia"].ToString());
                objeto.NombreSustancia = row["nombreSustancia"].ToString();
                objeto.UsoRecomendado = row["usoRecomendado"].ToString();
                objeto.PalabraAdvertencia = row["palabraAdvertencia"].ToString();
                objeto.TelefonoEmergencia = row["telefonoEmergencia"].ToString();
                objeto.CantidadEstimada = row["cantidadEstimada"].ToString();
                objeto.TipoContenedor = row["tipoContenedor"].ToString();
                objeto.ConsumoPromedio = row["consumoPromedio"].ToString();
                objeto.HdsExtension = row["hdsExtension"].ToString();
                objeto.HdsSustancia = (byte[])row["hdsSustancia"];
                objeto.ImagenExtension = row["imagenExtension"].ToString();
                objeto.ImagenSustancia = (byte[])row["imagenSustancia"];
                objeto.RiesgosSustancia = row["riesgosSustancia"].ToString();
                objeto.NumeroCas = row["numeroCas"].ToString();
                objeto.NumeroOnu = row["numeroOnu"].ToString();
                objeto.NombreFabricante = row["nombreFabricante"].ToString();
                objeto.TelefonoFabricante = row["telefonoFabricante"].ToString();
                objeto.DireccionFabricante = row["direccionFabricante"].ToString();
                objeto.EtiquetaExtension = row["etiquetaExtension"].ToString();
                objeto.EtiquetaSustancia = (byte[])row["etiquetaSustancia"];

                lista.Add(objeto);
            }
            return lista;
        }

        public void mostrarGaveta(CheckedListBox checkedListBox)
        {
            try
            {
                checkedListBox.DataSource = null;
                SqlDataAdapter adapter = new SqlDataAdapter("SELECT idGaveta,nombreGaveta FROM Gaveta",
                    objectConexion.establecerConexion());

                DataTable dt = new DataTable();
                adapter.Fill(dt);

                checkedListBox.DataSource = dt;
                checkedListBox.DisplayMember = "nombreGaveta";
                checkedListBox.ValueMember = "idGaveta";

                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró mostrar. Consulte a su administrador.");
            }
        }

        public void SeleccionarItems(CheckedListBox checkedListBox, int indice)
        {
            try
            {
                // Obtener los datos de la base de datos
                SqlDataAdapter adapter = new SqlDataAdapter("SELECT idGaveta FROM SustanciaGaveta WHERE idSustancia = "+ indice,
                    objectConexion.establecerConexion());
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                // Recorrer los elementos del CheckedListBox y seleccionar los que coinciden con los datos de la base de datos
                foreach (DataRow row in dt.Rows)
                {
                    int idGaveta = Convert.ToInt32(row["idGaveta"]);

                    for (int i = 0; i < checkedListBox.Items.Count; i++)
                    {
                        DataRowView item = checkedListBox.Items[i] as DataRowView;
                        int idGavetaItem = Convert.ToInt32(item["idGaveta"]);

                        if (idGaveta == idGavetaItem)
                        {
                            checkedListBox.SetItemChecked(i, true);
                        }
                    }
                    objectConexion.cerrarConexion();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error seleccionando items de gavetas. Consulte a su administrador");
            }
        }

        public void mostrarArea(CheckedListBox checkedListBox)
        {
            try
            {
                checkedListBox.DataSource = null;
                SqlDataAdapter adapter = new SqlDataAdapter("SELECT idArea, nombreArea FROM Area " +
                    "ORDER BY nombreArea ASC",
                    objectConexion.establecerConexion());

                DataTable dt = new DataTable();
                adapter.Fill(dt);

                checkedListBox.DataSource = dt;
                checkedListBox.DisplayMember = "nombreArea";
                checkedListBox.ValueMember = "idArea";

                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró mostrar. Consulte a su administrador.");
            }
        }

        public void mostrarAreaCombo(ComboBox comboArea)
        {
            try
            {
                comboArea.DataSource = null;
                SqlDataAdapter adapter = new SqlDataAdapter("SELECT idArea, nombreArea FROM Area " +
                    "ORDER BY nombreArea ASC",
                    objectConexion.establecerConexion());

                DataTable dt = new DataTable();
                adapter.Fill(dt);

                comboArea.DataSource = dt;
                comboArea.DisplayMember = "nombreArea";
                comboArea.ValueMember = "idArea";

                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró mostrar. Consulte a su administrador.");
            }
        }

        public void SeleccionarArea(CheckedListBox checkedListBox, int indice)
        {
            try
            {
                // Obtener los datos de la base de datos
                SqlDataAdapter adapter = new SqlDataAdapter("SELECT idArea FROM SustanciaArea WHERE idSustancia = " + indice,
                    objectConexion.establecerConexion());
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                // Recorrer los elementos del CheckedListBox y seleccionar los que coinciden con los datos de la base de datos
                foreach (DataRow row in dt.Rows)
                {
                    int idArea = Convert.ToInt32(row["idArea"]);

                    for (int i = 0; i < checkedListBox.Items.Count; i++)
                    {
                        DataRowView item = checkedListBox.Items[i] as DataRowView;
                        int idGavetaItem = Convert.ToInt32(item["idArea"]);

                        if (idArea == idGavetaItem)
                        {
                            checkedListBox.SetItemChecked(i, true);
                        }
                    }
                    objectConexion.cerrarConexion();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error seleccionando items de áreas. Consulte a su administrador");
            }
        }

        public void actualizarArea(CheckedListBox checkedListBox, int id)
        {
            try
            {
                int idSustancia = id;

                // Eliminar los registros existentes en la tabla SustanciaGaveta para el idSustancia específico
                string deleteQuery = "DELETE FROM SustanciaArea WHERE idSustancia = @idSustancia";
                SqlCommand deleteCommand = new SqlCommand(deleteQuery,
                    objectConexion.establecerConexion());
                deleteCommand.Parameters.AddWithValue("@idSustancia", idSustancia);
                deleteCommand.ExecuteNonQuery();
                objectConexion.cerrarConexion();

                // Recorrer los elementos seleccionados del CheckedListBox y realizar la inserción en la tabla SustanciaGaveta
                foreach (object item in checkedListBox.CheckedItems)
                {
                    DataRowView row = item as DataRowView;
                    int idArea = Convert.ToInt32(row["idArea"]);

                    // Insertar el registro en la tabla SustanciaGaveta
                    string insertQuery = "INSERT INTO SustanciaArea (idSustancia, idArea) VALUES (@idSustancia, @idArea)";
                    SqlCommand insertCommand = new SqlCommand(insertQuery,
                        objectConexion.establecerConexion());
                    insertCommand.Parameters.AddWithValue("@idSustancia", idSustancia);
                    insertCommand.Parameters.AddWithValue("@idArea", idArea);
                    insertCommand.ExecuteNonQuery();
                    objectConexion.cerrarConexion();
                }
                //MessageBox.Show("ACTUALIZACION DE GAVETAS CORRECTO");
            }
            catch (Exception e)
            {
                MessageBox.Show("Error seleccionando items de gavetas. Consulte a su administrador.");
            }
        }

        public void actualizarGaveta(CheckedListBox checkedListBox, int id)
        {
            try
            {
                int idSustancia = id;
                // Eliminar los registros existentes en la tabla SustanciaGaveta para el idSustancia específico
                string deleteQuery = "DELETE FROM SustanciaGaveta WHERE idSustancia = @idSustancia";
                SqlCommand deleteCommand = new SqlCommand(deleteQuery,
                    objectConexion.establecerConexion());
                deleteCommand.Parameters.AddWithValue("@idSustancia", idSustancia);
                deleteCommand.ExecuteNonQuery();
                objectConexion.cerrarConexion();

                // Recorrer los elementos seleccionados del CheckedListBox y realizar la inserción en la tabla SustanciaGaveta
                foreach (object item in checkedListBox.CheckedItems)
                {
                    DataRowView row = item as DataRowView;
                    int idGaveta = Convert.ToInt32(row["idGaveta"]);

                    // Insertar el registro en la tabla SustanciaGaveta
                    string insertQuery = "INSERT INTO SustanciaGaveta (idSustancia, idGaveta) VALUES (@idSustancia, @idGaveta)";
                    SqlCommand insertCommand = new SqlCommand(insertQuery,
                        objectConexion.establecerConexion());
                    insertCommand.Parameters.AddWithValue("@idSustancia", idSustancia);
                    insertCommand.Parameters.AddWithValue("@idGaveta", idGaveta);
                    insertCommand.ExecuteNonQuery();
                    objectConexion.cerrarConexion();
                }
                //MessageBox.Show("ACTUALIZACION DE GAVETAS CORRECTO");
            }
            catch (Exception e)
            {
                MessageBox.Show("Error seleccionando items de gavetas. Consulte a su administrador.");
            }
        }

        public int UltimoIdInsertado()
        {
            int lastInsertedId = 0;
            try
            {
                string query = "SELECT TOP 1 idSustancia FROM Sustancia " +
                    "ORDER BY idSustancia DESC";
                SqlCommand command = new SqlCommand(query, objectConexion.establecerConexion());
                lastInsertedId = (int)command.ExecuteScalar();
                objectConexion.cerrarConexion();
                return lastInsertedId;
            }
            catch (Exception e)
            {
                MessageBox.Show("Error en la obtención del ultimo insertado.");
                return lastInsertedId;
            }
        }

        public List<CSustancia> BuscarSustancias(string txtBuscar)
        {
            List<CSustancia> lista = new List<CSustancia>();
            try
            {
                string SQL = string.Empty;
                SQL += "SELECT idSustancia, nombreSustancia, usoRecomendado, palabraAdvertencia, imagenSustancia";
                SQL += " FROM Sustancia";
                SQL += " WHERE nombreSustancia LIKE '%"+ txtBuscar.ToString() + "%'";
                SQL += " ORDER BY Sustancia.nombreSustancia ASC";

                SqlCommand command = new SqlCommand(SQL, objectConexion.establecerConexion());

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                DataSet dataset = new DataSet();

                sqlDataAdapter.Fill(dataset, "Sustancia");
                objectConexion.cerrarConexion();

                foreach (DataRow row in dataset.Tables[0].Rows)
                {
                    CSustancia objeto = new CSustancia();
                    objeto.IdSustancia = Convert.ToInt32(row["idSustancia"].ToString());
                    objeto.NombreSustancia = row["nombreSustancia"].ToString();
                    objeto.UsoRecomendado = row["usoRecomendado"].ToString();
                    objeto.PalabraAdvertencia = row["palabraAdvertencia"].ToString();
                    objeto.ImagenSustancia = (byte[])row["imagenSustancia"];
                    lista.Add(objeto);
                }
                objectConexion.cerrarConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("NO SE PUDIERON SELECCIONAR LAS SUSTANCIAS. Consulte a su administrador");
            }
            return lista;
        }

        public List<CSustancia> selectSustanciaAreasGavetas(string nombreArea, string nombreGaveta)
        {
            List<CSustancia> lista = new List<CSustancia>();
            try
            {
                string SQL = string.Empty;
                SQL += "SELECT Sustancia.idSustancia, nombreSustancia, usoRecomendado, palabraAdvertencia, imagenSustancia";
                SQL += " FROM Sustancia, SustanciaArea, Area, Gaveta, SustanciaGaveta";
                SQL += " WHERE Sustancia.idSustancia = SustanciaArea.idSustancia";
                SQL += " AND Area.idArea = SustanciaArea.idArea";
                SQL += " AND Gaveta.idGaveta = SustanciaGaveta.idGaveta AND Sustancia.idSustancia = SustanciaGaveta.idSustancia";
                SQL += " AND Area.nombreArea = '" + nombreArea.ToString() + "' AND Gaveta.nombreGaveta = '" + nombreGaveta.ToString() + "'";
                SQL += " ORDER BY Sustancia.nombreSustancia ASC";

                SqlCommand command = new SqlCommand(SQL, objectConexion.establecerConexion());

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                DataSet dataset = new DataSet();

                sqlDataAdapter.Fill(dataset, "Sustancia");
                objectConexion.cerrarConexion();

                foreach (DataRow row in dataset.Tables[0].Rows)
                {
                    CSustancia objeto = new CSustancia();
                    objeto.IdSustancia = Convert.ToInt32(row["idSustancia"].ToString());
                    objeto.NombreSustancia = row["nombreSustancia"].ToString();
                    objeto.UsoRecomendado = row["usoRecomendado"].ToString();
                    objeto.PalabraAdvertencia = row["palabraAdvertencia"].ToString();
                    objeto.ImagenSustancia = (byte[])row["imagenSustancia"];
                    lista.Add(objeto);
                }
                objectConexion.cerrarConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("NO SE PUDIERON SELECCIONAR LAS SUSTANCIAS. Consulte a su administrador.");
            }
            return lista;
        }


        public List<CSustancia> selectSustanciaArea(string nombreArea)
        {
            List<CSustancia> lista = new List<CSustancia>();
            try
            {
                string SQL = string.Empty;
                SQL += "SELECT Sustancia.idSustancia, nombreSustancia, usoRecomendado, palabraAdvertencia, imagenSustancia";
                SQL += " FROM Sustancia, SustanciaArea, Area";
                SQL += " WHERE Sustancia.idSustancia = SustanciaArea.idSustancia";
                SQL += " AND Area.idArea = SustanciaArea.idArea";
                SQL += " AND Area.nombreArea = '" + nombreArea.ToString() + "'";
                SQL += " ORDER BY Sustancia.nombreSustancia ASC";

                SqlCommand command = new SqlCommand(SQL, objectConexion.establecerConexion());

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                DataSet dataset = new DataSet();

                sqlDataAdapter.Fill(dataset, "Sustancia");
                objectConexion.cerrarConexion();

                foreach (DataRow row in dataset.Tables[0].Rows)
                {
                    CSustancia objeto = new CSustancia();
                    objeto.IdSustancia = Convert.ToInt32(row["idSustancia"].ToString());
                    objeto.NombreSustancia = row["nombreSustancia"].ToString();
                    objeto.UsoRecomendado = row["usoRecomendado"].ToString();
                    objeto.PalabraAdvertencia = row["palabraAdvertencia"].ToString();
                    objeto.ImagenSustancia = (byte[])row["imagenSustancia"];
                    lista.Add(objeto);
                }
                objectConexion.cerrarConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("NO SE PUDIERON SELECCIONAR LAS SUSTANCIAS. Consulte a su administrador.");
            }
            return lista;
        }

        public List<CSustancia> selectSustanciaGaveta(string nombreGaveta)
        {
            List<CSustancia> lista = new List<CSustancia>();
            try
            {
                string SQL = string.Empty;
                SQL += "SELECT Sustancia.idSustancia, nombreSustancia, usoRecomendado, palabraAdvertencia, imagenSustancia";
                SQL += " FROM Sustancia, Gaveta, SustanciaGaveta";
                SQL += " WHERE Sustancia.idSustancia = SustanciaGaveta.idSustancia";
                SQL += " AND Gaveta.idGaveta = SustanciaGaveta.idGaveta";
                SQL += " AND Gaveta.nombreGaveta = '" + nombreGaveta.ToString() + "'";
                SQL += " ORDER BY Sustancia.nombreSustancia ASC";

                SqlCommand command = new SqlCommand(SQL, objectConexion.establecerConexion());

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                DataSet dataset = new DataSet();

                sqlDataAdapter.Fill(dataset, "Sustancia");
                objectConexion.cerrarConexion();

                foreach (DataRow row in dataset.Tables[0].Rows)
                {
                    CSustancia objeto = new CSustancia();
                    objeto.IdSustancia = Convert.ToInt32(row["idSustancia"].ToString());
                    objeto.NombreSustancia = row["nombreSustancia"].ToString();
                    objeto.UsoRecomendado = row["usoRecomendado"].ToString();
                    objeto.PalabraAdvertencia = row["palabraAdvertencia"].ToString();
                    objeto.ImagenSustancia = (byte[])row["imagenSustancia"];
                    lista.Add(objeto);
                }
                objectConexion.cerrarConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show("NO SE PUDIERON SELECCIONAR LAS SUSTANCIAS. Consulte a su administrador.");
            }
            return lista;
        }

        public void mostrarGavetaCombo(ComboBox comboArea)
        {
            try
            {
                comboArea.DataSource = null;
                SqlDataAdapter adapter = new SqlDataAdapter("SELECT idGaveta,nombreGaveta FROM Gaveta " +
                    "ORDER BY nombreGaveta ASC", 
                    objectConexion.establecerConexion());

                DataTable dt = new DataTable();
                adapter.Fill(dt);

                comboArea.DataSource = dt;
                comboArea.DisplayMember = "nombreGaveta";
                comboArea.ValueMember = "idGaveta";

                objectConexion.cerrarConexion();
            }
            catch (Exception e)
            {
                MessageBox.Show("No se logró mostrar. Consulte a su administrador.");
            }
        }
    }
}