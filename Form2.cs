﻿using System.Diagnostics;
using System.Windows.Forms;

namespace PruebaScreen
{
    public partial class Form2 : Form
    {
        Clases.CSustancia cSustancia = new Clases.CSustancia();
        Clases.CGaveta cGaveta = new Clases.CGaveta();
        Salir salir;
        Font SmallFont = new Font("Siemens Sans", 10);
        Font SmallFontBold = new Font("Siemens Sans", 10, FontStyle.Bold);
        Font MediumFont = new Font("Siemens Sans", 12);
        Font LargeFont = new Font("Siemens Sans", 14, FontStyle.Bold);
        TableLayoutPanel tableLayoutPanel1 = new TableLayoutPanel();
        private int rol;
        private int currentPage = 1;
        int itemsPerPage = 10;

        public Form2()
        {
            InitializeComponent();
            this.tableLayoutPanel1 = new TableLayoutPanel();
            mostrarSustancias();
            cSustancia.mostrarGavetaCombo(cmbBoxGaveta);
            cSustancia.mostrarAreaCombo(cmbBoxArea);
            cmbBoxArea.Enabled = false;
            cmbBoxGaveta.Enabled = false;
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        public void mostrarSustancias()
        {
            List<Clases.CSustancia> lista = new List<Clases.CSustancia>();
            lista = cSustancia.selectSustancias();
            int n = cSustancia.selectSustancias().Count();

            int startIndex = (currentPage - 1) * itemsPerPage;
            int endIndex = Math.Min(startIndex + itemsPerPage, lista.Count);

            for (int i = startIndex; i < endIndex; i++)
            {
                PictureBox pictureBox = new PictureBox();

                // Crear un contenedor para el elemento de la lista
                TableLayoutPanel itemPanel = new TableLayoutPanel();
                itemPanel.AutoSize = true;
                itemPanel.ColumnCount = 6;
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                itemPanel.Dock = DockStyle.Fill;
                itemPanel.Anchor = AnchorStyles.Left | AnchorStyles.Right |
                    AnchorStyles.Top | AnchorStyles.Bottom;

                using (MemoryStream ms = new MemoryStream(lista[i].ImagenSustancia))
                {
                    // Crear una instancia de Image a partir del MemoryStream
                    Image imagen = Image.FromStream(ms);

                    pictureBox.Image = imagen;
                }

                pictureBox.Margin = new Padding(0, 0, 0, 50);
                pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                pictureBox.Size = new Size(200, 200);
                pictureBox.Anchor = AnchorStyles.Left;

                Label label = new Label();
                label.Text = "N. de Sustancia: " + lista[i].IdSustancia.ToString();
                label.AutoSize = false; // Establece AutoSize en false
                label.Dock = DockStyle.Top; // Establece Dock en Top
                label.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                label.Margin = new Padding(20, 0, 0, 25);
                label.Font = LargeFont;
                label.AutoSize = false;
                label.Size = new Size(label.PreferredWidth, label.PreferredHeight);

                Label label2 = new Label();
                label2.Text = lista[i].NombreSustancia.ToString();
                label2.Tag = lista[i].IdSustancia;
                label2.Margin = new Padding(20, 0, 0, 25);
                label2.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                label2.Font = LargeFont;
                label2.AutoSize = false;
                label2.Size = new Size(label2.PreferredWidth, label2.PreferredHeight);
                label2.Click += DetalleButtonClick;
                label2.Cursor = Cursors.Hand;

                Label label3 = new Label();
                label3.Text = lista[i].UsoRecomendado.ToString();
                label3.Margin = new Padding(20, 0, 0, 25);
                label3.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                label3.Font = SmallFont;
                label3.AutoSize = false;
                label3.Size = new Size(label3.PreferredWidth, label3.PreferredHeight);

                Label label4 = new Label();
                label4.Text = "Palabra de advertencia: " + lista[i].PalabraAdvertencia.ToString();
                label4.Margin = new Padding(20, 0, 0, 50);
                label4.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                label4.Font = SmallFontBold;
                label4.AutoSize = false;
                label4.Size = new Size(label4.PreferredWidth, label4.PreferredHeight);

                Label label5 = new Label();
                label5.Text = "Gavetas a las que pertenece ";
                label5.Margin = new Padding(20, 0, 0, 25);
                label5.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                label5.Font = SmallFontBold;
                label5.AutoSize = false;
                label5.Size = new Size(label5.PreferredWidth, label5.PreferredHeight);

                // Manejador de evento para el botón de editar
                Button verButton = new Button();
                verButton.Text = "Ver HDS";
                verButton.Anchor = AnchorStyles.Right;
                verButton.Tag = lista[i].IdSustancia; // Guardar el índice del elemento en el botón
                verButton.Click += VerButtonClick;
                verButton.AutoSize = true;
                verButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(125)))), ((int)(((byte)(45)))));
                verButton.FlatAppearance.BorderSize = 0;
                verButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                verButton.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
                verButton.ForeColor = System.Drawing.Color.White;
                verButton.UseVisualStyleBackColor = false;

                Button etiquetaButton = new Button();
                etiquetaButton.Text = "Etiqueta";
                etiquetaButton.Anchor = AnchorStyles.Right;
                etiquetaButton.Tag = lista[i].IdSustancia; // Guardar el índice del elemento en el botón
                etiquetaButton.Click += EtiquetaButtonClick;
                etiquetaButton.AutoSize = true;
                etiquetaButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(125)))), ((int)(((byte)(45)))));
                etiquetaButton.FlatAppearance.BorderSize = 0;
                etiquetaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                etiquetaButton.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
                etiquetaButton.ForeColor = System.Drawing.Color.White;
                etiquetaButton.UseVisualStyleBackColor = false;

                // Ajustar el ancho de las columnas del TableLayoutPanel
                itemPanel.ColumnStyles[1] = new ColumnStyle(SizeType.Percent, 100);
                itemPanel.ColumnStyles[2] = new ColumnStyle(SizeType.AutoSize);
                itemPanel.ColumnStyles[3] = new ColumnStyle(SizeType.AutoSize);
                itemPanel.ColumnStyles[4] = new ColumnStyle(SizeType.AutoSize);
                itemPanel.ColumnStyles[5] = new ColumnStyle(SizeType.AutoSize);

                itemPanel.SetColumnSpan(pictureBox, 1);
                itemPanel.SetRowSpan(pictureBox, 5);
                itemPanel.Controls.Add(pictureBox, 0, 0);

                itemPanel.SetColumnSpan(label, 1);
                itemPanel.SetRowSpan(label, 1);
                itemPanel.Controls.Add(label, 1, 0);

                itemPanel.SetColumnSpan(label2, 1);
                itemPanel.SetRowSpan(label2, 1);
                itemPanel.Controls.Add(label2, 1, 1);

                itemPanel.SetColumnSpan(label3, 1);
                itemPanel.SetRowSpan(label3, 1);
                itemPanel.Controls.Add(label3, 1, 2);

                itemPanel.SetColumnSpan(label4, 1);
                itemPanel.SetRowSpan(label4, 1);
                itemPanel.Controls.Add(label4, 1, 3);

                itemPanel.SetColumnSpan(label5, 1);
                itemPanel.SetRowSpan(label5, 1);
                itemPanel.Controls.Add(label5, 1, 4);

                itemPanel.Controls.Add(verButton, 4, 0); //1
                itemPanel.Controls.Add(etiquetaButton, 4, 1); //0

                //////////////////////////////////////////////////
                List<Clases.CGaveta> listaGaveta = cGaveta.GetGavetaById(lista[i].IdSustancia);
                int ngaveta = listaGaveta.Count;

                FlowLayoutPanel flowLayoutPanelGaveta = new FlowLayoutPanel();
                flowLayoutPanelGaveta.FlowDirection = FlowDirection.LeftToRight;
                flowLayoutPanelGaveta.AutoSize = true; // Ajustar el tamaño automáticamente
                itemPanel.Controls.Add(flowLayoutPanelGaveta, 1, 5);

                for (int j = 0; j < ngaveta; j++)
                {
                    Label gaveta = new Label();
                    gaveta.Text = listaGaveta[j].NombreGaveta.ToString();
                    gaveta.Margin = new Padding(20, 0, 0, 50);
                    gaveta.Font = SmallFont;
                    gaveta.AutoSize = false;
                    gaveta.Size = new Size(gaveta.PreferredWidth, gaveta.PreferredHeight);

                    flowLayoutPanelGaveta.Controls.Add(gaveta);
                }
                //////////////////////////////////////////////////////

                flowLayoutPanel1.Controls.Add(itemPanel);
            }
        }

        private void CargarMasElementos()
        {
            currentPage++;
            mostrarSustancias();
        }

        private void DetalleButtonClick(object sender, EventArgs e)
        {
            Label label2 = (Label)sender;
            int indice = (int)label2.Tag;

            Detalle detalle = new Detalle(indice, rol);
            detalle.ShowDialog();
        }

        public void EtiquetaButtonClick(object sender, EventArgs e)
        {
            Button etiquetaButton = (Button)sender;
            int indice = (int)(etiquetaButton.Tag);

            List<Clases.CSustancia> lista = new List<Clases.CSustancia>();
            lista = cSustancia.sustanciaId(indice);
            int n = cSustancia.sustanciaId(indice).Count();
            //MessageBox.Show("HAY: "+n +" RESULTADOS");

            foreach (Clases.CSustancia item in lista)
            {
                //CREAR CARPETA TEMPORAL DONDE SE GUARDARÁN LOS ARCHIVOS
                string direccion = AppDomain.CurrentDomain.BaseDirectory;
                string carpeta = direccion + @"temp\";
                string ubicacionCompleta = carpeta + item.EtiquetaExtension;
                /*MessageBox.Show("DIRECCION: " + direccion + "\n" +
                                "CARPETA: " + carpeta + "\n" +
                                "UBICACIONCOMPLETA: " + ubicacionCompleta);*/

                //Validaciones
                if (!Directory.Exists(carpeta))
                    Directory.CreateDirectory(carpeta);

                if (File.Exists(ubicacionCompleta))
                    File.Delete(ubicacionCompleta);

                File.WriteAllBytes(ubicacionCompleta, item.EtiquetaSustancia);

                var p = new Process();
                p.StartInfo = new ProcessStartInfo(ubicacionCompleta)
                {
                    UseShellExecute = true
                };
                p.Start();
            }
        }

        private void VerButtonClick(object sender, EventArgs e)
        {
            Button verButton = (Button)sender;
            int indice = (int)verButton.Tag;

            List<Clases.CSustancia> lista = new List<Clases.CSustancia>();
            lista = cSustancia.sustanciaId(indice);
            int n = cSustancia.sustanciaId(indice).Count();

            foreach (Clases.CSustancia item in lista)
            {
                //CREAR CARPETA TEMPORAL DONDE SE GUARDARÁN LOS ARCHIVOS
                string direccion = AppDomain.CurrentDomain.BaseDirectory;
                string carpeta = direccion + @"temp\";
                string ubicacionCompleta = carpeta + item.HdsExtension;
                /*MessageBox.Show("DIRECCION: " + direccion + "\n" +
                                "CARPETA: " + carpeta + "\n" +
                                "UBICACIONCOMPLETA: " + ubicacionCompleta);*/

                //Validaciones
                if (!Directory.Exists(carpeta))
                    Directory.CreateDirectory(carpeta);

                if (File.Exists(ubicacionCompleta))
                    File.Delete(ubicacionCompleta);

                File.WriteAllBytes(ubicacionCompleta, item.HdsSustancia);

                var p = new Process();
                p.StartInfo = new ProcessStartInfo(ubicacionCompleta)
                {
                    UseShellExecute = true
                };
                p.Start();
            }
        }

        //Métodos para hacer un Loading
        public void Algo()
        {
            //Tiempo que esperará 
            Thread.Sleep(3000);
        }

        public void Show()
        {
            //Inicializa el form salir y lo muestra
            salir = new Salir();
            salir.Show();
        }

        public void Esconder()
        {
            //Método para cerrar el form salir
            if (salir != null)
                salir.Close();
        }

        private async void btnExit_Click(object sender, EventArgs e)
        {
            Show(); //Hace el método mostrar
            Task oTask = new Task(Algo); //Ejecución de tarea asíncrona
            oTask.Start(); //Inicia la tarea
            await oTask; //Espera a que haga la tarea
            Form1 salir = new Form1(); //Llama al form principal de iniciar sesión
            this.Hide(); //Esconde la pantalla actual
            Esconder(); //Cierra salir 
            salir.ShowDialog(); //Nos lleva a la pantalla que queremos
            this.Close(); //Cierra la pantalla actual
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            string textoBusqueda = txtBuscar.Text.ToLower();

            foreach (Control control in flowLayoutPanel1.Controls)
            {
                if (control is TableLayoutPanel tablePanel)
                {
                    bool hayCoincidencia = false;

                    // Busca los controles Label dentro de cada TableLayoutPanel
                    foreach (Control subControl in tablePanel.Controls)
                    {
                        if (subControl is FlowLayoutPanel flow)
                        {
                            foreach (Control control2 in flow.Controls)
                            {
                                if (control2 is Label label2 && label2.Text.ToLower().Contains(textoBusqueda))
                                {
                                    hayCoincidencia = true;
                                    break;
                                }
                            }
                        }
                        if (subControl is Label label && label.Text.ToLower().Contains(textoBusqueda))
                        {
                            hayCoincidencia = true;
                            break;
                        }
                    }
                    // Muestra u oculta el TableLayoutPanel según la existencia de coincidencias
                    tablePanel.Visible = hayCoincidencia;
                }
            }
        }

        private void btnCargar_Click(object sender, EventArgs e)
        {
            CargarMasElementos();
        }

        private void btnCargarFiltro_Click(object sender, EventArgs e)
        {
            CargarMasElementosFiltro();
        }

        private void mostrarSustanciaFiltro()
        {
            List<Clases.CSustancia> lista = new List<Clases.CSustancia>();
            int n = 0;
            string nombreArea = "";
            string nombreGaveta = "";
            if (cmbBoxArea.Enabled == false && cmbBoxGaveta.Enabled == false)
            {
                MessageBox.Show("Debes activar al menos 1 campo para filtrar");
            }
            else if (cmbBoxArea.Enabled == true && cmbBoxGaveta.Enabled == true)
            {
                //MessageBox.Show("SELECCIONASTE LOS 2");
                nombreArea = cmbBoxArea.Text;
                nombreGaveta = cmbBoxGaveta.Text;
                lista = cSustancia.selectSustanciaAreasGavetas(nombreArea, nombreGaveta);
                n = lista.Count();
            }
            else if (cmbBoxArea.Enabled == true && cmbBoxGaveta.Enabled == false)
            {
                //MessageBox.Show("SOLO SELECCIONASTE LAS AREAS");
                nombreArea = cmbBoxArea.Text;
                lista = cSustancia.selectSustanciaArea(nombreArea);
                n = lista.Count();
            }
            else if (cmbBoxArea.Enabled == false && cmbBoxGaveta.Enabled == true)
            {
                //MessageBox.Show("SOLO SELECCIONASTE LAS GAVETAS");
                nombreGaveta = cmbBoxGaveta.Text;
                lista = cSustancia.selectSustanciaGaveta(nombreGaveta);
                n = lista.Count();
            }
            if (n < 1)
            {
                MessageBox.Show("No hay sustancias con esas especificaciones");
            }
            else
            {
                int startIndex = (currentPage - 1) * itemsPerPage;
                int endIndex = Math.Min(startIndex + itemsPerPage, lista.Count);

                for (int i = startIndex; i < endIndex; i++)
                {
                    PictureBox pictureBox = new PictureBox();

                    // Crear un contenedor para el elemento de la lista
                    TableLayoutPanel itemPanel = new TableLayoutPanel();
                    itemPanel.AutoSize = true;
                    itemPanel.ColumnCount = 6;
                    itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                    itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));
                    itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                    itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                    itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                    itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                    itemPanel.Dock = DockStyle.Fill;
                    itemPanel.Anchor = AnchorStyles.Left | AnchorStyles.Right |
                        AnchorStyles.Top | AnchorStyles.Bottom;

                    using (MemoryStream ms = new MemoryStream(lista[i].ImagenSustancia))
                    {
                        // Crear una instancia de Image a partir del MemoryStream
                        Image imagen = Image.FromStream(ms);

                        pictureBox.Image = imagen;
                    }

                    pictureBox.Margin = new Padding(0, 0, 0, 50);
                    pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                    pictureBox.Size = new Size(200, 200);
                    pictureBox.Anchor = AnchorStyles.Left;

                    Label label = new Label();
                    label.Text = "N. de Sustancia: " + lista[i].IdSustancia.ToString();
                    label.AutoSize = false; // Establece AutoSize en false
                    label.Dock = DockStyle.Top; // Establece Dock en Top
                    label.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                    label.Margin = new Padding(20, 0, 0, 25);
                    label.Font = LargeFont;
                    label.AutoSize = false;
                    label.Size = new Size(label.PreferredWidth, label.PreferredHeight);

                    Label label2 = new Label();
                    label2.Text = lista[i].NombreSustancia.ToString();
                    label2.Tag = lista[i].IdSustancia;
                    label2.Margin = new Padding(20, 0, 0, 25);
                    label2.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                    label2.Font = LargeFont;
                    label2.AutoSize = false;
                    label2.Size = new Size(label2.PreferredWidth, label2.PreferredHeight);
                    label2.Click += DetalleButtonClick;
                    label2.Cursor = Cursors.Hand;

                    Label label3 = new Label();
                    label3.Text = lista[i].UsoRecomendado.ToString();
                    label3.Margin = new Padding(20, 0, 0, 25);
                    label3.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                    label3.Font = SmallFont;
                    label3.AutoSize = false;
                    label3.Size = new Size(label3.PreferredWidth, label3.PreferredHeight);

                    Label label4 = new Label();
                    label4.Text = "Palabra de advertencia: " + lista[i].PalabraAdvertencia.ToString();
                    label4.Margin = new Padding(20, 0, 0, 25);
                    label4.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                    label4.Font = SmallFontBold;
                    label4.AutoSize = false;
                    label4.Size = new Size(label4.PreferredWidth, label4.PreferredHeight);

                    Label label5 = new Label();
                    label5.Text = "Gavetas a las que pertenece ";
                    label5.Margin = new Padding(20, 0, 0, 25);
                    label5.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                    label5.Font = SmallFontBold;
                    label5.AutoSize = false;
                    label5.Size = new Size(label5.PreferredWidth, label5.PreferredHeight);

                    ///// BOTONES //////////////////////////

                    // Manejador de evento para el botón de editar
                    Button verButton = new Button();
                    verButton.Text = "Ver HDS";
                    verButton.Anchor = AnchorStyles.Right;
                    verButton.Tag = lista[i].IdSustancia; // Guardar el índice del elemento en el botón
                    verButton.Click += VerButtonClick;
                    verButton.AutoSize = true;
                    verButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(125)))), ((int)(((byte)(45)))));
                    verButton.FlatAppearance.BorderSize = 0;
                    verButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                    verButton.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
                    verButton.ForeColor = System.Drawing.Color.White;
                    verButton.UseVisualStyleBackColor = false;

                    Button etiquetaButton = new Button();
                    etiquetaButton.Text = "Etiqueta";
                    etiquetaButton.Anchor = AnchorStyles.Right;
                    etiquetaButton.Tag = lista[i].IdSustancia; // Guardar el índice del elemento en el botón
                    etiquetaButton.Click += EtiquetaButtonClick;
                    etiquetaButton.AutoSize = true;
                    etiquetaButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(125)))), ((int)(((byte)(45)))));
                    etiquetaButton.FlatAppearance.BorderSize = 0;
                    etiquetaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                    etiquetaButton.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
                    etiquetaButton.ForeColor = System.Drawing.Color.White;
                    etiquetaButton.UseVisualStyleBackColor = false;

                    // Ajustar el ancho de las columnas del TableLayoutPanel
                    itemPanel.ColumnStyles[1] = new ColumnStyle(SizeType.Percent, 100);
                    itemPanel.ColumnStyles[2] = new ColumnStyle(SizeType.AutoSize);
                    itemPanel.ColumnStyles[3] = new ColumnStyle(SizeType.AutoSize);
                    itemPanel.ColumnStyles[4] = new ColumnStyle(SizeType.AutoSize);
                    itemPanel.ColumnStyles[5] = new ColumnStyle(SizeType.AutoSize);

                    itemPanel.SetColumnSpan(pictureBox, 1);
                    itemPanel.SetRowSpan(pictureBox, 5);
                    itemPanel.Controls.Add(pictureBox, 0, 0);

                    itemPanel.SetColumnSpan(label, 1);
                    itemPanel.SetRowSpan(label, 1);
                    itemPanel.Controls.Add(label, 1, 0);

                    itemPanel.SetColumnSpan(label2, 1);
                    itemPanel.SetRowSpan(label2, 1);
                    itemPanel.Controls.Add(label2, 1, 1);

                    itemPanel.SetColumnSpan(label3, 1);
                    itemPanel.SetRowSpan(label3, 1);
                    itemPanel.Controls.Add(label3, 1, 2);

                    itemPanel.SetColumnSpan(label4, 1);
                    itemPanel.SetRowSpan(label4, 1);
                    itemPanel.Controls.Add(label4, 1, 3);

                    itemPanel.SetColumnSpan(label5, 1);
                    itemPanel.SetRowSpan(label5, 1);
                    itemPanel.Controls.Add(label5, 1, 4);

                    itemPanel.Controls.Add(verButton, 4, 0); //1
                    itemPanel.Controls.Add(etiquetaButton, 4, 1); //0
                    //////////////////////////////////////////////////
                    List<Clases.CGaveta> listaGaveta = cGaveta.GetGavetaById(lista[i].IdSustancia);
                    int ngaveta = listaGaveta.Count;

                    FlowLayoutPanel flowLayoutPanelGaveta = new FlowLayoutPanel();
                    flowLayoutPanelGaveta.FlowDirection = FlowDirection.LeftToRight;
                    flowLayoutPanelGaveta.AutoSize = true; // Ajustar el tamaño automáticamente
                    itemPanel.Controls.Add(flowLayoutPanelGaveta, 1, 5);

                    for (int j = 0; j < ngaveta; j++)
                    {
                        Label gaveta = new Label();
                        gaveta.Text = listaGaveta[j].NombreGaveta.ToString();
                        gaveta.Margin = new Padding(20, 0, 0, 30);
                        gaveta.Font = SmallFont;
                        gaveta.AutoSize = false;
                        gaveta.Size = new Size(gaveta.PreferredWidth, gaveta.PreferredHeight);

                        flowLayoutPanelGaveta.Controls.Add(gaveta);
                    }
                    //////////////////////////////////////////////////////

                    flowLayoutPanel1.Controls.Add(itemPanel);
                }
            }
        }

        private void btnEliminarFiltro_Click(object sender, EventArgs e)
        {
            flowLayoutPanel1.Controls.Clear();
            currentPage = 1;
            btnCargar.Visible = true;
            btnCargarFiltro.Visible = false;
            btnCargarBuscar.Visible = false;
            mostrarSustancias();
        }

        private void btnFiltro_Click(object sender, EventArgs e)
        {
            flowLayoutPanel1.Controls.Clear();
            currentPage = 1;
            btnCargar.Visible = false;
            btnCargarFiltro.Visible = true;
            btnCargarBuscar.Visible = false;
            mostrarSustanciaFiltro();
        }

        private void CargarMasElementosFiltro()
        {
            currentPage++;
            mostrarSustanciaFiltro();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            flowLayoutPanel1.Controls.Clear();
            currentPage = 1;
            btnCargar.Visible = false;
            btnCargarFiltro.Visible = false;
            btnCargarBuscar.Visible = true;
            mostrarSustanciaBuscar();
        }

        public void mostrarSustanciaBuscar()
        {
            string busqueda = txtBuscar.Text;
            List<Clases.CSustancia> lista = new List<Clases.CSustancia>();
            lista = cSustancia.BuscarSustancias(busqueda);
            int n = lista.Count();

            if (n < 1)
            {
                MessageBox.Show("No hay sustancias con esas especificaciones");
            }
            else
            {
                int startIndex = (currentPage - 1) * itemsPerPage;
                int endIndex = Math.Min(startIndex + itemsPerPage, lista.Count);

                for (int i = startIndex; i < endIndex; i++)
                {
                    PictureBox pictureBox = new PictureBox();

                    // Crear un contenedor para el elemento de la lista
                    TableLayoutPanel itemPanel = new TableLayoutPanel();
                    itemPanel.AutoSize = true;
                    itemPanel.ColumnCount = 6;
                    itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                    itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));
                    itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                    itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                    itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                    itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                    itemPanel.Dock = DockStyle.Fill;
                    itemPanel.Anchor = AnchorStyles.Left | AnchorStyles.Right |
                        AnchorStyles.Top | AnchorStyles.Bottom;

                    using (MemoryStream ms = new MemoryStream(lista[i].ImagenSustancia))
                    {
                        // Crear una instancia de Image a partir del MemoryStream
                        Image imagen = Image.FromStream(ms);

                        pictureBox.Image = imagen;
                    }

                    pictureBox.Margin = new Padding(0, 0, 0, 50);
                    pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                    pictureBox.Size = new Size(200, 200);
                    pictureBox.Anchor = AnchorStyles.Left;

                    Label label = new Label();
                    label.Text = "N. de Sustancia: " + lista[i].IdSustancia.ToString();
                    label.AutoSize = false; // Establece AutoSize en false
                    label.Dock = DockStyle.Top; // Establece Dock en Top
                    label.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                    label.Margin = new Padding(20, 0, 0, 25);
                    label.Font = LargeFont;
                    label.AutoSize = false;
                    label.Size = new Size(label.PreferredWidth, label.PreferredHeight);

                    Label label2 = new Label();
                    label2.Text = lista[i].NombreSustancia.ToString();
                    label2.Tag = lista[i].IdSustancia;
                    label2.Margin = new Padding(20, 0, 0, 25);
                    label2.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                    label2.Font = LargeFont;
                    label2.AutoSize = false;
                    label2.Size = new Size(label2.PreferredWidth, label2.PreferredHeight);
                    label2.Click += DetalleButtonClick;
                    label2.Cursor = Cursors.Hand;

                    Label label3 = new Label();
                    label3.Text = lista[i].UsoRecomendado.ToString();
                    label3.Margin = new Padding(20, 0, 0, 25);
                    label3.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                    label3.Font = SmallFont;
                    label3.AutoSize = false;
                    label3.Size = new Size(label3.PreferredWidth, label3.PreferredHeight);

                    Label label4 = new Label();
                    label4.Text = "Palabra de advertencia: " + lista[i].PalabraAdvertencia.ToString();
                    label4.Margin = new Padding(20, 0, 0, 25);
                    label4.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                    label4.Font = SmallFontBold;
                    label4.AutoSize = false;
                    label4.Size = new Size(label4.PreferredWidth, label4.PreferredHeight);

                    Label label5 = new Label();
                    label5.Text = "Gavetas a las que pertenece ";
                    label5.Margin = new Padding(20, 0, 0, 25);
                    label5.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                    label5.Font = SmallFontBold;
                    label5.AutoSize = false;
                    label5.Size = new Size(label5.PreferredWidth, label5.PreferredHeight);

                    ///// BOTONES //////////////////////////

                    // Manejador de evento para el botón de editar
                    Button verButton = new Button();
                    verButton.Text = "Ver HDS";
                    verButton.Anchor = AnchorStyles.Right;
                    verButton.Tag = lista[i].IdSustancia; // Guardar el índice del elemento en el botón
                    verButton.Click += VerButtonClick;
                    verButton.AutoSize = true;
                    verButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(125)))), ((int)(((byte)(45)))));
                    verButton.FlatAppearance.BorderSize = 0;
                    verButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                    verButton.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
                    verButton.ForeColor = System.Drawing.Color.White;
                    verButton.UseVisualStyleBackColor = false;

                    Button etiquetaButton = new Button();
                    etiquetaButton.Text = "Etiqueta";
                    etiquetaButton.Anchor = AnchorStyles.Right;
                    etiquetaButton.Tag = lista[i].IdSustancia; // Guardar el índice del elemento en el botón
                    etiquetaButton.Click += EtiquetaButtonClick;
                    etiquetaButton.AutoSize = true;
                    etiquetaButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(125)))), ((int)(((byte)(45)))));
                    etiquetaButton.FlatAppearance.BorderSize = 0;
                    etiquetaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                    etiquetaButton.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
                    etiquetaButton.ForeColor = System.Drawing.Color.White;
                    etiquetaButton.UseVisualStyleBackColor = false;

                    // Ajustar el ancho de las columnas del TableLayoutPanel
                    itemPanel.ColumnStyles[1] = new ColumnStyle(SizeType.Percent, 100);
                    itemPanel.ColumnStyles[2] = new ColumnStyle(SizeType.AutoSize);
                    itemPanel.ColumnStyles[3] = new ColumnStyle(SizeType.AutoSize);
                    itemPanel.ColumnStyles[4] = new ColumnStyle(SizeType.AutoSize);
                    itemPanel.ColumnStyles[5] = new ColumnStyle(SizeType.AutoSize);

                    itemPanel.SetColumnSpan(pictureBox, 1);
                    itemPanel.SetRowSpan(pictureBox, 5);
                    itemPanel.Controls.Add(pictureBox, 0, 0);

                    itemPanel.SetColumnSpan(label, 1);
                    itemPanel.SetRowSpan(label, 1);
                    itemPanel.Controls.Add(label, 1, 0);

                    itemPanel.SetColumnSpan(label2, 1);
                    itemPanel.SetRowSpan(label2, 1);
                    itemPanel.Controls.Add(label2, 1, 1);

                    itemPanel.SetColumnSpan(label3, 1);
                    itemPanel.SetRowSpan(label3, 1);
                    itemPanel.Controls.Add(label3, 1, 2);

                    itemPanel.SetColumnSpan(label4, 1);
                    itemPanel.SetRowSpan(label4, 1);
                    itemPanel.Controls.Add(label4, 1, 3);

                    itemPanel.SetColumnSpan(label5, 1);
                    itemPanel.SetRowSpan(label5, 1);
                    itemPanel.Controls.Add(label5, 1, 4);

                    itemPanel.Controls.Add(verButton, 4, 0); //1
                    itemPanel.Controls.Add(etiquetaButton, 4, 1); //0
                    //////////////////////////////////////////////////
                    List<Clases.CGaveta> listaGaveta = cGaveta.GetGavetaById(lista[i].IdSustancia);
                    int ngaveta = listaGaveta.Count;

                    FlowLayoutPanel flowLayoutPanelGaveta = new FlowLayoutPanel();
                    flowLayoutPanelGaveta.FlowDirection = FlowDirection.LeftToRight;
                    flowLayoutPanelGaveta.AutoSize = true; // Ajustar el tamaño automáticamente
                    itemPanel.Controls.Add(flowLayoutPanelGaveta, 1, 5);

                    for (int j = 0; j < ngaveta; j++)
                    {
                        Label gaveta = new Label();
                        gaveta.Text = listaGaveta[j].NombreGaveta.ToString();
                        gaveta.Margin = new Padding(20, 0, 0, 30);
                        gaveta.Font = SmallFont;
                        gaveta.AutoSize = false;
                        gaveta.Size = new Size(gaveta.PreferredWidth, gaveta.PreferredHeight);

                        flowLayoutPanelGaveta.Controls.Add(gaveta);
                    }
                    //////////////////////////////////////////////////////

                    flowLayoutPanel1.Controls.Add(itemPanel);
                }
            }
        }

        private void btnCargarBuscar_Click(object sender, EventArgs e)
        {
            CargarMasElementosBuscar();
        }

        private void CargarMasElementosBuscar()
        {
            currentPage++;
            mostrarSustanciaBuscar();
        }

        private void checkBoxGaveta_CheckedChanged(object sender, EventArgs e)
        {
            cmbBoxGaveta.Enabled = checkBoxGaveta.Checked;
        }

        private void checkBoxArea_CheckedChanged(object sender, EventArgs e)
        {
            cmbBoxArea.Enabled = checkBoxArea.Checked;
        }

        private void linkCheckList_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                VisitLink();
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se puede abrir el link. Intente más tarde.");
            }
        }

        public void VisitLink()
        {
            // Change the color of the link text by setting LinkVisited
            // to true.
            linkCheckList.LinkVisited = true;
            //Call the Process.Start method to open the default browser
            //with a URL:
            System.Diagnostics.Process.Start(new ProcessStartInfo
            {
                FileName = "https://forms.office.com/r/be9wE97W5V",
                UseShellExecute = true
            });
        }
    }
}