﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaScreen
{
    public partial class Eliminar : Form
    {
        Clases.CSustancia cSustancia = new Clases.CSustancia();
        Clases.CArea cCArea = new Clases.CArea();
        Clases.CEquipopp cEquipopp = new Clases.CEquipopp();
        Clases.CGaveta cGaveta = new Clases.CGaveta();
        Clases.CPictograma cPictograma = new Clases.CPictograma();
        Delete delete;
        private int indice;
        private int rol;
        private int idUsuario;

        public Eliminar(int indice, int idUsuario, int rol)
        {
            InitializeComponent();
            this.indice = indice;
            this.idUsuario = idUsuario;
            this.rol = rol;
        }

        private async void btnEliminar_Click(object sender, EventArgs e)
        {
            Show(); //Hace el método mostrar
            Task oTask = new Task(Algo); //Ejecución de tarea asíncrona
            oTask.Start(); //Inicia la tarea
            //MessageBox.Show("ELIMINARÁS EL ELEMENTO: "+indice);
            cSustancia.eliminarSustancia(indice);
            cCArea.eliminarSustanciaArea(indice);
            cEquipopp.eliminarSustanciaEpp(indice);
            cGaveta.eliminarSustanciaGaveta(indice);
            cPictograma.eliminarSustanciaPictograma(indice);
            await oTask; //Espera a que haga la tarea
            Administrador guardar = new Administrador(rol, idUsuario); //Llama al form principal de iniciar sesión
            this.Hide(); //Esconde la pantalla actual
            Esconder(); //Cierra salir 
            guardar.ShowDialog(); //Nos lleva a la pantalla que queremos
            this.Close(); //Cierra la pantalla actual
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Administrador administrador = new Administrador(rol, idUsuario); //Llama al form principal de iniciar sesión
            this.Hide(); //Esconde la pantalla actual
            administrador.ShowDialog(); //Nos lleva a la pantalla que queremos
            this.Close(); //Cierra la pantalla actual
        }

        //Métodos para hacer un Loading
        public void Algo()
        {
            //Tiempo que esperará 
            Thread.Sleep(3000);
        }

        public void Show()
        {
            //Inicializa el form salir y lo muestra
            delete = new Delete();
            delete.Show();
        }

        public void Esconder()
        {
            //Método para cerrar el form salir
            if (delete != null)
                delete.Close();
        }
    }
}
