﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaScreen
{
    public partial class Salir : Form
    {
        public Salir()
        {
            InitializeComponent();
        }

        private void Salir_Load(object sender, EventArgs e)
        {
            //Cargamos el GIF en el picture box que se colocó en el Form
            MiSalir.Load("salida.gif"); //Asignamos el GIF
            MiSalir.Location = new Point(this.Width / 2 - MiSalir.Width / 2,
                this.Height / 2 - MiSalir.Height / 2); //Lo colocamos a la mitad de la pantalla
        }
    }
}
