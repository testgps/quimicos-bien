﻿namespace PruebaScreen
{
    partial class Delete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MiEliminar = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.MiEliminar)).BeginInit();
            this.SuspendLayout();
            // 
            // MiEliminar
            // 
            this.MiEliminar.Location = new System.Drawing.Point(111, 33);
            this.MiEliminar.Name = "MiEliminar";
            this.MiEliminar.Size = new System.Drawing.Size(898, 654);
            this.MiEliminar.TabIndex = 0;
            this.MiEliminar.TabStop = false;
            // 
            // Delete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1107, 732);
            this.Controls.Add(this.MiEliminar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Delete";
            this.Opacity = 0.7D;
            this.Text = "Delete";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Delete_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MiEliminar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PictureBox MiEliminar;
    }
}