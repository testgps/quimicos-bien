﻿using PruebaScreen.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaScreen
{
    public partial class Solicitudes : Form
    {
        Clases.CSolicitud CSolicitud = new Clases.CSolicitud();
        Font SmallFont = new Font("Siemens Sans", 10);
        Font SmallFontBold = new Font("Siemens Sans", 10, FontStyle.Bold);
        Font MediumFont = new Font("Siemens Sans", 12);
        Font LargeFont = new Font("Siemens Sans", 14, FontStyle.Bold);
        private int rol;
        private int idUsuario;

        public Solicitudes(int rol, int idUsuario)
        {
            InitializeComponent();
            this.rol = rol;
            this.idUsuario = idUsuario;
            if (rol == 3)
            {
                btnRegresar.Visible = false;
                btnEliminarSoli.Visible = false;
            }
            if (rol == 1)
            {
                btnSolicitar.Visible = false;
            }
            mostrarSolicitudes();

        }

        private void Solicitudes_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnSolicitar_Click(object sender, EventArgs e)
        {
            Solicitud solicitud = new Solicitud(rol, idUsuario);
            this.Hide();
            solicitud.ShowDialog();
            this.Close();
        }

        private void btnSalirSolicitud_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            this.Hide();
            form1.ShowDialog();
            this.Close();
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            Administrador administrador = new Administrador(rol, idUsuario); //Llama al form principal de iniciar sesión
            this.Hide(); //Esconde la pantalla actual
            administrador.ShowDialog(); //Nos lleva a la pantalla que queremos
            this.Close(); //Cierra la pantalla actual
        }

        public void mostrarSolicitudes()
        {
            List<Clases.CSolicitud> listaSolicitudes = new List<Clases.CSolicitud>();
            listaSolicitudes = CSolicitud.selectSolicitud();
            int n = listaSolicitudes.Count();

            for (int i = 0; i < n; i++)
            {
                // Crear un contenedor para el elemento de la lista
                TableLayoutPanel itemPanel = new TableLayoutPanel();
                itemPanel.AutoSize = true;
                itemPanel.ColumnCount = 6;
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                itemPanel.Dock = DockStyle.Fill;
                itemPanel.Anchor = AnchorStyles.Left | AnchorStyles.Right |
                    AnchorStyles.Top | AnchorStyles.Bottom;

                Label label = new Label();
                label.Text = "N. de Solicitud: " + listaSolicitudes[i].IdSolicitud.ToString();
                label.AutoSize = false; // Establece AutoSize en false
                label.Dock = DockStyle.Top; // Establece Dock en Top
                label.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                label.Margin = new Padding(20, 0, 0, 25);
                label.Font = LargeFont;
                label.AutoSize = false;
                label.Size = new Size(label.PreferredWidth, label.PreferredHeight);

                Label label2 = new Label();
                label2.Text = listaSolicitudes[i].NombreSolicitud.ToString();
                label2.Tag = listaSolicitudes[i].IdSolicitud;
                label2.Margin = new Padding(20, 0, 0, 25);
                label2.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                label2.Font = LargeFont;
                label2.AutoSize = false;
                label2.Size = new Size(label2.PreferredWidth, label2.PreferredHeight);
                label2.Click += DetalleButtonClick;
                label2.Cursor = Cursors.Hand;

                Label label3 = new Label();
                label3.Text = listaSolicitudes[i].FabricanteSolicitud.ToString();
                label3.Margin = new Padding(20, 0, 0, 25);
                label3.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                label3.Font = SmallFont;
                label3.AutoSize = false;
                label3.Size = new Size(label3.PreferredWidth, label3.PreferredHeight);

                Label label4 = new Label();
                label4.Text = "Fecha de solicitud: " + listaSolicitudes[i].FechaSolicitud.ToString();
                label4.Margin = new Padding(20, 0, 0, 25);
                label4.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                label4.Font = SmallFontBold;
                label4.AutoSize = false;
                label4.Size = new Size(label4.PreferredWidth, label4.PreferredHeight);

                Label label5 = new Label();
                if (listaSolicitudes[i].StatusSolicitud.ToString() == string.Empty)
                {
                    label5.Text = "STATUS: NA";
                    label5.Margin = new Padding(20, 0, 0, 25);
                    label5.Font = SmallFontBold;
                    label5.AutoSize = false;
                    label5.Size = new Size(label5.PreferredWidth, label5.PreferredHeight);
                }
                else
                {
                    label5.Text = "STATUS: " + listaSolicitudes[i].StatusSolicitud.ToString();
                    label5.Margin = new Padding(20, 0, 0, 25);
                    label5.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                    label5.Font = SmallFontBold;
                    label5.AutoSize = false;
                    label5.Size = new Size(label5.PreferredWidth, label5.PreferredHeight);
                }

                Label separatorLabel = new Label();
                separatorLabel.BorderStyle = BorderStyle.FixedSingle;
                separatorLabel.AutoSize = false;
                separatorLabel.Height = 1;
                separatorLabel.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                separatorLabel.Margin = new Padding(20, 0, 0, 25);
                separatorLabel.Padding = new Padding(0);
                separatorLabel.BorderStyle = BorderStyle.None;
                separatorLabel.BackColor = Color.Gray;
                int separatorWidth = 400; // Ajusta el ancho según tus necesidades
                separatorLabel.Size = new Size(separatorWidth, separatorLabel.Height);

                ///// BOTONES //////////////////////////
                // Crear los botones de editar y borrar
                Button editarButton = new Button();
                editarButton.Text = "Editar";
                editarButton.Anchor = AnchorStyles.Right;
                editarButton.Tag = listaSolicitudes[i].IdSolicitud; // Guardar el índice del elemento en el botón
                editarButton.Click += EditarButtonClick;
                editarButton.AutoSize = true;
                editarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
                editarButton.FlatAppearance.BorderSize = 0;
                editarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                editarButton.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
                editarButton.ForeColor = System.Drawing.Color.White;
                editarButton.UseVisualStyleBackColor = false;

                // Manejador de evento para el botón de editar
                Button verButton = new Button();
                verButton.Text = "Ver HDS";
                verButton.Anchor = AnchorStyles.Right;
                verButton.Tag = listaSolicitudes[i].IdSolicitud; // Guardar el índice del elemento en el botón
                verButton.Click += VerHdsButtonClick;
                verButton.AutoSize = true;
                verButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(125)))), ((int)(((byte)(45)))));
                verButton.FlatAppearance.BorderSize = 0;
                verButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                verButton.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
                verButton.ForeColor = System.Drawing.Color.White;
                verButton.UseVisualStyleBackColor = false;

                Button fichaButton = new Button();
                fichaButton.Text = "Ver Ficha";
                fichaButton.Anchor = AnchorStyles.Right;
                fichaButton.Tag = listaSolicitudes[i].IdSolicitud; // Guardar el índice del elemento en el botón
                fichaButton.Click += FichaButtonClick;
                fichaButton.AutoSize = true;
                fichaButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(125)))), ((int)(((byte)(45)))));
                fichaButton.FlatAppearance.BorderSize = 0;
                fichaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                fichaButton.Font = new System.Drawing.Font("Siemens Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
                fichaButton.ForeColor = System.Drawing.Color.White;
                fichaButton.UseVisualStyleBackColor = false;

                // Ajustar el ancho de las columnas del TableLayoutPanel
                itemPanel.ColumnStyles[1] = new ColumnStyle(SizeType.Percent, 100);
                itemPanel.ColumnStyles[2] = new ColumnStyle(SizeType.AutoSize);
                itemPanel.ColumnStyles[3] = new ColumnStyle(SizeType.Percent, 100);
                itemPanel.ColumnStyles[4] = new ColumnStyle(SizeType.AutoSize);
                itemPanel.ColumnStyles[5] = new ColumnStyle(SizeType.AutoSize);

                itemPanel.SetColumnSpan(label, 1);
                itemPanel.SetRowSpan(label, 1);
                itemPanel.Controls.Add(label, 1, 0);

                itemPanel.SetColumnSpan(label2, 1);
                itemPanel.SetRowSpan(label2, 1);
                itemPanel.Controls.Add(label2, 1, 1);

                itemPanel.SetColumnSpan(label3, 1);
                itemPanel.SetRowSpan(label3, 1);
                itemPanel.Controls.Add(label3, 1, 2);

                itemPanel.SetColumnSpan(label4, 1);
                itemPanel.SetRowSpan(label4, 1);
                itemPanel.Controls.Add(label4, 1, 3);

                itemPanel.SetColumnSpan(label5, 1);
                itemPanel.SetRowSpan(label5, 1);
                itemPanel.Controls.Add(label5, 1, 4);

                itemPanel.SetColumnSpan(separatorLabel, 1);
                itemPanel.SetRowSpan(separatorLabel, 1);
                itemPanel.Controls.Add(separatorLabel, 1, 5);

                itemPanel.Controls.Add(editarButton, 4, 0);
                if (listaSolicitudes[i].StatusSolicitud.ToString() == "EN REVISIÓN" ||
                    listaSolicitudes[i].StatusSolicitud.ToString() == "NO APROBADO")
                {
                    editarButton.Visible = true;
                }
                if (rol == 1 || listaSolicitudes[i].StatusSolicitud.ToString() == "APROBADO")
                {
                    editarButton.Visible = false;
                }
                itemPanel.Controls.Add(verButton, 5, 0);
                itemPanel.Controls.Add(fichaButton, 5, 1);

                flowLayoutPanel1.Controls.Add(itemPanel);
            }
        }

        private void DetalleButtonClick(object sender, EventArgs e)
        {
            Label label2 = (Label)sender;
            int indice = (int)label2.Tag;
            //MessageBox.Show("DETALLE DE LA SOLICITUD: " + indice);
            DetalleSolicitud detalleSolicitud = new DetalleSolicitud(indice, rol, idUsuario);
            this.Hide();
            detalleSolicitud.ShowDialog();
            this.Close();
        }

        private void EditarButtonClick(object sender, EventArgs e)
        {
            Button editarButton = (Button)sender;
            int indice = (int)editarButton.Tag;
            CSolicitud.IdSolicitud = indice;
            //MessageBox.Show("VAS A EDITAR EL ELEMENTO: " + CSolicitud.IdSolicitud);
            ActualizarSolicitud actualizarSolicitud = new ActualizarSolicitud(rol, idUsuario, indice);
            this.Hide();
            actualizarSolicitud.ShowDialog();
            this.Close();
        }

        public void VerHdsButtonClick(object sender, EventArgs e)
        {
            Button verButton = (Button)sender;
            int indice = (int)verButton.Tag;
            //MessageBox.Show("HOJA DE SEGURIDAD DE LA SOLICITUD: "+ indice);

            List<Clases.CSolicitud> lista = new List<Clases.CSolicitud>();
            lista = CSolicitud.solicitudId(indice);
            int n = CSolicitud.solicitudId(indice).Count();

            foreach (Clases.CSolicitud item in lista)
            {
                //CREAR CARPETA TEMPORAL DONDE SE GUARDARÁN LOS ARCHIVOS
                string direccion = AppDomain.CurrentDomain.BaseDirectory;
                string carpeta = direccion + @"temp\";
                string ubicacionCompleta = carpeta + item.ExtensionHdsSolicitud;
                /*MessageBox.Show("DIRECCION: " + direccion + "\n" +
                                "CARPETA: " + carpeta + "\n" +
                                "UBICACIONCOMPLETA: " + ubicacionCompleta);*/

                //Validaciones
                if (!Directory.Exists(carpeta))
                    Directory.CreateDirectory(carpeta);

                if (File.Exists(ubicacionCompleta))
                    File.Delete(ubicacionCompleta);

                File.WriteAllBytes(ubicacionCompleta, item.HdsSolicitud);

                var p = new Process();
                p.StartInfo = new ProcessStartInfo(ubicacionCompleta)
                {
                    UseShellExecute = true
                };
                p.Start();
            }
        }

        public void FichaButtonClick(object sender, EventArgs e)
        {
            Button fichaButton = (Button)sender;
            int indice = (int)fichaButton.Tag;
            //MessageBox.Show("FICHA TÉCICA DE LA SOLICITUD: " + indice);

            List<Clases.CSolicitud> lista = new List<Clases.CSolicitud>();
            lista = CSolicitud.solicitudId(indice);
            int n = CSolicitud.solicitudId(indice).Count();

            foreach (Clases.CSolicitud item in lista)
            {
                //CREAR CARPETA TEMPORAL DONDE SE GUARDARÁN LOS ARCHIVOS
                string direccion = AppDomain.CurrentDomain.BaseDirectory;
                string carpeta = direccion + @"temp\";
                string ubicacionCompleta = carpeta + item.ExtensionFichaSolicitud;
                /*MessageBox.Show("DIRECCION: " + direccion + "\n" +
                                "CARPETA: " + carpeta + "\n" +
                                "UBICACIONCOMPLETA: " + ubicacionCompleta);*/

                //Validaciones
                if (!Directory.Exists(carpeta))
                    Directory.CreateDirectory(carpeta);

                if (File.Exists(ubicacionCompleta))
                    File.Delete(ubicacionCompleta);

                File.WriteAllBytes(ubicacionCompleta, item.FichaSolicitud);

                var p = new Process();
                p.StartInfo = new ProcessStartInfo(ubicacionCompleta)
                {
                    UseShellExecute = true
                };
                p.Start();
            }
        }

        private void btnEliminarSoli_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("¿Seguro que deseas eliminar todas las solicitudes?",
                "Eliminar solicitudes", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                CSolicitud.eliminarSolicitudes();
                Administrador administrador = new Administrador(rol, idUsuario); //Llama al form principal de iniciar sesión
                this.Hide(); //Esconde la pantalla actual
                administrador.ShowDialog(); //Nos lleva a la pantalla que queremos
                this.Close(); //Cierra la pantalla actual
            }
        }
    }
}
