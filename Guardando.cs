﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaScreen
{
    public partial class Guardando : Form
    {
        public Guardando()
        {
            InitializeComponent();
        }

        private void Guardando_Load(object sender, EventArgs e)
        {
            imgGuardando.Load("guardando.gif");
            imgGuardando.Location = new Point (this.Width / 2 - imgGuardando.Width / 2,
                this.Height / 2 - imgGuardando.Height / 2);
        }
    }
}
