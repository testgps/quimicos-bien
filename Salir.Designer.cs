﻿namespace PruebaScreen
{
    partial class Salir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MiSalir = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.MiSalir)).BeginInit();
            this.SuspendLayout();
            // 
            // MiSalir
            // 
            this.MiSalir.Location = new System.Drawing.Point(145, 21);
            this.MiSalir.Name = "MiSalir";
            this.MiSalir.Size = new System.Drawing.Size(791, 657);
            this.MiSalir.TabIndex = 0;
            this.MiSalir.TabStop = false;
            // 
            // Salir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 690);
            this.Controls.Add(this.MiSalir);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Salir";
            this.Opacity = 0.7D;
            this.Text = "Salir";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Salir_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MiSalir)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PictureBox MiSalir;
    }
}