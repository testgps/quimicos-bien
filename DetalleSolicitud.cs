﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PruebaScreen
{
    public partial class DetalleSolicitud : Form
    {
        Clases.CSolicitud cSolicitud = new Clases.CSolicitud();
        Clases.CComentario cComentario = new Clases.CComentario();
        Font SmallFont = new Font("Siemens Sans", 10);
        Font SmallFontBold = new Font("Siemens Sans", 10, FontStyle.Bold);
        Font MediumFont = new Font("Siemens Sans", 12);
        Font LargeFont = new Font("Siemens Sans", 14, FontStyle.Bold);
        private int rol;
        private int indice;
        private int idUsuario;
        private string idSolicitud;
        private string status;

        public DetalleSolicitud(int indice, int rol, int idUsuario)
        {
            InitializeComponent();
            this.rol = rol;
            this.indice = indice;
            this.idUsuario = idUsuario;
            if (rol == 3) //COMPRAS
            {
                lblComentarios.Visible = false;
                txtComentarios.Visible = false;
                btnEnviar.Visible = false;
                flowLayoutPanel1.Dock = DockStyle.Fill;
            }
            mostrarSolicitud();
        }

        public void mostrarSolicitud()
        {
            List<Clases.CSolicitud> listaSolicitud = new List<Clases.CSolicitud>();
            listaSolicitud = cSolicitud.solicitudId(indice);
            int n = cSolicitud.solicitudId(indice).Count();

            List<Clases.CComentario> listaComentarios = cComentario.GetComentarioById(indice);
            int nComentarios = listaComentarios.Count;
            //MessageBox.Show("LA SOLICITUD " + indice + " TIENE " + nComentarios + " COMENTARIOS");
            status = "";
            
            for (int i = 0; i < n; i++)
            {
                // Crear un contenedor para el elemento de la lista
                TableLayoutPanel itemPanel = new TableLayoutPanel();
                itemPanel.AutoSize = true;
                itemPanel.ColumnCount = 6;
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                itemPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));

                Label label = new Label();
                label.Text = "N. de Solicitud: " + listaSolicitud[i].IdSolicitud.ToString();
                label.AutoSize = false; // Establece AutoSize en false
                label.Dock = DockStyle.Top; // Establece Dock en Top
                label.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                label.Margin = new Padding(20, 0, 0, 25);
                label.Font = LargeFont;
                label.AutoSize = false;
                label.Size = new Size(label.PreferredWidth, label.PreferredHeight);

                Label label2 = new Label();
                label2.Text = listaSolicitud[i].NombreSolicitud.ToString();
                label2.Margin = new Padding(20, 0, 0, 25);
                label2.Font = LargeFont;
                label2.AutoSize = false;
                label2.Size = new Size(label2.PreferredWidth, label2.PreferredHeight);

                Label label3 = new Label();
                label3.Text = listaSolicitud[i].FabricanteSolicitud.ToString();
                label3.Margin = new Padding(20, 0, 0, 25);
                label3.Font = SmallFont;
                label3.AutoSize = false;
                label3.Size = new Size(label3.PreferredWidth, label3.PreferredHeight);

                Label label4 = new Label();
                label4.Text = "Fecha de solicitud: " + listaSolicitud[i].FechaSolicitud.ToString();
                label4.Margin = new Padding(20, 0, 0, 25);
                label4.Font = SmallFontBold;
                label4.AutoSize = false;
                label4.Size = new Size(label4.PreferredWidth, label4.PreferredHeight);

                ComboBox comboStatus = new ComboBox();
                if (rol == 1)
                {
                    comboStatus.DropDownWidth = 280;
                    comboStatus.Items.AddRange(new object[] {"EN REVISIÓN",
                        "APROBADO",
                        "NO APROBADO",});
                    comboStatus.Margin = new Padding(20, 0, 0, 25);
                    comboStatus.Font = SmallFontBold;
                    comboStatus.AutoSize = false;
                    comboStatus.DropDownStyle = ComboBoxStyle.DropDownList;
                    comboStatus.FormattingEnabled = true;
                    comboStatus.Text = listaSolicitud[i].StatusSolicitud.ToString();
                    comboStatus.Size = new Size(280, 21);

                    itemPanel.SetColumnSpan(comboStatus, 1);
                    itemPanel.SetRowSpan(comboStatus, 1);
                    itemPanel.Controls.Add(comboStatus, 1, 5);
                }

                Label label5 = new Label();
                if (listaSolicitud[i].StatusSolicitud.ToString() == string.Empty)
                {
                    label5.Text = "STATUS: NA";
                    label5.Margin = new Padding(20, 0, 0, 25);
                    label5.Font = SmallFontBold;
                    label5.AutoSize = false;
                    label5.Size = new Size(label5.PreferredWidth, label5.PreferredHeight);
                }
                else
                {
                    label5.Text = "STATUS: " + listaSolicitud[i].StatusSolicitud.ToString();
                    label5.Margin = new Padding(20, 0, 0, 25);
                    label5.Font = SmallFontBold;
                    label5.AutoSize = false;
                    label5.Size = new Size(label5.PreferredWidth, label5.PreferredHeight);
                }

                Label separatorLabel = new Label();
                separatorLabel.BorderStyle = BorderStyle.FixedSingle;
                separatorLabel.AutoSize = false;
                separatorLabel.Height = 1;
                separatorLabel.Margin = new Padding(20, 0, 0, 25);
                separatorLabel.Padding = new Padding(0);
                separatorLabel.BorderStyle = BorderStyle.None;
                separatorLabel.BackColor = Color.Gray;
                int separatorWidth = 400; // Ajusta el ancho según tus necesidades
                separatorLabel.Size = new Size(separatorWidth, separatorLabel.Height);

                // Ajustar el ancho de las columnas del TableLayoutPanel
                itemPanel.ColumnStyles[1] = new ColumnStyle(SizeType.Percent, 100);
                itemPanel.ColumnStyles[2] = new ColumnStyle(SizeType.AutoSize);
                itemPanel.ColumnStyles[3] = new ColumnStyle(SizeType.Percent, 100);
                itemPanel.ColumnStyles[4] = new ColumnStyle(SizeType.AutoSize);
                itemPanel.ColumnStyles[5] = new ColumnStyle(SizeType.AutoSize);

                itemPanel.SetColumnSpan(label, 1);
                itemPanel.SetRowSpan(label, 1);
                itemPanel.Controls.Add(label, 1, 0);

                itemPanel.SetColumnSpan(label2, 1);
                itemPanel.SetRowSpan(label2, 1);
                itemPanel.Controls.Add(label2, 1, 1);

                itemPanel.SetColumnSpan(label3, 1);
                itemPanel.SetRowSpan(label3, 1);
                itemPanel.Controls.Add(label3, 1, 2);

                itemPanel.SetColumnSpan(label4, 1);
                itemPanel.SetRowSpan(label4, 1);
                itemPanel.Controls.Add(label4, 1, 3);

                itemPanel.SetColumnSpan(label5, 1);
                itemPanel.SetRowSpan(label5, 1);
                itemPanel.Controls.Add(label5, 1, 4);

                itemPanel.SetColumnSpan(separatorLabel, 1);
                itemPanel.SetRowSpan(separatorLabel, 1);
                itemPanel.Controls.Add(separatorLabel, 1, 6);

                Label comentarios = new Label();
                comentarios.Text = "Comentarios";
                comentarios.Margin = new Padding(20, 0, 0, 25);
                comentarios.Font = SmallFontBold;
                comentarios.AutoSize = false;
                comentarios.Size = new Size(label4.PreferredWidth, label4.PreferredHeight);

                itemPanel.SetColumnSpan(comentarios, 1);
                itemPanel.SetRowSpan(comentarios, 1);
                itemPanel.Controls.Add(comentarios, 1, 7);

                FlowLayoutPanel flowLayoutPanelComentarios = new FlowLayoutPanel();
                flowLayoutPanelComentarios.FlowDirection = FlowDirection.TopDown;
                flowLayoutPanelComentarios.AutoSize = true; // Ajustar el tamaño automáticamente
                itemPanel.Controls.Add(flowLayoutPanelComentarios, 1, 8);

                for (int j = 0; j < nComentarios; j++)
                {
                    Label comentario = new Label();
                    comentario.Text = listaComentarios[j].Usuario.ToString()+": "+ listaComentarios[j].Comentario.ToString();
                    comentario.Margin = new Padding(20, 0, 0, 25);
                    comentario.Font = SmallFont;
                    comentario.AutoSize = false;
                    comentario.Size = new Size(comentario.PreferredWidth, comentario.PreferredHeight);

                    flowLayoutPanelComentarios.Controls.Add(comentario);
                }

                flowLayoutPanel1.Controls.Add(itemPanel);
                //status = comboStatus.SelectedItem.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (rol == 1)
            {
                foreach (Control control in flowLayoutPanel1.Controls)
                {
                    if (control is TableLayoutPanel tablePanel)
                    {
                        // Busca los controles Label dentro de cada TableLayoutPanel
                        foreach (Control subControl in tablePanel.Controls)
                        {
                            if (subControl is ComboBox comboBox)
                            {
                                status = comboBox.SelectedItem.ToString();
                                break;
                            }
                        }
                    }
                }
                //MessageBox.Show("EL STATUS ACTUAL DE LA SOLICITUD ES: " + status);
                cSolicitud.StatusSolicitud = status;
                cSolicitud.editarStatus(indice);
            }

            Solicitudes solicitudes = new Solicitudes(rol, idUsuario);
            this.Hide();
            solicitudes.ShowDialog();
            this.Close();
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            cComentario.IdSolicitud = indice;
            cComentario.IdUsuario = idUsuario;
            cComentario.Comentario = txtComentarios.Text;

            if (string.IsNullOrWhiteSpace(cComentario.Comentario))
            {
                MessageBox.Show("Debes ingresar un comentario.");
            }
            else
            {
                cComentario.agregarComentario();
                txtComentarios.Text = string.Empty;

                /////////////////////////
                // Actualizar el flowLayoutPanelComentarios
                List<Clases.CComentario> listaComentarios = cComentario.GetComentarioById(indice);
                int nComentarios = listaComentarios.Count;

                for (int i = 0; i < flowLayoutPanel1.Controls.Count; i++)
                {
                    TableLayoutPanel itemPanel = flowLayoutPanel1.Controls[i] as TableLayoutPanel;
                    FlowLayoutPanel flowLayoutPanelComentarios = itemPanel.Controls[itemPanel.Controls.Count - 1] as FlowLayoutPanel;

                    if (flowLayoutPanelComentarios != null)
                    {
                        // Limpiar el contenido actual del flowLayoutPanelComentarios
                        flowLayoutPanelComentarios.Controls.Clear();

                        // Agregar los nuevos comentarios al flowLayoutPanelComentarios
                        for (int j = 0; j < nComentarios; j++)
                        {
                            Label comentario = new Label();
                            comentario.Text = listaComentarios[j].Usuario.ToString() + ": " + listaComentarios[j].Comentario.ToString();
                            comentario.Margin = new Padding(20, 0, 0, 25);
                            comentario.Font = SmallFont;
                            comentario.AutoSize = false;
                            comentario.Size = new Size(comentario.PreferredWidth, comentario.PreferredHeight);

                            flowLayoutPanelComentarios.Controls.Add(comentario);
                        }
                        flowLayoutPanelComentarios.Refresh();
                    }
                }
            }
        }
    }
}
