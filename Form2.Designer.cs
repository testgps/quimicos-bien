﻿namespace PruebaScreen
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            txtBuscar = new TextBox();
            btnExit = new PictureBox();
            label1 = new Label();
            panel1 = new Panel();
            checkBoxArea = new CheckBox();
            checkBoxGaveta = new CheckBox();
            btnEliminarFiltro = new Button();
            btnFiltro = new Button();
            cmbBoxArea = new ComboBox();
            cmbBoxGaveta = new ComboBox();
            label11 = new Label();
            btnBuscar = new PictureBox();
            flowLayoutPanel1 = new FlowLayoutPanel();
            btnCargarFiltro = new Button();
            btnCargar = new Button();
            btnCargarBuscar = new Button();
            linkCheckList = new LinkLabel();
            ((System.ComponentModel.ISupportInitialize)btnExit).BeginInit();
            panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)btnBuscar).BeginInit();
            SuspendLayout();
            // 
            // txtBuscar
            // 
            txtBuscar.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            txtBuscar.Location = new Point(158, 68);
            txtBuscar.Name = "txtBuscar";
            txtBuscar.Size = new Size(951, 31);
            txtBuscar.TabIndex = 22;
            txtBuscar.TextChanged += txtBuscar_TextChanged;
            // 
            // btnExit
            // 
            btnExit.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            btnExit.BackColor = Color.Transparent;
            btnExit.Cursor = Cursors.Hand;
            btnExit.Image = (Image)resources.GetObject("btnExit.Image");
            btnExit.Location = new Point(1192, 9);
            btnExit.Name = "btnExit";
            btnExit.Size = new Size(50, 50);
            btnExit.SizeMode = PictureBoxSizeMode.StretchImage;
            btnExit.TabIndex = 24;
            btnExit.TabStop = false;
            btnExit.Click += btnExit_Click;
            // 
            // label1
            // 
            label1.Anchor = AnchorStyles.Top;
            label1.BackColor = Color.Transparent;
            label1.Font = new Font("Siemens Sans", 22F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(442, 1);
            label1.Name = "label1";
            label1.Size = new Size(464, 58);
            label1.TabIndex = 25;
            label1.Text = "Listado de Sustancias";
            // 
            // panel1
            // 
            panel1.Controls.Add(checkBoxArea);
            panel1.Controls.Add(checkBoxGaveta);
            panel1.Controls.Add(btnEliminarFiltro);
            panel1.Controls.Add(btnFiltro);
            panel1.Controls.Add(cmbBoxArea);
            panel1.Controls.Add(cmbBoxGaveta);
            panel1.Controls.Add(label11);
            panel1.Controls.Add(btnBuscar);
            panel1.Controls.Add(label1);
            panel1.Controls.Add(txtBuscar);
            panel1.Controls.Add(btnExit);
            panel1.Dock = DockStyle.Top;
            panel1.Location = new Point(0, 0);
            panel1.Name = "panel1";
            panel1.Size = new Size(1333, 158);
            panel1.TabIndex = 26;
            // 
            // checkBoxArea
            // 
            checkBoxArea.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxArea.ImageAlign = ContentAlignment.TopCenter;
            checkBoxArea.Location = new Point(145, 109);
            checkBoxArea.Name = "checkBoxArea";
            checkBoxArea.Size = new Size(97, 48);
            checkBoxArea.TabIndex = 95;
            checkBoxArea.Text = "Activar áreas";
            checkBoxArea.TextAlign = ContentAlignment.MiddleCenter;
            checkBoxArea.UseVisualStyleBackColor = true;
            checkBoxArea.CheckedChanged += checkBoxArea_CheckedChanged;
            // 
            // checkBoxGaveta
            // 
            checkBoxGaveta.Font = new Font("Siemens Sans", 9F, FontStyle.Bold, GraphicsUnit.Point);
            checkBoxGaveta.ImageAlign = ContentAlignment.TopCenter;
            checkBoxGaveta.Location = new Point(577, 106);
            checkBoxGaveta.Name = "checkBoxGaveta";
            checkBoxGaveta.Size = new Size(105, 48);
            checkBoxGaveta.TabIndex = 94;
            checkBoxGaveta.Text = "Activar gavetas";
            checkBoxGaveta.TextAlign = ContentAlignment.MiddleCenter;
            checkBoxGaveta.UseVisualStyleBackColor = true;
            checkBoxGaveta.CheckedChanged += checkBoxGaveta_CheckedChanged;
            // 
            // btnEliminarFiltro
            // 
            btnEliminarFiltro.Anchor = AnchorStyles.Top;
            btnEliminarFiltro.BackColor = Color.FromArgb(135, 30, 80);
            btnEliminarFiltro.FlatAppearance.BorderSize = 0;
            btnEliminarFiltro.FlatStyle = FlatStyle.Flat;
            btnEliminarFiltro.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnEliminarFiltro.ForeColor = Color.White;
            btnEliminarFiltro.Location = new Point(1168, 109);
            btnEliminarFiltro.Name = "btnEliminarFiltro";
            btnEliminarFiltro.Size = new Size(156, 41);
            btnEliminarFiltro.TabIndex = 93;
            btnEliminarFiltro.Text = "Eliminar filtros";
            btnEliminarFiltro.UseVisualStyleBackColor = false;
            btnEliminarFiltro.Click += btnEliminarFiltro_Click;
            // 
            // btnFiltro
            // 
            btnFiltro.Anchor = AnchorStyles.Top;
            btnFiltro.BackColor = Color.FromArgb(80, 190, 215);
            btnFiltro.FlatAppearance.BorderSize = 0;
            btnFiltro.FlatStyle = FlatStyle.Flat;
            btnFiltro.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnFiltro.ForeColor = Color.White;
            btnFiltro.Location = new Point(1004, 109);
            btnFiltro.Name = "btnFiltro";
            btnFiltro.Size = new Size(156, 41);
            btnFiltro.TabIndex = 89;
            btnFiltro.Text = "Aplicar filtros";
            btnFiltro.UseVisualStyleBackColor = false;
            btnFiltro.Click += btnFiltro_Click;
            // 
            // cmbBoxArea
            // 
            cmbBoxArea.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbBoxArea.FormattingEnabled = true;
            cmbBoxArea.Location = new Point(248, 113);
            cmbBoxArea.Name = "cmbBoxArea";
            cmbBoxArea.Size = new Size(295, 33);
            cmbBoxArea.TabIndex = 92;
            // 
            // cmbBoxGaveta
            // 
            cmbBoxGaveta.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbBoxGaveta.FormattingEnabled = true;
            cmbBoxGaveta.Location = new Point(688, 113);
            cmbBoxGaveta.Name = "cmbBoxGaveta";
            cmbBoxGaveta.Size = new Size(295, 33);
            cmbBoxGaveta.TabIndex = 90;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label11.Location = new Point(28, 117);
            label11.Name = "label11";
            label11.Size = new Size(110, 24);
            label11.TabIndex = 91;
            label11.Text = "Filtrar por:";
            // 
            // btnBuscar
            // 
            btnBuscar.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            btnBuscar.Cursor = Cursors.Hand;
            btnBuscar.Image = (Image)resources.GetObject("btnBuscar.Image");
            btnBuscar.Location = new Point(1134, 60);
            btnBuscar.Name = "btnBuscar";
            btnBuscar.Size = new Size(40, 40);
            btnBuscar.SizeMode = PictureBoxSizeMode.StretchImage;
            btnBuscar.TabIndex = 38;
            btnBuscar.TabStop = false;
            btnBuscar.Click += btnBuscar_Click;
            // 
            // flowLayoutPanel1
            // 
            flowLayoutPanel1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            flowLayoutPanel1.AutoScroll = true;
            flowLayoutPanel1.FlowDirection = FlowDirection.TopDown;
            flowLayoutPanel1.Location = new Point(12, 164);
            flowLayoutPanel1.Name = "flowLayoutPanel1";
            flowLayoutPanel1.Size = new Size(1321, 578);
            flowLayoutPanel1.TabIndex = 47;
            flowLayoutPanel1.WrapContents = false;
            // 
            // btnCargarFiltro
            // 
            btnCargarFiltro.Anchor = AnchorStyles.Bottom;
            btnCargarFiltro.BackColor = Color.FromArgb(80, 190, 215);
            btnCargarFiltro.FlatAppearance.BorderSize = 0;
            btnCargarFiltro.FlatStyle = FlatStyle.Flat;
            btnCargarFiltro.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnCargarFiltro.ForeColor = Color.White;
            btnCargarFiltro.Location = new Point(566, 748);
            btnCargarFiltro.Name = "btnCargarFiltro";
            btnCargarFiltro.Size = new Size(200, 41);
            btnCargarFiltro.TabIndex = 49;
            btnCargarFiltro.Text = "CARGAR MÁS";
            btnCargarFiltro.UseVisualStyleBackColor = false;
            btnCargarFiltro.Visible = false;
            btnCargarFiltro.Click += btnCargarFiltro_Click;
            // 
            // btnCargar
            // 
            btnCargar.Anchor = AnchorStyles.Bottom;
            btnCargar.BackColor = Color.FromArgb(80, 190, 215);
            btnCargar.FlatAppearance.BorderSize = 0;
            btnCargar.FlatStyle = FlatStyle.Flat;
            btnCargar.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnCargar.ForeColor = Color.White;
            btnCargar.Location = new Point(566, 748);
            btnCargar.Name = "btnCargar";
            btnCargar.Size = new Size(200, 41);
            btnCargar.TabIndex = 50;
            btnCargar.Text = "Cargar más";
            btnCargar.UseVisualStyleBackColor = false;
            btnCargar.Click += btnCargar_Click;
            // 
            // btnCargarBuscar
            // 
            btnCargarBuscar.Anchor = AnchorStyles.Bottom;
            btnCargarBuscar.BackColor = Color.FromArgb(80, 190, 215);
            btnCargarBuscar.FlatAppearance.BorderSize = 0;
            btnCargarBuscar.FlatStyle = FlatStyle.Flat;
            btnCargarBuscar.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            btnCargarBuscar.ForeColor = Color.White;
            btnCargarBuscar.Location = new Point(566, 748);
            btnCargarBuscar.Name = "btnCargarBuscar";
            btnCargarBuscar.Size = new Size(200, 41);
            btnCargarBuscar.TabIndex = 51;
            btnCargarBuscar.Text = "Cargar MÁS";
            btnCargarBuscar.UseVisualStyleBackColor = false;
            btnCargarBuscar.Visible = false;
            btnCargarBuscar.Click += btnCargarBuscar_Click;
            // 
            // linkCheckList
            // 
            linkCheckList.AutoSize = true;
            linkCheckList.Font = new Font("Siemens Sans", 10F, FontStyle.Bold, GraphicsUnit.Point);
            linkCheckList.Location = new Point(847, 756);
            linkCheckList.Name = "linkCheckList";
            linkCheckList.Size = new Size(474, 24);
            linkCheckList.TabIndex = 96;
            linkCheckList.TabStop = true;
            linkCheckList.Text = "CHECK LIST DE GAVETA DE SUSTANCIAS QUIMICAS";
            linkCheckList.LinkClicked += linkCheckList_LinkClicked;
            // 
            // Form2
            // 
            AutoScaleDimensions = new SizeF(10F, 25F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoScroll = true;
            BackColor = Color.White;
            ClientSize = new Size(1333, 794);
            Controls.Add(linkCheckList);
            Controls.Add(btnCargarBuscar);
            Controls.Add(btnCargar);
            Controls.Add(btnCargarFiltro);
            Controls.Add(flowLayoutPanel1);
            Controls.Add(panel1);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "Form2";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Listado de Sustancias";
            Load += Form2_Load;
            ((System.ComponentModel.ISupportInitialize)btnExit).EndInit();
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)btnBuscar).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private TextBox txtBuscar;
        private PictureBox btnExit;
        private Label label1;
        private Panel panel1;
        private FlowLayoutPanel flowLayoutPanel1;
        private PictureBox btnBuscar;
        private Button btnCargarFiltro;
        private Button btnCargar;
        private Button btnEliminarFiltro;
        private Button btnFiltro;
        private ComboBox cmbBoxArea;
        private ComboBox cmbBoxGaveta;
        private Label label11;
        private Button btnCargarBuscar;
        private CheckBox checkBoxGaveta;
        private CheckBox checkBoxArea;
        private LinkLabel linkCheckList;
    }
}